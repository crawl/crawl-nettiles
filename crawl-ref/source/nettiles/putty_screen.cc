/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#include "AppHdr.h"
#include "screen.h"
#include "putty_terminal_frontend.h"
#include <sstream>
#include <iostream>
using namespace std;

extern "C" {
#include "putty/putty.h"
#include "putty/terminal.h"
}

struct PuttyScreen : public Screen, public PuttyTerminalFrontend
{
	Config cfg;
	Terminal* term;
	struct unicode_data ucsdata;

	PuttyScreen(unsigned max_rows, unsigned max_cols, bool utf8)
	{
		do_defaults(NULL, &cfg);
		strcpy(cfg.line_codepage, utf8 ? "UTF-8" : "");
		cfg.utf8_override = utf8;
		memset(&ucsdata, 0, sizeof(ucsdata));
#ifdef WIN32
		init_ucs(&cfg, &ucsdata);
#else
		init_ucs(&ucsdata, cfg.line_codepage, FALSE, 0, cfg.vtmode);
#endif

		/*
		int i;
		for (i = 0; i < 256; i++) {
			if (i >= 0x5F && i < 0x7F)
				ucsdata.unitab_xterm[i] = 0xD900 + i;
			else
				ucsdata.unitab_xterm[i] = ucsdata.unitab_line[i];
		}
		*/

		ucsdata.dbcs_screenfont = FALSE;
		term = term_init(&cfg, &ucsdata, (PuttyTerminalFrontend*)this);
		term->ldisc = NULL;
		term_size(term, max_rows, max_cols, 0);
		term_pwron(term, TRUE);
	}

	~PuttyScreen()
	{}

	void notify()
	{
		if(stamp_begin < stamp_end) {
			update_signal(*this);
			stamp_begin = stamp_end;
			changes.clear();
		}
	}

	virtual void draw_text(int x, int y, wchar_t *text, int len, xattr_u a)
	{
		int i;

		if(!len)
			return;

		if(y >= (int)screen.size())
			screen.resize(y + 1);
		vector<screen_cell>& line = screen[y];

		if((x + len) > (int)line.size()) {
			if(a.s.bg == BLACK && !a.s.vis_cursor && !a.s.reverse) {
				int min_len = line.size() - x;
				if(min_len < 0)
					min_len = 0;

				while(len > min_len && text[len - 1] == L' ')
					--len;

				if(!len)
					return;
			}

			if((x + len) > (int)line.size())
				line.resize(x + len);
		}

		for(i = 0; i < len; ++i) {
			wchar_t c = text[i];
			/* strip bizarre PuTTY remapped ranges */
			if((c & 0xff00) == 0xd800)
				c &= 0xff;
			else if((c & 0xff00) == 0xf000)
				c &= 0xff;
			else if((c & 0xff00) == 0xf100)
				c &= 0xff;
			assert(line[x + i].stamp < stamp_end);
			if(line[x + i].stamp >= stamp_begin)
				notify();
			wcell ca;
			ca.c = c;
			ca.a = a;
			if(c == L' ')
				ca.a.s.fg = 0;
			line[x + i].update(ca, stamp_end++);
			assert(line[x + i].stamp < stamp_end);
			changes.push_back(make_pair(coord_def(x + i, y), ca));
		}

		unsigned rows = cols.size();
		if(y >= cols.size())
			cols.resize(y + 1);

		if((x + len) >= cols[y]) {
			for(i = x + len - 1; i >= 0 && line[i].c == L' ' && line[i].a.v == 0; --i)
			{}
			cols[y] = i + 1;
			if((y + 1) >= rows) {
				for(i = y; i >= 0 && cols[i] == 0; --i)
				{}
				cols.resize(i + 1);
			}
		}
	}

	virtual void draw_cursor(int x, int y)
	{
		cursor.x = x;
		cursor.y = y;
	}

	virtual void set_cursor(int x, int y)
	{
		int_cursor.x = x;
		int_cursor.y = y;
	}

	virtual void move_cursor(int x, int y, int marg_clip)
	{
		cursor_stamp = stamp_end++;
		wcell dummy;
		dummy.c = 0;
		dummy.a.v = 0;
		changes.push_back(make_pair(coord_def(x, y), dummy));
	}

	virtual void add_data(const void* p, size_t size)
	{
		term_data(term, 0, (const char*)p, size);
	}

	virtual void update()
	{
		cursor.x = -1;
		cursor.y = -1;

		term_update(term);

		notify();
	}

	virtual void set_charset(const char* charset_name)
	{
		strcpy(cfg.line_codepage, charset_name);
#ifdef WIN32
		init_ucs(&cfg, &ucsdata);
#else
		init_ucs(&ucsdata, cfg.line_codepage, FALSE, 0, cfg.vtmode);
#endif
	}

	virtual bool in_application_cursor_mode()
	{
		return !!term->app_cursor_keys;
	}

	virtual bool in_application_keypad_mode()
	{
		return !!term->app_keypad_keys;
	}
};

Screen* Screen::create(unsigned rows, unsigned cols, bool utf8)
{
	return new PuttyScreen(rows, cols, utf8);
}
