/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#include "AppHdr.h"
#include "branch.h"
#include "output.h"
#include "skills2.h"
#include "colour.h"
#include "feature.h"
#include "env.h"
#include "view.h"
#include "mon-util.h"
#include "items.h"
#include "mgen_data.h"
#include "mon-place.h"
#include "coord.h"
#include "coordit.h"
#include "tiledef-main.h"
#include "tiledef-dngn.h"
#include "tiledef-player.h"
#include "libutil.h"
#include "transform.h"
#include "options.h"
#include "mon-stuff.h"

#include "crawl_data.h"
#include "crawl_parser.h"
#include "crawl_tiles_finalizer.h"
#include "crawl_defs.h"

#include <algorithm>
#include <sstream>
#include <iostream>
#ifdef _MSC_VER
#include <unordered_map>
#include <unordered_set>
#include <functional>
#else
#include <tr1/unordered_map>
#include <tr1/unordered_set>
#include <tr1/functional>
#endif
#include "ext_hash.h"
#include <map>
#include <iomanip>
#include <math.h>
#ifndef HUGE_VALF
#define HUGE_VALF ((float)HUGE_VAL)
#endif

using namespace std;
using namespace std::tr1;
using namespace std::tr1::placeholders;

struct full_line
{
	string s;

	friend bool operator ==(const full_line& a, const full_line& b)
	{
		return a.s == b.s;
	}

	friend bool operator !=(const full_line& a, const full_line& b)
	{
		return a.s != b.s;
	}
};

istream& operator >>(istream& in, full_line& l)
{
	getline(in, l.s);
	return in;
}

template<typename T>
struct ios_type
{
	typedef T type;
};

template<>
struct ios_type<char>
{
	typedef int type;
};

template<>
struct ios_type<unsigned char>
{
	typedef unsigned type;
};

/* we get these for free from the putty copy we carry */
struct charset_spec {
    int charset;
    void (*read)(...);
    void (*write)(...);
    unsigned long* data;
};

extern "C" const charset_spec charset_CS_ISO8859_1;
extern "C" const charset_spec charset_CS_CP437;

struct viewport_cell
{
	wchar_t c;
	cattr_t a;
	uint8_t reverse : 1;
	unsigned bg_flags;
	unsigned char fg_freqs[16];

	viewport_cell()
	{
		reset();
	}

	void reset()
	{
		c = L' ';
		a.fg = 0;
		a.bg = 0;
		bg_flags = 0;
		reverse = 0;
		memset(fg_freqs, 0, sizeof(fg_freqs));
	}

	void update(wcell pc, bool maybe_ray)
	{
		bg_flags = 0;
		if(pc.a.s.int_cursor && !pc.a.s.vis_cursor && pc.a.s.bg != BLACK) {
			bg_flags = TILE_FLAG_CURSOR1;
			/* In Tiles, the ray bg is there only if we are shooting
			 * However, we currently don't attempt to detect that, and this looks good.
			 */
			if(pc.a.s.fg == DARKGRAY)
				bg_flags |= TILE_FLAG_RAY_OOR;
			else
				bg_flags |= TILE_FLAG_RAY;
		} else if(maybe_ray && pc.a.s.fg == MAGENTA) {
			bg_flags = TILE_FLAG_RAY;
		} else if(maybe_ray && pc.a.s.fg == DARKGRAY) {
			bg_flags = TILE_FLAG_RAY_OOR;
		} else {
			if(pc.c != c) {
				memset(fg_freqs, 0, sizeof(fg_freqs));
				c = pc.c;
			}

			/* ignore vis_cursor and reverse */
			a = pc.a.s;
			if(a.fg == BLACK)
				a.fg = a.bg;
			reverse = pc.a.s.reverse;

			++fg_freqs[a.fg];
			if(!fg_freqs[a.fg]) {
				--fg_freqs[a.fg];
				for(unsigned i = 0; i < 16; ++i)
					fg_freqs[a.fg] >>= 1;
				++fg_freqs[a.fg];
			}
		}
	}
};

struct CrawlParserImpl : public CrawlParser
{
	Screen* screen;
	crawl_view_geometry geom;
	unsigned charset;
	bool tiles_enabled;

	bool have_more;

	int branch_depth;
	bool berserk;
	bool silence;
	coord_def player_pos;
	coord_def last_player_pos;
	coord_def double_center_pos;
	string cur_place;
	string cur_name_title;
	string cur_level_species_god;
	vector<vector<simple_cell> > simple_cells;
	vector<vector<viewport_cell> > viewport;

	int64_t tildes_seen;
	int64_t curlies_seen;

	unsigned unseen_wall_color;
	unsigned unseen_wavy_tile;
	unsigned wall_tiles[16];
	unsigned floor_tiles[16];
	unsigned cloud_tiles[16];
	bool is_wall_instead_of_cloud[16];

	bool inited;

	signal_connection update_connection;

	bool incomplete_mlist;

	typedef unordered_map<pair<char, unsigned char>, list<parsed_monster> > mlist_char_a_to_monsters_t;
	mlist_char_a_to_monsters_t mlist_char_a_to_monsters;
	mlist_char_a_to_monsters_t remaining_mlist_char_a_to_monsters;

	typedef unordered_set<string> effects_t;
	effects_t effects;

	CrawlTilesFinalizer* finalizer;

	virtual void set_charset(unsigned pcharset)
	{
		charset = pcharset;
	}

	virtual void enable_tiles(bool value)
	{
		if(value != tiles_enabled) {
			tiles_enabled = value;
			handle_screen_update(*screen);
		}
	}

	CrawlParserImpl(Screen* pscreen)
	: screen(pscreen), charset(0), tiles_enabled(false), branch_depth(-1), inited(false), view_lb(0), view_ub(INT_MAX)
	{
		finalizer = CrawlTilesFinalizer::create();
		update_connection = screen->update_signal.connect(std::tr1::bind(&CrawlParserImpl::handle_screen_update, this, _1));
	}

	~CrawlParserImpl()
	{
		update_connection.remove();
		delete finalizer;
	}

	void init()
	{
		if(inited)
			return;
		inited = true;

		crawl_data.init();

		init_game();
		init_level();
	}

	void init_game()
	{
		tildes_seen = 0;
		curlies_seen = 0;

		last_player_pos = player_pos = double_center_pos = coord_def(-1, -1);
	}

	void init_level()
	{
		/* TODO: Shoals huts should get vine walls
		 * TODO: Wizlabs should get appropriate rock/floors
		 */

		finalizer->reset();

		env.floor_colour = LIGHTGREY;
		env.rock_colour = BROWN;

		if (you.level_type == LEVEL_DUNGEON)
		{
			cerr << "LEVEL: " << branches[you.where_are_you].longname << ' ' << branch_depth << endl;

		        env.floor_colour    = branches[you.where_are_you].floor_colour;
		        env.rock_colour     = branches[you.where_are_you].rock_colour;

		        if(you.where_are_you == BRANCH_HALL_OF_ZOT) {
				int zot_wall_colors[5] = {LIGHTGREY, BLUE, LIGHTBLUE, MAGENTA, LIGHTMAGENTA};
				env.rock_colour = zot_wall_colors[branch_depth - 1];
		        }

		        unseen_wall_color = env.rock_colour;

		        if (you.where_are_you == BRANCH_VAULTS)
		        {
				if (branch_depth > 2)
					unseen_wall_color = LIGHTGREY;

				if (branch_depth > 4)
					unseen_wall_color = CYAN;

// TODO: autodetect this
//			            if (branch_depth > 6 && one_chance_in(10))
//			                unseen_wall_color = GREEN;
		        }
		        else if (you.where_are_you == BRANCH_CRYPT)
				unseen_wall_color = LIGHTGREY;
			else if (you.where_are_you == BRANCH_DIS)
				unseen_wall_color = CYAN;
		}
		else
			unseen_wall_color = env.rock_colour;

		if(you.level_type == LEVEL_PANDEMONIUM || player_in_branch(BRANCH_GEHENNA) || player_in_branch(BRANCH_DIS) || player_in_branch(BRANCH_TARTARUS))
			unseen_wavy_tile = TILE_DNGN_LAVA | TILE_FLAG_UNSEEN;
		else
			unseen_wavy_tile = TILE_DNGN_DEEP_WATER | TILE_FLAG_UNSEEN;

	        TileNewLevel(true);

	        init_walls_clouds_floors();
	}

	void init_walls_clouds_floors()
	{
	        tile_init_default_flavour();

		if(you.level_type == LEVEL_DUNGEON) {
			if(you.where_are_you == BRANCH_HALL_OF_ZOT) {
				int zot_wall_tiles[5] = {TILE_WALL_ZOT_WHITE, TILE_WALL_ZOT_BLUE, TILE_WALL_ZOT_CYAN, TILE_WALL_ZOT_BLUE, TILE_WALL_ZOT_MAGENTA};
				env.tile_default.wall = zot_wall_tiles[branch_depth - 1];
			}
		} else if(you.level_type == LEVEL_PANDEMONIUM) {
		        if((env.floor_colour - 1) % 3)
				env.tile_default.floor = TILE_FLOOR_TOMB;
		        else
				env.tile_default.floor = TILE_FLOOR_NERVES;

		        /* local Tiles does not use light version */
			env.tile_default.wall = tile_dngn_coloured(TILE_WALL_ZOT_BLUE, env.rock_colour);
		} else if (you.level_type == LEVEL_ABYSS) {
			env.tile_default.floor = TILE_FLOOR_NERVES;

		        switch (env.rock_colour)
		        {
		        case BROWN:
		        case YELLOW:
				env.tile_default.wall = TILE_WALL_HIVE;
				break;
		        case RED:
		        case LIGHTRED:
				env.tile_default.wall = TILE_WALL_PEBBLE_RED;
				break;
			case GREEN:
			case LIGHTGREEN:
				env.tile_default.wall = TILE_WALL_SLIME;
				break;
				break;
		        case CYAN:
			case LIGHTCYAN:
				env.tile_default.wall = TILE_WALL_ICE;
				break;
				break;
			case WHITE:
				env.tile_default.wall = TILE_WALL_HALL;
				break;
			case LIGHTGREY:
				env.tile_default.wall = TILE_WALL_UNDEAD;
				break;
			default:
				/* local Tiles does not do this */
				env.tile_default.wall = tile_dngn_coloured(TILE_WALL_ZOT_BLUE, env.rock_colour);
				break;
		        }
		} else if(you.level_type == LEVEL_PORTAL_VAULT) {
			env.tile_default.floor = tile_dngn_coloured(TILE_FLOOR_PEBBLE, env.floor_colour);
			env.tile_default.wall = tile_dngn_coloured(TILE_WALL_ZOT_BLUE, env.rock_colour);
		}

		for(unsigned i = 0; i < 16; ++i) {
			wall_tiles[i] = 0;
			cloud_tiles[i] = 0;
			floor_tiles[i] = 0;
		}

	        floor_tiles[DARKGREY] = env.tile_default.floor;
		floor_tiles[RED] = env.tile_default.floor | TILE_FLAG_BLOOD;

	        cloud_tiles[MAGENTA] = TILE_CLOUD_TLOC_ENERGY;
	        cloud_tiles[LIGHTBLUE] = TILE_CLOUD_BLUE_SMOKE;
	        cloud_tiles[WHITE] = TILE_CLOUD_COLD_2;
	        cloud_tiles[BLUE] = TILE_CLOUD_COLD_0;
	        cloud_tiles[GREEN] = TILE_CLOUD_POISON_2;
	        cloud_tiles[LIGHTGREEN] = TILE_CLOUD_POISON_1;
	        /* the other fire cloud tiles are too small and look bad */
	        cloud_tiles[YELLOW] = TILE_CLOUD_FIRE_2;
	        cloud_tiles[LIGHTRED] = TILE_CLOUD_FIRE_2;
	        cloud_tiles[RED] = TILE_CLOUD_FIRE_2;
	        cloud_tiles[LIGHTMAGENTA] = TILE_CLOUD_MUTAGENIC_2;
	        cloud_tiles[CYAN] = TILE_CLOUD_MIST;
	        cloud_tiles[LIGHTGREY] = TILE_CLOUD_GREY_SMOKE;
	        cloud_tiles[DARKGREY] = TILE_CLOUD_MIASMA;

	        wall_tiles[LIGHTCYAN] = TILE_DNGN_TRANSPARENT_WALL;
	        wall_tiles[GREEN] = TILE_DNGN_GREEN_CRYSTAL_WALL; /* TODO: limit where it can appear? */
	        wall_tiles[DARKGREY] = env.tile_default.wall;

	        /* water vapor conflicts with this, so don't show metal and stone walls in watery branches (unless a non-ASCII charset is used) */
	        if(you.where_are_you != BRANCH_SWAMP && you.where_are_you != BRANCH_SHOALS) {
			wall_tiles[CYAN] = TILE_DNGN_METAL_WALL;
			wall_tiles[LIGHTGREY] = TILE_DNGN_STONE_WALL;
	        }

	        if(you.where_are_you == BRANCH_SHOALS)
			wall_tiles[BLUE] = TILE_DNGN_OPEN_SEA;
	        if(you.where_are_you == BRANCH_MAIN_DUNGEON || you.where_are_you == BRANCH_HIVE)
			wall_tiles[YELLOW] = TILE_DNGN_WAX_WALL;

	        floor_tiles[env.floor_colour] = env.tile_default.floor;
	        wall_tiles[env.rock_colour] = env.tile_default.wall;

	        for(unsigned i = 0; i < 16; ++i)
			is_wall_instead_of_cloud[i] = wall_tiles[i] || !cloud_tiles[i];

	        wall_tiles[CYAN] = TILE_DNGN_METAL_WALL;
	        wall_tiles[LIGHTGREY] = TILE_DNGN_STONE_WALL;

	        for(unsigned i = 0; i < 16; ++i) {
			if(!floor_tiles[i])
				floor_tiles[i] = tile_dngn_coloured(env.tile_default.floor, i);
			if(i != DARKGREY)
				floor_tiles[i] |= TILE_FLAG_SEEN;
			else if(env.floor_colour != DARKGREY)
				floor_tiles[i] |= TILE_FLAG_UNSEEN;
	        }

	        for(unsigned i = 0; i < 16; ++i) {
			if(!wall_tiles[i])
				wall_tiles[i] = tile_dngn_coloured(env.tile_default.wall, i);
	        }

	        wall_tiles[DARKGREY] = wall_tiles[unseen_wall_color];

	        for(unsigned i = 0; i < 16; ++i) {
			if(i != DARKGREY)
				wall_tiles[i] |= TILE_FLAG_SEEN;
			else if(env.rock_colour != DARKGREY)
				wall_tiles[i] |= TILE_FLAG_UNSEEN;
	        }

	        for(unsigned i = 0; i < 16; ++i) {
			if(!cloud_tiles[i])
				cloud_tiles[i] = TILE_CLOUD_MUTAGENIC_2_3;
	        }
	}

	vector<span> hud_spans(unsigned row)
	{
		vector<span> res;
		generate_spans(res, simple_cells[geom.hudp.y + row], geom.hudp.x, geom.hudsz.x);
		return res;
	}

	bool parse_name()
	{
		vector<span> spans = hud_spans(0);
		if(!spans.size())
			return false;
		span& span = spans.front();
		if(span.a.fg != YELLOW || span.a.bg != 0)
			return false;

		if(cur_name_title == span.s)
			return true;
		cur_name_title = span.s;

		//cout << "NT " << span.s << endl;

		string name;
		const char* p = span.s.c_str();
		int i;
		for(i = span.s.size() - 1; i >= 0; --i)
		{
			if(!strncmp(p + i, " the ", 5)) {
				name = span.s.substr(0, i);
				break;
			} else if(!strncmp(p + i, ",", 2)) {
				name = span.s.substr(0, i);
				break;
			}
		}

		if(!name.empty() && name != you.your_name) {
			you.your_name = name;
			init_game();
		}

		//redraw_skill(you.your_name, player_title());

		return i >= 0;
	}

	bool parse_level_race()
	{
		vector<span> spans = hud_spans(1);
		if(!spans.size())
			return false;
		span& span = spans.front();
		if(span.a.fg != YELLOW || span.a.bg != 0)
			return false;

		if(span.s == cur_level_species_god)
			return true;
		 cur_level_species_god = span.s;

		//cout << "LSG " << span.s << endl;

		istringstream ss;
		ss.str(span.s);
		string level;
		ss >> level;
		if(level != "Level")
			return false;
		ss >> you.experience_level;
		if(ss.fail())
			return false;
		string species_god;
		getline(ss, species_god);
		species_god = trim_string(species_god);
		size_t godp = species_god.find(" of ");
		string species = species_god;
		string god;
		if(godp != string::npos) {
			species = species_god.substr(0, godp);
			god = species_god.substr(godp + 4);
		}

		CrawlData::name_to_species_t::iterator speciesi = crawl_data.name_to_species.find(species);
		if(speciesi != crawl_data.name_to_species.end()) {
			you.species = speciesi->second;
		} else {
			cerr << "Unknown species: " << species << endl;
			return false;
		}


		god_type religion = GOD_RANDOM;
		if(god.empty())
			religion = GOD_NO_GOD;
		else if(starts_with(god, "Jiyva"))
			religion = GOD_JIYVA;
		else {
			CrawlData::name_to_god_t::iterator godi = crawl_data.name_to_god.find(god);
			if(godi != crawl_data.name_to_god.end())
				religion = godi->second;
		}

		if(religion != GOD_RANDOM) {
			you.religion = religion;
			/* hack to turn on TSO halo for the doll */
			you.penance[you.religion] = 0;
			you.piety = 200;
		} else {
			cerr << "Unknown god: " << god << endl;
			return false;
		}

		//redraw_skill(you.your_name, player_title());

		init_player_doll();
		return true;
	}

	template<typename T>
	bool parse_hud_attr(unsigned row, const char* name, const char* name2, bool* changed, T* var, T* maxvar = 0, cattr_t* attr = 0, cattr_t* maxattr = 0)
	{
		vector<span> spans = hud_spans(row);
		string namec = name;
		namec += ":";
		string namec2;
		if(name2) {
			namec2 = name2;
			namec2 += ":";
		}

		for(unsigned i = 0; i < spans.size(); ++i) {
			//cout << "Span for " << name << ": " << spans[i].s << endl;
			// spans[i].a.fg == BROWN && (don't check this, some people change it)
			if(spans[i].a.bg == 0 && (spans[i].s == namec || spans[i].s == namec2) && (i + 1) < spans.size()) {
				istringstream ss;
				typename ios_type<T>::type v;
				if(!maxvar) {
					ss.str(spans[i + 1].s);
					ss >> v;
					if(ss.fail()) {
						cout << "fail" << endl;
						return false;
					}
					if(*var != v) {
						*var = v;
						if(changed)
							*changed = true;
					}
				} else {
					string s = spans[i + 1].s;
					size_t sl = s.find('/');
					if(sl == string::npos && i + 2 < spans.size()) {
						s += spans[i + 2].s;
						sl = s.find('/');
						if(maxattr)
							*maxattr = spans[i + 2].a;
					}
					else 	if(maxattr)
						*maxattr = spans[i + 1].a;
					if(sl == string::npos)
						return false;

					ss.str(s);
					ss >> v;
					if(ss.fail())
						return false;
					if(*var != v) {
						*var = v;
						if(changed)
							*changed = true;
					}

					ss.str(s.substr(sl + 1));
					ss >> v;
					if(ss.fail())
						return false;
					if(*maxvar != v) {
						*maxvar = v;
						if(changed)
							*changed = true;
					}
				}

				if(attr)
					*attr = spans[i + 1].a;
				return true;
			}
		}

		return false;
	}

	bool parse_place(unsigned line)
	{
		full_line place_line;
		if(!parse_hud_attr(line, "Place", 0, 0, &place_line))
			return false;
		string place = place_line.s;

		if(place == cur_place)
			return true;
		cur_place = place;

		if(starts_with(place, "A "))
			place = place.substr(2);
		else if(starts_with(place, "An "))
			place = place.substr(3);
		else if(starts_with(place, "The "))
			place = place.substr(4);

		string::size_type colon = place.find(':');
		int depth = -1;
		if(colon != string::npos) {
			istringstream ss;
			ss.str(place.substr(colon + 1));
			ss >> depth;
			place = place.substr(0, colon);
		}

		cerr << "PLACE: \"" << place << "\"" << endl;

		if(place == "Abyss") {
			you.level_type = LEVEL_ABYSS;
			you.where_are_you = NUM_BRANCHES;
		} else if(place == "Pandemonium") {
			you.level_type = LEVEL_PANDEMONIUM;
			you.where_are_you = NUM_BRANCHES;
		} else if(place == "Labyrinth") {
			you.level_type = LEVEL_LABYRINTH;
			you.where_are_you = NUM_BRANCHES;
		} else {
			unsigned i;
			for(i = 0; i < NUM_BRANCHES; ++i) {
				if(place == branches[i].shortname) {
					you.level_type = LEVEL_DUNGEON;
					you.where_are_you = (branch_type)i;
					break;
				}
			}
			if(i == NUM_BRANCHES) {
				you.level_type = LEVEL_PORTAL_VAULT;
				you.where_are_you = NUM_BRANCHES;
			}
		}

		/* TODO: set proper abbrev/origin */
		you.level_type_name = you.level_type_name_abbrev = you.level_type_origin = place;

		branch_depth = depth;

		/* XXX: actually do this */
		if(depth >= 0)
			you.absdepth0 = depth - 1;
		else
			you.absdepth0 = 0;

		init_level();

//		print_stats_level();

		return true;
	}

	int parse_hud()
	{
		int missing = 0;
		missing += !parse_name();
		if(missing)
			cerr << "Failed to parse name!" << endl;
		int v = !parse_level_race();
		missing += v;
		if(v)
			cerr << "Failed to parse level+race!" << endl;
		cattr_t hpa, hpmaxa;
		cattr_t mpa, mpmaxa;
		cattr_t stra;
		missing += !parse_hud_attr(2, "Health", "HP", &you.redraw_hit_points, &you.hp, &you.hp_max, &hpa, &hpmaxa);
		if(you.hp_max <= 0) /* avoid div by 0 */
			you.hp_max = 1;
		missing += !parse_hud_attr(3, "Magic", "MP", &you.redraw_magic_points, &you.magic_points, &you.max_magic_points, &mpa, &mpmaxa);
		if(you.max_magic_points <= 0) /* avoid div by 0 */
			you.max_magic_points = 1;
		unsigned mod_stats[NUM_STATS];
		unsigned max_stats[NUM_STATS];
		for(unsigned i = 0; i < NUM_STATS; ++i) {
			mod_stats[i] = you.stat((stat_type)i, false);
			max_stats[i] = you.max_stat((stat_type)i);
		}
		missing += !parse_hud_attr(4, "Str", 0, &you.redraw_stats[STAT_STR], &mod_stats[STAT_STR], (unsigned*)0, &stra);
		missing += !parse_hud_attr(5, "Int", 0, &you.redraw_stats[STAT_INT], &mod_stats[STAT_INT]);
		missing += !parse_hud_attr(6, "Dex", 0, &you.redraw_stats[STAT_DEX], &mod_stats[STAT_DEX]);
		bool has_gold = parse_hud_attr(7, "Gold", "$", 0, &you.gold);
		unsigned post_gold = 7;
		if(has_gold) {
			missing += !parse_hud_attr(7, "Turn", "T", 0, &you.num_turns);
			post_gold = 8;
		}

		missing += !parse_hud_attr(post_gold, "Exp Pool", "XP", &you.redraw_experience, &you.exp_available);
		missing += !parse_place(post_gold);
		if(missing)
			cerr << "HUD missing: " << missing << endl;

		// if mpmaxa.fg == LIGHTBLUE, we will detect it based on player symbol color, unless we are a pig
		berserk = stra.fg == LIGHTBLUE && hpmaxa.fg == LIGHTBLUE && mpmaxa.fg != LIGHTBLUE;
		you.duration[DUR_BERSERKER] = berserk ? INT_MAX : 0;

		vector<span> effect_spans;
		generate_spans(effect_spans, simple_cells[geom.hudp.y + post_gold + 3], geom.hudp.x, geom.hudsz.x, GENERATE_SPANS_WORDS);
		effects.clear();
		for(unsigned i = 0; i < effect_spans.size(); ++i)
			effects.insert(effect_spans[i].s);

		silence = (effects.count("Sil") + effects.count("Silence")) > 0;

//		print_stats();
		return missing;
	}

	void parse_messages()
	{
		vector<string> new_messages;
		unsigned i;
		for(i = geom.msgp.y; i < simple_cells.size() && !simple_cells[i].empty(); ++i) {
			ostringstream ss;
			for(unsigned j = 0; j < simple_cells[i].size(); ++j)
				ss << (char)simple_cells[i][j].c;
		}

		if(new_messages.empty())
			return;

		for(i = min(messages.size(), new_messages.size()); i >= 0; --i)
		{
			for(unsigned j = 0; j < i; ++j) {
				if(new_messages[j] != messages[messages.size() - i + j])
					goto nope;
			}
			break;
			nope:;
		}

		messages = new_messages;

		for(; i < messages.size(); ++i)
			message_signal(messages[i]);
	}

	void set_monster_tag(string& tag, monster_type mon, string name, bool is_pet)
	{
		monsterentry* me = get_monster_data(mon);

		const tag_pref pref = Options.tile_tag_pref;
		if (pref == TAGPREF_NONE)
			return;
		else if (pref == TAGPREF_TUTORIAL)
		{}
		else if(me ? !(me->bitfields & M_UNIQUE) : !isupper(name[0]))
			return;

		if(pref != TAGPREF_NAMED && is_pet)
			return;

		if(!name.empty())
			tag = name;
		else if(me)
			tag = me->name;
	}

	struct CellParsingContext
	{
		viewport_cell& vc;
		cattr_t a;
		unsigned fg;
		unsigned bg;
		string tag;
		bool berserk_red;

		CellParsingContext(viewport_cell& p_vc)
		: vc(p_vc), a(p_vc.a), fg(0), bg(0), berserk_red(false)
		{
		}
	};

	bool parse_cell_as_dchar(CellParsingContext& ctx)
	{
		wchar_t c = ctx.vc.c;
		cattr_t a = ctx.a;

		unsigned dc = NUM_DCHAR_TYPES;

		CrawlData::wchar_to_dchar_t::iterator dci = crawl_data.wchar_to_dchar[charset].find(c);
		if(dci == crawl_data.wchar_to_dchar[charset].end())
			return false;

		dc = dci->second;

		if(dc == DCHAR_SPACE)
			return true;

		if(ctx.vc.reverse)
			ctx.fg |= TILE_FLAG_S_UNDER;

		/* post-0.6, ~ and { have been swapped in ASCII.
		 * Assume that the most seen symbol is water.
		 *
		 * Use the Unicode charset for more precise display.
		 */
		if(dc == DCHAR_AMBIGUOUS_TILDE) {
			if(tildes_seen > curlies_seen)
				dc = DCHAR_WAVY;
			else
				dc = DCHAR_INVIS_EXPOSED;
		} else if(dc == DCHAR_AMBIGUOUS_CURLY) {
			if(tildes_seen <= curlies_seen)
				dc = DCHAR_WAVY;
			else
				dc = DCHAR_INVIS_EXPOSED;
		}

		if((dc == DCHAR_WALL || dc == DCHAR_WALL_MAGIC)
			&& (charset != CSET_ASCII || is_wall_instead_of_cloud[a.fg])) {
			ctx.bg |= ctx.berserk_red ? env.tile_default.wall : wall_tiles[a.fg];
			return true;
		}

		if(dc == DCHAR_FLOOR || dc == DCHAR_FLOOR_MAGIC) {
			if(ctx.berserk_red)
				ctx.bg |= env.tile_default.floor;
			else if(you.religion == GOD_SHINING_ONE && a.fg == YELLOW && env.floor_colour != YELLOW)
				ctx.bg |= env.tile_default.floor | TILE_FLAG_HALO | TILE_FLAG_SEEN;
			else if(you.religion == GOD_SHINING_ONE && silence && a.fg == LIGHTCYAN && env.floor_colour != YELLOW)
				ctx.bg |= env.tile_default.floor | TILE_FLAG_HALO | TILE_FLAG_SILENCED | TILE_FLAG_SEEN;
			else if(silence && a.fg == CYAN && env.floor_colour != CYAN)
				ctx.bg |= env.tile_default.floor | TILE_FLAG_SILENCED | TILE_FLAG_SEEN;
			else
				ctx.bg |= floor_tiles[a.fg] | TILE_FLAG_NO_HALO | TILE_FLAG_NOT_SILENCED;
			return true;
		}

		if(dc == DCHAR_WALL || dc == DCHAR_CLOUD) {
			ctx.fg |= cloud_tiles[a.fg];
			return true;
		}

		if(dc == DCHAR_INVIS_EXPOSED || dc == DCHAR_ITEM_DETECTED) {
			if(a.fg == GREEN || a.fg == DARKGREY) {
				/* TODO: can also be murky water disturbance, but that's quite rare, so let's just don't care for now */
				ctx.bg |= TILE_FLAG_MM_UNSEEN | TILE_FLAG_UNSEEN;
				ctx.fg |= TILE_UNSEEN_ITEM;
			} else // if(a.fg == BLUE)
				ctx.bg |= TILE_DNGN_SHALLOW_WATER_DISTURBANCE | TILE_FLAG_SEEN;
			return true;
		}

		if(dc == DCHAR_WAVY) {
			if(a.fg == DARKGRAY)
				ctx.bg |= unseen_wavy_tile;
			else if(a.fg == LIGHTGREEN)
				ctx.bg |= TILE_DNGN_SHALLOW_WATER_MURKY | TILE_FLAG_SEEN;
			else if(a.fg == GREEN)
				ctx.bg |= TILE_DNGN_DEEP_WATER_MURKY | TILE_FLAG_SEEN;
			else if(a.fg == BLUE)
				ctx.bg |= TILE_DNGN_DEEP_WATER | TILE_FLAG_SEEN;
			else if(a.fg == CYAN)
				ctx.bg |= TILE_DNGN_SHALLOW_WATER | TILE_FLAG_SEEN;
			else if(a.fg == RED)
				ctx.bg |= TILE_DNGN_LAVA | TILE_FLAG_SEEN;
			else
				return false;
			return true;
		}

		CrawlData::dchar_fg_to_tile_t::iterator ti = crawl_data.dchar_fg_to_tile.find(make_pair(dc, (unsigned)a.fg));

		if(ti != crawl_data.dchar_fg_to_tile.end()) {
			ctx.fg |= ti->second;
			return true;
		}

		CrawlData::dchar_to_color_to_feature_t::iterator fi = crawl_data.dchar_to_color_to_feature.find(dc);
		if(fi != crawl_data.dchar_to_color_to_feature.end()) {
			vector<unsigned> fgs;
			for(CrawlData::color_to_feature_t::iterator fgi = fi->second.begin(); fgi != fi->second.end(); ++fgi)
				fgs.push_back(fgi->first);
			int fgc;
			if(ctx.berserk_red)
				fgc = a.fg;
			else {
				fgc = crawl_data.guess_element_color(fgs, ctx.vc.fg_freqs);
				if(fgc < 0)
					fgc = a.fg;
			}
			if(fi->second.count(fgc)) {
				pair<dungeon_feature_type, unsigned> d = fi->second[fgc];

				if(d.first == DNGN_TREE && fgc == GREEN)
					ctx.bg |= TILE_DNGN_TREE_1;
				else if(d.first == DNGN_TREE && fgc == BROWN)
					ctx.bg |= TILE_DNGN_TREE; /* Tiles does this, but TILE_DNGN_TREE_RED[_1] may be better */
				else if(d.first == DNGN_TREE)
					ctx.bg |= TILE_DNGN_TREE;
				else
					ctx.bg |= tileidx_feature_type((dungeon_feature_type)d.first);

				if(a.fg == WHITE && (d.first == DNGN_STONE_STAIRS_DOWN_I || d.first == DNGN_STONE_STAIRS_UP_I))
					ctx.bg |= TILE_FLAG_NEW_STAIR;
				ctx.bg |= d.second;
				return true;
			}
		}

		if(dc == DCHAR_WALL_MAGIC || (dc >= DCHAR_FIRED_FLASK && dc <= DCHAR_FIRED_MISSILE)) {
			ctx.fg |= tileidx_zap(a.fg);
			return true;
		}

		ctx.fg &= ~TILE_FLAG_S_UNDER;
		return false;
	}

	/* TODO: this needs to become much more intelligent and properly recognize ambiguities */
	bool parse_cell_as_list_monster(CellParsingContext& ctx)
	{
		if(ctx.vc.c >= 0x80)
			return false;

		char c = (char)ctx.vc.c;
		cattr_t a = ctx.a;

		if(c >= 0x80)
			return false;

		mlist_char_a_to_monsters_t::iterator mli = remaining_mlist_char_a_to_monsters.find(make_pair((char)c, (uint8_t)a));
		if(mli == remaining_mlist_char_a_to_monsters.end())
			return false;

		parsed_monster pm = mli->second.front();

		mli->second.pop_front();
		if(mli->second.empty())
			remaining_mlist_char_a_to_monsters.erase(mli);
		ctx.fg |= pm.tile;
		ctx.bg |= TILE_FLAG_SEEN;

		set_monster_tag(ctx.tag, pm.type, pm.name, ctx.fg & TILE_FLAG_PET);

		return true;
	}

	/* TODO: this needs to become much more intelligent and be smarter about ambiguities */
	bool parse_cell_as_glyph_monster(CellParsingContext& ctx)
	{
		if(ctx.vc.c >= 0x80)
			return false;

		char c = (char)ctx.vc.c;
		if(!(isalnum(c) || c == '&' || c == '@' || c == '*' || c == ';'))
			return false;

		cattr_t a = ctx.a;

		pair<monster_type, unsigned> monster;
		monster.first = NUM_MONSTERS;

		if(!ctx.berserk_red && a.fg != DARKGRAY) {
			int count = crawl_data.char_fg_to_monsters.count(make_pair((char)c, (unsigned)a.fg));
			if(count == 1)
				monster = crawl_data.char_fg_to_monsters.find(make_pair((char)c, (unsigned)a.fg))->second;
			else {
				pair<CrawlData::char_fg_to_monsters_t::iterator, CrawlData::char_fg_to_monsters_t::iterator> range;

				range = crawl_data.char_fg_to_monsters.equal_range(make_pair((char)c, (unsigned)a.fg));
				cerr << "Ambiguous monster glyph, candidates are:";
				for(CrawlData::char_fg_to_monsters_t::iterator i = range.first; i != range.second; ++i) {
					monsterentry* me = get_monster_data(i->second.first);
					if(i != range.first)
						cerr << ',';
					cerr << ' ' << (me ? me->name : "unknown monster");
				}
				cerr << endl;
			}
		}

		if(monster.first == NUM_MONSTERS) {
			monster = crawl_data.char_to_base_monster[c];
			ctx.bg |= TILE_FLAG_MM_UNSEEN;
		}

		if(monster.first == NUM_MONSTERS)
			return false;

		ctx.fg |= monster.second &~(TILE_FLAG_SEEN | TILE_FLAG_UNSEEN);
		ctx.bg |= monster.second & (TILE_FLAG_SEEN | TILE_FLAG_UNSEEN);

		if(ctx.vc.reverse)
			ctx.fg |= TILE_FLAG_S_UNDER;

		crawl_data.adjust_monster_tile_bg(ctx.fg, a.bg);

		if(!(ctx.bg & TILE_FLAG_MM_UNSEEN))
			set_monster_tag(ctx.tag, monster.first, "", ctx.fg & TILE_FLAG_PET);
		return true;
	}

	void parse_cell(unsigned& fg, unsigned& bg, string& tag, viewport_cell& vc, bool is_player)
	{
		CellParsingContext ctx(vc);
		ctx.bg |= vc.bg_flags;

		if(berserk && ctx.a.fg == RED) {
			ctx.a.fg = LIGHTGREY;
			ctx.berserk_red = true;
		}

		if(vc.c == L' ')
		{}
		else if(is_player) {
			/* avoid missing player if we failed to parse the species somehow  */
			if(ctx.fg == TILEP_PLAYER && you.species == SP_UNKNOWN)
				ctx.fg |= TILEP_MONS_HUMAN;
			else
				ctx.fg |= tileidx_player(you.char_class);
		} else {
			bool prefer_dchar = false;

			/* prefer statues or orcish idols to clay golems */
			if(vc.c == L'8' && ctx.a.fg == BROWN)
				prefer_dchar = true;

			if(0
				|| parse_cell_as_list_monster(ctx)
				|| (!prefer_dchar && parse_cell_as_glyph_monster(ctx))
				|| parse_cell_as_dchar(ctx)
				|| (prefer_dchar && parse_cell_as_glyph_monster(ctx))
			)
			{}
			else {
				ctx.bg |= TILE_DNGN_ERROR;
				ctx.fg |= TILE_ERROR;
			}
		}

		if(is_player)
			ctx.bg &=~ TILE_FLAG_RAY | TILE_FLAG_RAY_OOR;

		 if(you.where_are_you == BRANCH_SHOALS) {
			 if((ctx.bg & TILE_FLAG_MASK) == TILE_DNGN_SHALLOW_WATER)
				 ctx.bg = (ctx.bg & ~TILE_FLAG_MASK) | TILE_SHOALS_SHALLOW_WATER;
			 else if((ctx.bg & TILE_FLAG_MASK) == TILE_DNGN_SHALLOW_WATER_DISTURBANCE)
				 ctx.bg = (ctx.bg & ~TILE_FLAG_MASK) | TILE_SHOALS_SHALLOW_WATER_DISTURBANCE;
			 else if((ctx.bg & TILE_FLAG_MASK) == TILE_DNGN_DEEP_WATER)
				 ctx.bg = (ctx.bg & ~TILE_FLAG_MASK) | TILE_SHOALS_DEEP_WATER;
		 }

		 fg = ctx.fg;
		 bg = ctx.bg;
		 tag = ctx.tag;
	}

	static void print_wchar(ostream& out, wchar_t wc)
	{
		if(wc < 0x20)
			out << "\\x" << hex << setw(2) << setfill('0') << (unsigned)wc << dec;
		else if(wc >= 0x80) {
			char buf[MB_LEN_MAX + 1];
			setlocale(LC_CTYPE, "");
			int len = wctomb(buf, wc);
			if(len >= 0) {
				buf[len] = 0;
				out << buf;
			} else
				out << "\\u" << hex << setw(4) << setfill('0') << (unsigned)wc << dec;
		}
		else if(wc == L'\\')
			out << "\\\\";
		else
			out << (char)wc;
	}

	void parse_viewport()
	{
		if(viewport.size() < (unsigned)geom.viewsz.y)
			viewport.resize(geom.viewsz.y);

		for(unsigned y = 0; y < (unsigned)geom.viewsz.y; ++y) {
			unsigned maxx = min((unsigned)geom.viewsz.x, screen->cols[y]);
			if(viewport[y].size() < (unsigned)geom.viewsz.x)
				viewport[y].resize(geom.viewsz.x);

			unsigned x;
			for(x = 0; x < maxx; ++x) {
				wcell c = screen->screen[y][x];
				CrawlData::wchar_to_dchar_t::iterator dci = crawl_data.wchar_to_dchar[charset].find(c.c);
				bool maybe_ray = false;
				if(dci != crawl_data.wchar_to_dchar[charset].end()) {
					unsigned char dc = dci->second;
					if(dc == DCHAR_WALL_MAGIC || dc == DCHAR_FIRED_ZAP || dc == DCHAR_FIRED_MISSILE)
						maybe_ray = true;
				} else if(c.a.s.reverse)
					maybe_ray = true;

				if(x == (unsigned)screen->int_cursor.x && y == (unsigned)screen->int_cursor.y)
					c.a.s.int_cursor = 1;
				viewport[y][x].update(c, maybe_ray);
			}

			for(; x < (unsigned)geom.viewsz.x; ++x) {
				wcell c;
				c.c = L' ';
				c.a.v = 0;
				viewport[y][x].update(c, false);
			}
		}
	}

	unsigned parse_player_color(cattr_t a)
	{
		int color = a.fg;
		if(color == BLACK)
			color = a.bg;
		else if(a.bg != BLACK)
			return 0;

		if(color == you.colour)
			return LIGHTGREY;
		else if(color == BLUE || color == CYAN || color == RED) /* deep water, shallow water, berserk */
			return color;
		else
			return 0;
	}

	void detect_wall_floor_colors()
	{
		bool greenable_vaults = you.level_type == LEVEL_DUNGEON && you.where_are_you == BRANCH_VAULTS && branch_depth > 6 && unseen_wall_color != GREEN;

		if(!(greenable_vaults || you.level_type == LEVEL_ABYSS || you.level_type == LEVEL_PANDEMONIUM || you.level_type == LEVEL_PORTAL_VAULT))
			return;

		unsigned wall_freqs[16];
		unsigned floor_freqs[16];

		for(unsigned i = 0; i < 16; ++i) {
			wall_freqs[i] = 0;
			floor_freqs[i] = 0;
		}

		for(int y = 0; y < geom.viewsz.y; ++y) {
			for(int x = 0; x < geom.viewsz.x; ++x) {
				CrawlData::wchar_to_dchar_t::iterator dci = crawl_data.wchar_to_dchar[charset].find(viewport[y][x].c);
				if(dci != crawl_data.wchar_to_dchar[charset].end()) {
					unsigned dc = dci->second;

					if(dc == DCHAR_WALL || dc == DCHAR_WALL_MAGIC)
						++wall_freqs[viewport[y][x].a.fg];
					else if(dc == DCHAR_FLOOR || dc == DCHAR_FLOOR_MAGIC)
						++floor_freqs[viewport[y][x].a.fg];
				}
			}
		}

		/* double all them so that special kinds are lower priority than one element sets */
		for(unsigned i = 0; i < 16; ++i)
			wall_freqs[i] *= 2;

		if(wall_freqs[DARKGRAY])
			wall_freqs[DARKGRAY] = 1;

		/* ignore stone and metal colored walls outside Pandemonium since they are likely actually stone or metal */
		if(you.level_type != LEVEL_PANDEMONIUM) {
			if(wall_freqs[CYAN])
				wall_freqs[CYAN] = 1;
			if(wall_freqs[LIGHTGREY])
				wall_freqs[LIGHTGREY] = 1;
		}

		if(floor_freqs[DARKGRAY])
			floor_freqs[DARKGRAY] = 1;

		/* ignore halo */
		if(you.religion == GOD_SHINING_ONE) {
			if(floor_freqs[YELLOW])
				floor_freqs[YELLOW] = 1;
			if(silence && floor_freqs[LIGHTCYAN])
				floor_freqs[LIGHTCYAN] = 1;
		}

		if(silence && floor_freqs[CYAN])
			floor_freqs[CYAN] = 1;

		unsigned wall_color = BROWN;
		unsigned floor_color = LIGHTGREY;
		for(unsigned i = 1; i < 16; ++i) {
			if(wall_freqs[i] > wall_freqs[wall_color])
				wall_color = i;
			if(floor_freqs[i] > floor_freqs[floor_color])
				floor_color = i;
		}

		if(greenable_vaults) {
			if(wall_color == GREEN) {
				unseen_wall_color = GREEN;
				wall_tiles[DARKGREY] = wall_tiles[unseen_wall_color] | TILE_FLAG_UNSEEN;
			}
		} else {
			if(wall_color != env.rock_colour || floor_color != env.floor_colour) {
				cout << "Detected " << colour_to_str(wall_color) << " walls and " << colour_to_str(floor_color) << " floors" << endl;
				env.rock_colour = wall_color;
				env.floor_colour = floor_color;
				init_walls_clouds_floors();
			}
		}
	}

	void detect_player()
	{
		vector<coord_def> player_pos_cands;

		if(screen->cursor.x >= 0 && screen->cursor.y >= 0 && screen->cursor.x < geom.viewsz.x && screen->cursor.y < geom.viewsz.y) {
			bool ok = true;
			wchar_t c = viewport[screen->cursor.y][screen->cursor.x].c;
			if(c == L'@') {
				you.attribute[ATTR_TRANSFORMATION] = TRAN_NONE; /* TODO: or blade hands... */
				you.colour = LIGHTGREY;
			} else if(c == L'8') {
				you.attribute[ATTR_TRANSFORMATION] = TRAN_STATUE;
				you.colour = LIGHTGREY;
			} else if(c == L'L') {
				you.attribute[ATTR_TRANSFORMATION] = TRAN_LICH;
				you.colour = LIGHTGREY;
			} else if(c == L'b') {
				you.attribute[ATTR_TRANSFORMATION] = TRAN_BAT;
				you.colour = (you.species == SP_VAMPIRE ? DARKGREY : LIGHTGREY);
			} else if(c == L's') {
				you.attribute[ATTR_TRANSFORMATION] = TRAN_SPIDER;
				you.colour = BROWN;
			} else if(c == L'D') {
				you.attribute[ATTR_TRANSFORMATION] = TRAN_DRAGON;
				you.colour = GREEN;
			} else if(c == L'I') {
				you.attribute[ATTR_TRANSFORMATION] = TRAN_ICE_BEAST;
				you.colour = WHITE;
			} else if(c == L'h') {
				you.attribute[ATTR_TRANSFORMATION] = TRAN_PIG;
				you.colour = RED;
			} else
				ok = false;
			if(ok) {
				cattr_t a = viewport[screen->cursor.y][screen->cursor.x].a;
				unsigned color = parse_player_color(a);
				if(color == you.colour)
				{}
				else if(color == RED) {
					berserk = true;
					you.duration[DUR_BERSERKER] = INT_MAX;
				} else
					cerr << "Unexpected player character color: " << colour_to_str(a.fg) << '/' << colour_to_str(a.bg) << " (expected " << colour_to_str(you.colour) << ")" << endl;
				you.symbol = (char)c;
				player_pos_cands.push_back(screen->cursor);
			}
		}

		/* assume the player hasn't transformed in the meantime (should be impossible) */
		if(player_pos_cands.empty() && last_player_pos.x >= 0) {
			if(viewport[last_player_pos.y][last_player_pos.x].c == you.symbol) {
				if(parse_player_color(viewport[last_player_pos.y][last_player_pos.x].a))
					player_pos_cands.push_back(player_pos);
			}
		}

		if(player_pos_cands.empty()) {
			for(unsigned y = 0; y < (unsigned)geom.viewsz.y; ++y) {
				for(unsigned x = 0; x < (unsigned)geom.viewsz.x; ++x) {
					if(viewport[y][x].c == you.symbol) {
						if(parse_player_color(viewport[y][x].a)) {
							player_pos_cands.push_back(coord_def(x, y));
						}
					}
				}
			}
		}

		if(player_pos_cands.empty()) {
			player_pos = coord_def(-1, -1);
			if(last_player_pos.x >= 0) {
				double_center_pos = last_player_pos;
				double_center_pos *= 2;
			} else {
				double_center_pos = geom.viewsz;
				double_center_pos -= 1;
			}
		} else {
			if(player_pos_cands.size() == 1) {
				last_player_pos = player_pos = player_pos_cands[0];
				double_center_pos = player_pos;
				double_center_pos *= 2;
			} else {
				coord_def br = player_pos_cands[0];
				coord_def ul = player_pos_cands[0];
				player_pos = coord_def(-1, -1);
				for(unsigned i = 1; i < player_pos_cands.size(); ++i) {
					ul.x = min(ul.x, player_pos_cands[i].x);
					ul.y = min(ul.y, player_pos_cands[i].y);
					br.x = max(br.x, player_pos_cands[i].x);
					br.y = max(br.y, player_pos_cands[i].y);
				}
				double_center_pos = ul + br;
			}
		}
	}

	void detect_dcharset()
	{
		for(int y = 0; y < geom.viewsz.y; ++y) {
			for(int x = 0; x < geom.viewsz.x; ++x) {
				if(viewport[y][x].c == L'~')
					++tildes_seen;
				else if(viewport[y][x].c == L'{')
					++curlies_seen;
			}
		}
	}

	void tile_viewport()
	{
		//cout << "Viewport: " << geom.viewsz.x << "x" << geom.viewsz.y << endl;

		/*
		for(unsigned i = 0; i < screen->changes.size(); ++i)
		{
			int y = screen->changes[i].y;
			int x = screen->changes[i].x;
		*/

		CrawlFragment frag(geom.viewsz.x, geom.viewsz.y);

		vector<pair<coord_def, string> > tags;

		for(unsigned y = 0; y < geom.viewsz.y; ++y) {
			for(unsigned x = 0; x < geom.viewsz.x; ++x) {
				pair<screen_buffer_t, screen_buffer_t>& p = frag(x, y);
				string tag;
				viewport_cell& vc = viewport[y][x];
				parse_cell(p.first, p.second, tag, vc, x == player_pos.x && y == player_pos.y);

				if(p.first == TILE_ERROR || p.second == TILE_DNGN_ERROR) {
					cerr << "Unknown character/fg: ";
					print_wchar(cerr, vc.c);
					cerr <<  ' ' << colour_to_str(vc.a.fg);
					if(vc.a.bg != BLACK)
						cerr << '/' << colour_to_str(vc.a.bg);
					cerr << endl;
				}

				if(!tag.empty())
					tags.push_back(make_pair(coord_def(x, y), tag));
			}
		}

		finalizer->finalize(frag, floor_tiles, wall_tiles, player_pos, (env.floor_colour != DARKGRAY) && !berserk, silence);

		if(!remaining_mlist_char_a_to_monsters.empty())
			cerr << "Unable to match " << remaining_mlist_char_a_to_monsters.size() << " monsters list entries" << endl;

		update_dungeon_signal(frag, env.tile_default.floor, env.tile_default.wall, tags, double_center_pos);
	}

	enum prefix_type
	{
	    P_NOTMSG = -1,
	    P_NONE,
	    P_TURN_START,
	    P_TURN_END,
	    P_NEW_CMD, // new command, but no new turn
	    P_NEW_TURN,
	    P_FULL_MORE,   // single-character more prompt (full window)
	    P_OTHER_MORE   // the other type of --more-- prompt
	};

	pair<int, string> parse_message_line(unsigned line)
	{
		pair<int, string> notmsg = make_pair(-1, "");
		int prefix;

		if(screen->cols[line] < 2)
			return notmsg;

		if(screen->screen[line][1].c == L' ')
			return notmsg;

		char c = screen->screen[line][0].c;
		unsigned fg = screen->screen[line][0].a.s.fg;

		if(c == L' ')
			prefix = P_NONE;
		else if(c == L'_') {
			if(fg == LIGHTGRAY)
				prefix = P_NEW_TURN;
			else if(fg == DARKGRAY)
				prefix = P_NEW_CMD;
			else
				prefix = P_NONE; /* what is this??? seen a blueish one... */
		} else if(c == L'-') {
			/* if(fg == LIGHTGRAY) */
			prefix = P_TURN_START;
		} else if(c == L'+') {
			if(fg == LIGHTRED)
				prefix = P_OTHER_MORE;
			else /* channel_to_colour */
				prefix = P_FULL_MORE;
		} else {
			prefix = P_NONE;
		}

		ostringstream ss;

		for(unsigned x = 1; x < simple_cells[line].size(); ++x) {
			if(simple_cells[line][x].a.bg != BLACK)
				return notmsg;

			char mc = (char)screen->screen[line][x].c;
			ss << mc;
		}

		string s = ss.str();
		s = trim_string(s);
		return make_pair(prefix, s);
	}

	unsigned view_lb;
	unsigned view_ub;

#define VIEW 1
#define MSG 2
#define MORE 4

	int find_viewport_height() {
		vector<int> lines_status;
		lines_status.resize(screen->cols.size());
		int i;
		for(i = 0; i < (int)screen->cols.size(); ++i) {
			unsigned line_status;
			vector<span> spans;
			generate_spans(spans, simple_cells[i]);
			if(simple_cells[i].empty())
				line_status = VIEW;
			else if(spans.size() == 1 && spans[0].s == "--more--")
				line_status = MORE;
			else {
				pair<int, string> msgline = parse_message_line(i);
				if(msgline.first < 0)
					line_status = VIEW;
				else if(msgline.second == "Unknown command.")
					line_status = MSG;
				else
					line_status = VIEW | MSG;

				/* view lines have spaces between the view and potential monster list */
				if((unsigned)geom.hudp.x <= simple_cells[i].size() && simple_cells[i][geom.hudp.x - 1].c != ' ')
					line_status &=~ VIEW;
			}

			/* view lines are valid */
			if(line_status & VIEW) {
				for(int x = 0; x < geom.viewsz.x; ++x) {
					if(screen->screen[i][x].c == L' ')
						continue;

					if(screen->screen[i][x].a.s.reverse)
						continue;

					if(crawl_data.wchar_to_dchar[charset].count(screen->screen[i][x].c))
						continue;

					if(screen->screen[i][x].c <= 0x80) {
						unsigned fg = screen->screen[i][x].a.s.fg;
						if(fg == BLACK)
							fg = screen->screen[i][x].a.s.bg;
						if(crawl_data.char_fg_to_monsters.count(make_pair((char)screen->screen[i][x].c, (unsigned char)fg)))
							continue;

						if(((berserk && fg == RED) || fg == DARKGRAY)
								&& crawl_data.char_to_base_monster[(unsigned char)screen->screen[i][x].c].first != NUM_MONSTERS)
							continue;
					}

					line_status &=~ VIEW;
					break;
				}
			}

			lines_status[i] = line_status;
		}

		unsigned lb, ub;
		for(i = 0; i < (int)screen->cols.size(); ++i) {
			if(!(lines_status[i] & VIEW))
				break;
		}
		ub = i;

		for(i = screen->cols.size() - 1; i >= 0; --i) {
			if(lines_status[i] != 0 && lines_status[i] != MORE)
				break;
		}

		for(; i >= 0; --i) {
			if(!(lines_status[i] & MSG))
				break;
		}
		lb = i + 1;

		bool update = true;
		if(ub < lb) {
			cerr << "No possible choice for viewport height (" << lb << " -> " << ub << "): attempting to work around that" << endl;

			/* this seems the best option we have */
			swap(lb, ub);
			update = false;
		}


		/* find intersection; if intersection empty, set lb = ub = nearest point to existing range */

		unsigned b;
		unsigned new_lb = max(lb, view_lb);
		unsigned new_ub = min(ub, view_ub);

		if(new_ub < new_lb) {
			new_lb = lb;
			new_ub = ub;
			if(ub < view_lb)
				b = ub;
			else
				b = lb;
		}
		else
			b = new_ub;

		if(update && (new_lb != view_lb || new_ub != view_ub)) {
			/* only update if any messages are there, to try to avoid updating on partial frames */
			if((new_ub + 1) < (int)screen->cols.size()) {
				cerr << "Resetting viewport bounds to " << lb << " -> " << ub << endl;
				view_lb = new_lb;
				view_ub = new_ub;
			}
		}

		return b;
	}

	void add_monster(char c, cattr_t a, const string& s, cattr_t sa, int health)
	{
		if(berserk) {
			a.fg = LIGHTGREY;
			a.bg = BLACK; // TODO: is this correct?
		}

		// TODO: maybe should do this before
		if(a.fg == BLACK)
			a.fg = a.bg;

		parsed_monster pm = crawl_data.parse_monster(c, a, s, sa, health);
		mlist_char_a_to_monsters[make_pair(c, (uint8_t)a)].push_back(pm);
	}

	void parse_monster_list()
	{
		incomplete_mlist = false;
		mlist_char_a_to_monsters.clear();
		remaining_mlist_char_a_to_monsters.clear();

		if(geom.mlistsz.x < 5) {
			incomplete_mlist = true;
			return;
		}

		for(int i = 0; i < geom.mlistsz.y; ++i) {
			vector<simple_cell>& cells = simple_cells[geom.mlistp.y + i];
			unsigned x0 = geom.mlistp.x;

			int elli;
			for(elli = geom.mlistsz.x - 1; elli >= 0 && cells[x0 + elli].c == ' '; --elli)
			{}

			if(elli >= 2 && cells[x0 + elli].c == '.' && cells[x0 + elli - 1].c == '.' && cells[x0 + elli - 2].c == '.') {
				incomplete_mlist = true;
				elli -= 3;
			}
			++elli;

			if(cells[x0 + 1].c == ' ' && cells[x0 + 3].c == ' ') {
				int mdam = -1;
				switch(cells[x0 + 2].a.bg) {
				case RED:
					mdam = MDAM_SEVERELY_DAMAGED;
					break;
				case MAGENTA:
					mdam = MDAM_HEAVILY_DAMAGED;
					break;
				case BROWN:
					mdam = MDAM_MODERATELY_DAMAGED;
					break;
				case GREEN:
					mdam = MDAM_OKAY;
					break;
				case CYAN:
				default:
					break;
				}

				vector<span> rest;
				generate_spans(rest, cells, x0 + 4, elli - 4);

				if(rest.empty())
					cerr << "Monster name missing in monster list" << endl;
				else
					add_monster(cells[x0].c, cells[x0].a, rest[0].s, rest[0].a, mdam);
			} else {
				vector<simple_cell> glyphs;
				for(unsigned j = 0; j < (unsigned)geom.mlistsz.x; ++j) {
					simple_cell ca = cells[x0 + j];
					if(ca.c == ' ')
						break;
					glyphs.push_back(ca);
				}

				vector<span> rest;
				generate_spans(rest, cells, x0 + glyphs.size() + 1, elli - (glyphs.size() + 1));

				if(rest.size() > 1)
					cerr << "Warning: more than one monster list name span" << endl;
				if(!rest.size()) {
					cerr << "Only glyphs in monster entry" << endl;
					continue;
				}

				int count = 1;
				cattr_t sa = rest[0].a;
				stringstream ss(rest[0].s);
				ss >> count;
				if(ss.fail()) {
					count = 1;
					cerr << "No count in monster list entry" << endl;
				}

				string s;
				getline(ss, s);
				s = trim_string(s);

				if((int)glyphs.size() < count)
					incomplete_mlist = true;

				for(unsigned j = 0; j < glyphs.size(); ++j)
					add_monster(glyphs[j].c, glyphs[j].a, s, sa, 0);
			}
		}

		remaining_mlist_char_a_to_monsters = mlist_char_a_to_monsters;
	}

	void parse_dungeon_screen() {
		int i;

		// geom.hudp is already set by the caller
		// do anything we can without knowing the start of messages first,
		// then heuristically determine that

		geom.viewp.x = 0;
		geom.viewp.y = 0;

		geom.viewsz.x = geom.hudp.x - 1;

		geom.hudsz.x = 0;
		for(i = 0;; ++i)
		{
			int len = simple_cells[i].size() - geom.hudp.x;
			if(len <= 0)
				break;
			if(len > geom.hudsz.x)
				geom.hudsz.x = len;
		}
		geom.hudsz.y = i;

		/* must do this early to set berserk for find_viewport_height() monster parsing */
		parse_hud();


		int viewh = find_viewport_height();
		//cout << "View height: " << viewh << endl;

		geom.viewsz.y = viewh;

		geom.msgp.x = 0;
		geom.msgp.y = viewh;

		geom.mlistp.x = geom.hudp.x;

		geom.mlistsz.x = 0;
		for(i = geom.hudp.y + geom.hudsz.y + 1;  i < (int)geom.msgp.y; ++i) {
			int len = simple_cells[i].size() - geom.mlistp.x;
			if(len > 0)
				break;
		}
		geom.mlistp.y = i;
		for(;  i < (int)geom.msgp.y; ++i)
		{
			int len = simple_cells[i].size() - geom.mlistp.x;
			if(len <= 0)
				break;

			if(len > geom.mlistsz.x)
				geom.mlistsz.x = len;
		}
		geom.mlistsz.y = i - geom.mlistp.y;

		geom.msgsz.x = 0;
		geom.msgsz.y = 0;

		for(i = geom.msgp.y; i < (int)screen->cols.size(); ++i) {
			int len = screen->cols[i];
			if(len > 0)
				geom.msgsz.y = i - geom.msgp.y + 1;
			if(len > geom.msgsz.x)
				geom.msgsz.x = len;
		}

		parse_monster_list();
		parse_messages();
		parse_viewport();

		detect_player();
		detect_dcharset();
		detect_wall_floor_colors();
	}

	void clear()
	{
		    env.grid.init(DNGN_UNSEEN);
		    env.pgrid.init(0);
		    env.map_knowledge.init(map_cell());

		    env.igrid.init(NON_ITEM);
		    env.mgrid.init(NON_MONSTER);

		    for (int i = 0; i < MAX_ITEMS; ++i)
		        init_item(i);

		    // Empty messaging string.
		    info[0] = 0;

		    for (int i = 0; i < MAX_MONSTERS; ++i)
		        env.mons[i].reset();
	}

	void display_on_region(coord_def p, coord_def sz, GotoRegion region)
	{
		unsigned ox = p.x;
		unsigned oy = p.y;
		unsigned w = sz.x;
		unsigned h = sz.y;

		vector<vector<simple_cell> > region_cells;
		region_cells.resize(h);

		for(unsigned i = 0; i < h; ++i)
		{
			if((oy + i) >= simple_cells.size())
				break;

			vector<simple_cell>& cells = simple_cells[oy + i];
			vector<simple_cell>& outcells = region_cells[i];

			for(unsigned j = 0; j < w; ++j) {
				if(ox + j >= cells.size())
					break;
				simple_cell c = cells[ox + j];
				if(region == GOTO_STAT && (i == 2 || i == 3)) {
					if(c.c == '-' || c.c == '=') {
						c.a.bg = c.a.fg;
						c.a.fg = 0;
						c.c = ' ';
					}
				}
				outcells.push_back(c);
			}
		}

		update_text_region_signal(region, region_cells);
	}

	void display_on_crt()
	{
		display_on_region(geom.termp, geom.termsz, GOTO_CRT);
	}

	void display_on_dungeon()
	{
		tile_viewport();
		display_on_region(geom.hudp, geom.hudsz, GOTO_STAT);
		display_on_region(geom.msgp, geom.msgsz, GOTO_MSG);
	}

	void handle_screen_update(Screen& the_screen)
	{
		init();

		/* TODO: don't do all this every time... */
		clear();

		have_more = false;

		screen->compute_simple_cells(simple_cells, &crawl_data.unicode_replacements);
		geom.termp.x = 0;
		geom.termp.y = 0;

		geom.termsz.x = 0;
		for(unsigned i = 0; i < screen->cols.size(); ++i) {
			if(screen->cols[i] > (unsigned)geom.termsz.x)
				geom.termsz.x = screen->cols[i];
		}
		geom.termsz.y = screen->cols.size();

		enum screen_type_t new_screen_type = SCREEN_CRT;

		vector<span> line0, line1;
		if(simple_cells.size() >= 2) {
			generate_spans(line0, simple_cells[0]);
			generate_spans(line1, simple_cells[1]);

			/* XXX: what if the first viewport line ends with something yellow?!? */
			if(!line0.empty() && !line1.empty()) {
				const span& span0 = line0.back();
				const span& span1 = line1.back();
				if(span0.a.fg == YELLOW && span0.a.bg == BLACK && span1.a.fg == YELLOW && span1.a.bg == BLACK) {
					size_t lev = span1.s.rfind("Level ");
					if(lev != string::npos && (lev == 0 || span1.s[lev - 1] == ' ')) {
						//cout << "MScreen: " << span.s << endl;
						geom.hudp = coord_def(span1.x + lev, 0);
						new_screen_type = SCREEN_DUNGEON;
					}
				}
			}
		}

		if(screen_type != new_screen_type) {
			screen_type = new_screen_type;
			screen_type_changed_signal(new_screen_type);
		}

		if(screen_type == SCREEN_DUNGEON)
			parse_dungeon_screen();

		if(screen_type == SCREEN_DUNGEON && tiles_enabled)
			display_on_dungeon();
		else
			display_on_crt();
	}
};

CrawlParser* CrawlParser::create(Screen* screen)
{
	return new CrawlParserImpl(screen);
}
