/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#include "AppHdr.h"
#include "cell.h"
#include <sstream>

using namespace std;

void generate_spans(vector<span>& res, const simple_cell* line, size_t size, unsigned flags)
{
	cattr_t attr;
	int spaces = 0;
	ostringstream ss;
	bool in_span = false;
	attr.fg = 0;
	attr.bg = 0;
	for(unsigned x = 0;; ++x)
	{
		wchar_t c;
		cattr_t a;
		if(x < size) {
			c = line[x].c;
			a = line[x].a;
		} else {
			c = 0;
			a.fg = 0;
			a.bg = 0;
		}

		if(in_span && (0
				|| a.bg != attr.bg
				|| (c != L' ' && a.fg != attr.fg)
				|| c == 0
				|| ((flags & GENERATE_SPANS_WORDS) && c == L' ' && x > 0 && line[x - 1].c != L' ' )
			)) {
			span span;
			span.s = ss.str();
			span.x = x - span.s.size() - spaces;
			span.a = attr;
			res.push_back(span);

			spaces = 0;
			ss.str(string());
			in_span = false;
		}

		if(!c)
			break;

		if(in_span) {
			if(!(flags & GENERATE_SPANS_INCLUDE_SPACES) && c == L' ' && a.bg == BLACK) {
				++spaces;
			} else {
				for(int i = 0; i < spaces; ++i)
					ss << ' ';
				spaces = 0;
				ss << (char)c;
			}
		} else {
			if((flags & GENERATE_SPANS_INCLUDE_SPACES) || c != L' ' || a.bg != BLACK) {
				ss << (char)c;
				attr = a;
				in_span = true;
			}
		}
	}
}
