/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#include "AppHdr.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <iomanip>
#include <stdexcept>

#ifdef HAVE_LIBSSH
#include "connection.h"
#include <libssh/libssh.h>

using namespace std;

// Dear libssh guys, where is privatekey_from_string?!?

ssh_private_key my_privatekey_from_string(ssh_session session, const char* privkey)
{
#ifdef WIN32
	char privkey_file[MAX_PATH + 2];
	char temp_path[MAX_PATH + 2];
	GetTempPathA(MAX_PATH + 1, temp_path);
	GetTempFileNameA(temp_path, "sshkey", 0, privkey_file);
	FILE* fp = fopen(privkey_file, "wb");
	fwrite(privkey, 1, strlen(privkey), fp);
	fclose(fp);
	ssh_private_key privkey_obj = privatekey_from_file(session, privkey_file, 0, 0);
	_unlink(privkey_file);
#else
	char privkey_file[] = "/tmp/cao_key.XXXXXX";
	int fd = mkstemp(privkey_file);
	write(fd, privkey, strlen(privkey));
	close(fd);
	ssh_private_key privkey_obj = privatekey_from_file(session, privkey_file, 0, 0);
	unlink(privkey_file);
#endif
	return privkey_obj;
}

struct Ssh : public Connection
{
	ssh_session session;
	ssh_channel channel;

	void throw_ssh_error(const string& s)
	{
		throw runtime_error(s + ": " + ssh_get_error(session));
	}

	Ssh(const string& host, const char* host_hash, const char* username, const char* password, const char* privkey, unsigned rows, unsigned cols)
	{
		session = ssh_new();
		//int verbosity = SSH_LOG_FUNCTIONS;
		//ssh_options_set(session, SSH_OPTIONS_LOG_VERBOSITY, &verbosity);
		if(0
			|| ssh_options_set(session, SSH_OPTIONS_HOST, host.c_str()) < 0
			|| ssh_options_set(session, SSH_OPTIONS_USER, username) < 0
			|| ssh_options_set(session, SSH_OPTIONS_COMPRESSION_C_S, "none") < 0
			|| ssh_options_set(session, SSH_OPTIONS_COMPRESSION_S_C, "zlib,none") < 0
			)
			throw_ssh_error("Setting SSH options failed");

		if(ssh_connect(session) < 0)
			throw_ssh_error("SSH connection failed");

		unsigned char* hash = 0;
		int hash_len = ssh_get_pubkey_hash(session, &hash);

		if(hash_len != 16 || !host_hash || memcmp(host_hash, hash, 16))
			throw std::runtime_error("Host key hash mismatch: someone may be impersonating the remote Crawl server");

		if(ssh_userauth_none(session, username) != SSH_AUTH_SUCCESS) {
			if(!password || ssh_userauth_password(session, username, password) != SSH_AUTH_SUCCESS) {
				int ret = SSH_AUTH_DENIED;
				if(privkey) {
					ssh_private_key privkey_obj = my_privatekey_from_string(session, privkey);
					ssh_public_key pubkey = publickey_from_privatekey(privkey_obj);
					ssh_string pubkey_str = publickey_to_string(pubkey);
					ret = ssh_userauth_pubkey(session, username, pubkey_str, privkey_obj);
					string_free(pubkey_str);
					publickey_free(pubkey);
					privatekey_free(privkey_obj);
				}
				if(ret != SSH_AUTH_SUCCESS)
					throw_ssh_error("SSH authentication failed");
			}
		}

		channel = channel_new(session);
		if(channel_open_session(channel) < 0)
			throw_ssh_error("Unable to open a SSH channel");
		if(channel_request_pty_size(channel, "xterm", cols, rows) < 0)
			throw_ssh_error("Unable to request an SSH pty");
		if(channel_request_shell(channel) < 0)
			throw_ssh_error("Unable to request an SSH shell");
	}

	// XXX: are we SURE these are threadsafe?!

	virtual ptrdiff_t retrieve(void* buf, size_t size)
	{
		return channel_read(channel, buf, size, 0);
	}

	virtual ptrdiff_t transmit(const void* buf, size_t size)
	{
		return channel_write(channel, buf, size);
	}

	~Ssh()
	{
		channel_close(channel);
		channel_free(channel);
		ssh_disconnect(session);
		ssh_free(session);
	}
};

Connection* create_ssh_connection(const string& host, const char* host_hash, const char* username, const char* password, const char* privkey, unsigned rows, unsigned cols)
{
	return new Ssh(host, host_hash, username, password, privkey, rows, cols);
}

#endif
