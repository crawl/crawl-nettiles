/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#ifndef SERVER_DATA_H_
#define SERVER_DATA_H_

enum server_type
{
	SERVER_UNKNOWN,
	SERVER_DGAMELAUNCH,
	SERVER_RLSERVER,
	SERVER_TV
};

#endif /* SERVER_DATA_H_ */
