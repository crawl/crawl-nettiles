/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#include "AppHdr.h"
#include "screen.h"

using namespace std;

void Screen::compute_line_simple_cells(vector<simple_cell>& res, const vector<screen_cell>& line, unsigned w, const std::tr1::unordered_map<wchar_t, char>* unicode_replacements)
{
	res.clear();

	for(int x = 0; x < w; ++x)
	{
		wchar_t wc = line[x].c;
		simple_cell c;
		c.a = line[x].a.s.simplify();
		if(wc >= 0x80) {
			c.c = '?';
			if(unicode_replacements) {
				std::tr1::unordered_map<wchar_t, char>::const_iterator ri = unicode_replacements->find(wc);
				if(ri != unicode_replacements->end())
					c.c = ri->second;
			}
		} else
			c.c = (char)wc;

		res.push_back(c);
	}
}

void Screen::compute_simple_cells(vector<vector<simple_cell> >& res, const std::tr1::unordered_map<wchar_t, char>* unicode_replacements)
{
	res.resize(cols.size());
	for(unsigned y = 0; y < cols.size(); ++y) {
		compute_line_simple_cells(res[y], screen[y], cols[y], unicode_replacements);
	}
}
