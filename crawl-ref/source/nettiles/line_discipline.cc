/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#include "AppHdr.h"

#include "line_discipline.h"

using namespace std;

#define CTRL(x) (x^'@')

struct LineDisciplineImpl : public LineDiscipline
{
	static const size_t max_line_length = 256;

	string line;
	int state;
	char lastc;
	bool ignore_line;

	LineDisciplineImpl()
	: state(0), lastc(0), ignore_line(false)
	{}

	virtual void process(const std::string& s)
	{
		for(unsigned i = 0; i < s.size(); ++i)
		{
			char c = s[i];
			if(state == 1 && (c == 'O' || c == '['))
				state = 2;
			else if(state == 2 && c == 'D') {
				if(!line.empty())
					line.resize(line.size() - 1);
				state = 0;
			} else {
				state = 0;

				if(c == 8 || c == 127) {
					if(!line.empty())
						line.resize(line.size() - 1);
				} else if(c == CTRL('W')) {
					while(!line.empty() && isspace(line[line.size() - 1]))
						line.resize(line.size() - 1);

					while(!line.empty() && !isspace(line[line.size() - 1]))
						line.resize(line.size() - 1);
				} else if(c == CTRL('U')) {
					line = "";
					ignore_line = false;
				} else if(c == '\n' && lastc == '\r')
				{}
				else if(c == '\r' || c == '\n') {
					if(!ignore_line)
						line_signal(line);
					line = "";
					ignore_line = false;
				} else if(c == '\x1b')
					state = 1;
				else if((unsigned char)c < ' ')
				{}
				else {
					if(ignore_line)
					{}
					else if(line.size() >= max_line_length) {
						ignore_line = true;
						line = "";
					} else
						line += c;
				}

				lastc = c;
			}
		}
	}
};

LineDiscipline* LineDiscipline::create()
{
	return new LineDisciplineImpl();
}
