/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#ifndef PUTTY_TERMINAL_FRONTEND_H_
#define PUTTY_TERMINAL_FRONTEND_H_

#include "cell.h"

struct PuttyTerminalFrontend
{
	virtual ~PuttyTerminalFrontend() {}
	virtual void draw_text(int x, int y, wchar_t *text, int len, xattr_u attr) {}
	virtual void draw_cursor(int x, int y) {}
	virtual void set_cursor(int x, int y) {};
	virtual void move_cursor(int x, int y, int marg_clip) {}
};

#endif
