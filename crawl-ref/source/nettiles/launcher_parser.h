/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#ifndef LAUNCHER_PARSER_H_
#define LAUNCHER_PARSER_H_

#include "screen.h"
#include "signal.h"

struct LauncherParser
{
	virtual ~LauncherParser() {}

	signal<void(const std::string&)> login_signal;

	static LauncherParser* create(Screen* screen);
};

#endif /* LAUNCHER_PARSER_H_ */
