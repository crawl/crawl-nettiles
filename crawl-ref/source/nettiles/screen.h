/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#ifndef TERMINAL_H_
#define TERMINAL_H_

#include "cell.h"
#include "signal.h"
#ifdef _MSC_VER
#include <unordered_map>
#else
#include <tr1/unordered_map>
#endif

struct screen_cell : public wcell
{
	uint64_t stamp;

	screen_cell(const wcell& pc)
	: wcell(pc), stamp(0)
	{}

	screen_cell()
	: stamp(0)
	{
		c = L' ';
		a.v = 0;
	}

	void update(const wcell& pc, uint64_t new_stamp)
	{
		*(wcell*)this = pc;
		stamp = new_stamp;
	}
};

struct Screen
{
	static Screen* create(unsigned rows, unsigned cols, bool utf8 = false);

	virtual void set_charset(const char* charset_name) = 0;
	virtual void add_data(const void* p, size_t size) = 0;
	virtual void update() = 0;
	virtual bool in_application_cursor_mode() = 0;
	virtual bool in_application_keypad_mode() = 0;

	std::vector<unsigned> cols;
	std::vector<std::vector<screen_cell> > screen;
	std::vector<std::pair<coord_def, wcell> > changes;
	coord_def cursor;
	coord_def int_cursor;
	uint64_t cursor_stamp;
	uint64_t stamp_begin;
	uint64_t stamp_end;

	void compute_line_simple_cells(std::vector<simple_cell>& cells, const std::vector<screen_cell>& line, unsigned w, const std::tr1::unordered_map<wchar_t, char>* unicode_replacements);
	void compute_simple_cells(std::vector<std::vector<simple_cell> >& cells, const std::tr1::unordered_map<wchar_t, char>* unicode_replacements);

	signal<void(Screen&)> update_signal;

protected:
	std::list<std::tr1::function<void(Screen&)> > notifiers;

	Screen()
	: cursor_stamp(0), stamp_begin(1), stamp_end(1)
	 {
	 }
};

#endif /* TERMINAL_H_ */
