/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#include "AppHdr.h"
#include "branch.h"
#include "output.h"
#include "skills2.h"
#include "colour.h"
#include "feature.h"
#include "env.h"
#include "view.h"
#include "mon-util.h"
#include "items.h"
#include "mgen_data.h"
#include "mon-place.h"
#include "coord.h"
#include "coordit.h"
#include "tiledef-main.h"
#include "tiledef-dngn.h"
#include "tiledef-player.h"
#include "libutil.h"
#include "transform.h"
#include "options.h"
#include "mon-stuff.h"

#include "crawl_data.h"
#include "crawl_parser.h"
#include "crawl_defs.h"

#include <sstream>
#include <iostream>
#ifdef _MSC_VER
#include <unordered_map>
#include <unordered_set>
#else
#include <tr1/unordered_map>
#include <tr1/unordered_set>
#endif
#include <map>
#include <iomanip>
#include <math.h>
#include <malloc.h>
#ifndef HUGE_VALF
#define HUGE_VALF ((float)HUGE_VAL)
#endif

using namespace std;

void CrawlData::adjust_monster_tile(unsigned& fg, monster_type mon)
{
	if (Options.tile_show_demon_tier)
	{
		switch (mons_char(mon))
		{
		case '1':
			fg |= TILE_FLAG_DEMON_1;
			break;
		case '2':
			fg |= TILE_FLAG_DEMON_2;
			break;
		case '3':
			fg  |= TILE_FLAG_DEMON_3;
			break;
		case '4':
			fg |= TILE_FLAG_DEMON_4;
			break;
		case '5':
			fg |= TILE_FLAG_DEMON_5;
			break;
		}
	}

	if(mons_class_flies(mon))
		fg |= TILE_FLAG_FLYING;
	if(mons_is_mimic(mon))
		fg |= TILE_FLAG_ANIM_WEP;
}

void CrawlData::adjust_monster_tile_bg(unsigned& fg, unsigned abg)
{
	if(abg == ((Options.friend_brand >> 8) & 15)) /* GREEN */
		fg |= TILE_FLAG_PET;
	else if(abg == ((Options.neutral_brand >> 8) & 15)) /* LIGHTGREY */
		fg |= TILE_FLAG_NEUTRAL;
	else if(abg == ((Options.stab_brand >> 8) & 15)) /* BLUE */
		fg |= TILE_FLAG_STAB;
	else if(abg == ((Options.may_stab_brand >> 8) & 15)) /* YELLOW */
		fg |= TILE_FLAG_MAY_STAB;
}

parsed_monster CrawlData::parse_monster_name(string s)
{
	parsed_monster pm;
	pm.type = NUM_MONSTERS;
	pm.base_type = NUM_MONSTERS;
	pm.number = 0;
	pm.is_shapeshifter = false;
	pm.tile = 0;

	{
		int open_paren = -1;
		ostringstream ss;
		for(unsigned i = 0; i < s.length(); ++i)
		{
			if(s[i] == '(')
				open_paren = i;
			else if(s[i] == ')') {
				if(open_paren >= 0)
				{
					string qual = s.substr(open_paren + 1, i - open_paren - 1);
					// TODO: do something with these (they are wandering, confused, etc.)
					if(qual == "caught")
						pm.tile |= TILE_FLAG_NET;
					else if(qual == "poisoned") // unused
						pm.tile |= TILE_FLAG_POISON;
					else if(qual == "burning")
						pm.tile |= TILE_FLAG_FLAME;
					else if(qual == "berserk" || qual == "frenzied")
						pm.tile |= TILE_FLAG_BERSERK;
					else if(qual == "friendly") /* or GREEN */
						pm.tile |= TILE_FLAG_PET;
					else if(qual == "neutral" || qual == "fellow slime") /* or BROWN */
						pm.tile |= TILE_FLAG_NEUTRAL;
					else if(qual == "resting" || qual == "dormant" || qual == "sleeping"|| qual == "paralysed")
						pm.tile |= TILE_FLAG_STAB;
					else if(qual == "distracted" || qual == "unaware" || qual == "wandering" || qual == "confused" || qual == "fleeing" || qual == "petrified" || qual == "petrifying")
						pm.tile |= TILE_FLAG_MAY_STAB;
					else if(qual == "invisible")
					{}
				}
			}
			else if(open_paren >= 0)
			{}
			else
				ss << s[i];
		}
		s = ss.str();
		s = trim_string(s);
	}

	pm.name = s;

	for(;;) {
		size_t sp = s.find(' ');
		if(sp == string::npos)
			break;
		string tok = s.substr(0, sp);

		if(tok == "spectral")
			pm.type = MONS_SPECTRAL_THING;
		else if(ends_with(tok, "-headed")) {
			string heads_str = tok.substr(0, tok.size() - 7);
			const char* cardinals[] = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"};
			int i;
			for(i = 0; i < 10; ++i) {
				if(heads_str == cardinals[i]) {
					pm.number = i;
					break;
				}
			}
			if(i == 10) {
				istringstream ss(heads_str);
				ss >> pm.number;
			}
		} else
			break;

		s = s.substr(sp + 1);
	}

	while((int)pm.base_type == NUM_MONSTERS) {
		size_t sp = s.rfind(' ');
		if(sp == string::npos)
			break;
		string tok = s.substr(sp + 1);

		if(tok == "zombie" || tok == "zombies")
			pm.type = MONS_ZOMBIE_SMALL;
		else if(tok == "simulacrum" || tok == "simulacrums" || tok == "simulacra")
			pm.type = MONS_SIMULACRUM_SMALL;
		else if(tok == "skeleton" || tok == "skeletons")
			pm.type = MONS_SKELETON_SMALL;
		else if(tok == "ghost" && sp >= 2 && (s[sp - 1] == '\'' || s[sp - 2] == '\''))
			pm.base_type = pm.type = MONS_PLAYER_GHOST;
		else if(tok == "illusion") // TODO: is this correct?
			pm.base_type = pm.type = MONS_PLAYER_ILLUSION;
		else if(tok == "shifter" || tok == "shifters") {
			sp = s.rfind(' ', sp -  1);
			if(sp == string::npos)
				break;
			tok = s.substr(sp + 1);
			if(tok == "shaped shifter" || tok == "shaped shifters")
				pm.is_shapeshifter = true;
			else
				break;
		}
		else
			break;

		s = s.substr(0, sp);
	}

	if(pm.base_type == MONS_PLAYER_GHOST || pm.base_type == MONS_PLAYER_ILLUSION) {
		size_t ap = s.rfind('\'');
		if(ap != string::npos)
			s = s.substr(0, ap);
	} else {
		if(pm.type == MONS_SPECTRAL_THING && s == "warrior") {
			pm.type = NUM_MONSTERS;
			pm.base_type = MONS_SPECTRAL_WARRIOR;
		}

		if(ends_with(s, "the Cloud Mage"))
			s = "the Cloud Mage";
		else if(ends_with(s, "the Hellbinder"))
			s = "the Hellbinder";

		{
			name_to_monster_t::iterator i = name_to_monster.find(s);
			if(i != name_to_monster.end()) {
				pm.base_type = i->second.first;
				pm.number = max(pm.number, i->second.second);
			}
		}

		{
			name_to_monster_tile_t::iterator i = name_to_monster_tile.find(s);
			if(i != name_to_monster.end()) {
				pm.base_type = i->second.first;
				pm.tile = i->second.second;
			}
		}
	}

	if(pm.base_type != NUM_MONSTERS) {
		if(pm.type == MONS_ZOMBIE_SMALL && mons_zombie_size(pm.base_type) == Z_BIG)
			pm.type = MONS_ZOMBIE_LARGE;

		if(pm.type == MONS_SKELETON_SMALL && mons_zombie_size(pm.base_type) == Z_BIG)
			pm.type = MONS_SKELETON_LARGE;

		if(pm.type == MONS_SIMULACRUM_SMALL && mons_zombie_size(pm.base_type) == Z_BIG)
			pm.type = MONS_SIMULACRUM_LARGE;

		if((int)pm.type == NUM_MONSTERS)
			pm.type = pm.base_type;
	} else if(pm.type != NUM_MONSTERS)
		pm.base_type = pm.type;

	if(pm.type == NUM_MONSTERS) {
		if(!s.empty() && isupper(s[0])) {
			cerr << "Unknown monster name (assuming Pan lord): " << pm.name << endl;
			pm.type = pm.base_type = MONS_PANDEMONIUM_DEMON;
		} else {
			cerr << "Unknown monster name: " << pm.name << endl;
		}
	}

	return pm;
}

parsed_monster CrawlData::parse_monster(char c, cattr_t a, const string& s, cattr_t sa, int health)
{
	parsed_monster pm = parse_monster_name(s);

	if(!(pm.tile & TILE_FLAG_MASK))
		pm.tile |= tileidx_monster_type(pm.type, pm.base_type, a.fg != BLACK ? a.fg : a.bg, pm.number);

	adjust_monster_tile(pm.tile, pm.type);
	adjust_monster_tile(pm.tile, pm.base_type);
	adjust_monster_tile_bg(pm.tile, a.bg);

	switch (health)
	{
		case MDAM_DEAD:
		case MDAM_ALMOST_DEAD:
			pm.tile |= TILE_FLAG_MDAM_ADEAD;
			break;
		case MDAM_SEVERELY_DAMAGED:
			pm.tile |= TILE_FLAG_MDAM_SEV;
			break;
		case MDAM_HEAVILY_DAMAGED:
			pm.tile |= TILE_FLAG_MDAM_HEAVY;
			break;
		case MDAM_MODERATELY_DAMAGED:
			pm.tile |= TILE_FLAG_MDAM_MOD;
			break;
		case MDAM_LIGHTLY_DAMAGED:
			pm.tile |= TILE_FLAG_MDAM_LIGHT;
			break;
		case MDAM_OKAY:
		default:
			// no flag for okay.
			break;
	}

	switch(sa.fg)
	{
	case GREEN:
		pm.tile |= TILE_FLAG_PET;
		break;
	case BROWN:
		pm.tile |= TILE_FLAG_NEUTRAL;
		break;
	default:
		if(sa.fg == Options.evil_colour)
		{} // TODO: add a tile for unchivalric actions
	}

	if(pm.is_shapeshifter)
	{} // TODO: add a tile for shapeshifter

	return pm;
}

int CrawlData::guess_element_color(vector<unsigned> elems, unsigned char* freqs)
{
	// P(e | c...) = (P(c1 | e) P(c2 | e) P(e)) / P(c...)
	// max P(e | c...) = (prod P(c_i | e)^freq[c[i]] P(e))
	// max log P(e | c...) = (sum freq[c[i]] log P(c_i | e) + log P(e))

	unsigned n = elems.size();
	float* probs = (float*)alloca(sizeof(float) * n);

	for(unsigned i = 0; i < n; ++i)
		probs[i] = 0.0f;

	bool anything = false;
	for(unsigned j = 1; j < 16; ++j) {
		if(j != DARKGRAY && freqs[j]) {
			anything = true;
			for(unsigned i = 0; i < n; ++i) {
				unsigned elem = elems[i];
//					cout << i << ' ' << j << ": " << log_element_to_color_prob[elem][j] * freqs[j] << endl;
				probs[i] += log_element_to_color_prob[elem][j] * freqs[j];
			}
		}
	}

	if(!anything)
		return -1;

	int best_elem = -1;
	float best_prob = -HUGE_VALF;

	for(unsigned i = 0; i < n; ++i) {
//			cout << "Element " << elems[i] << " has prob " << exp(probs[i]) << endl;
		if(probs[i] > best_prob) {
			best_prob = probs[i];
			best_elem = elems[i];
		}
	}

	return best_elem;
}
