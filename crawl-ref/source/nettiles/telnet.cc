/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#include "AppHdr.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include "connection.h"

#ifdef WIN32
#include <winsock2.h>
#else
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>
#define closesocket close
typedef int SOCKET;
#endif

using namespace std;

#define SE 0xf0
#define GA 0xf9
#define SB 0xfa
#define WILL 0xfb
#define WONT 0xfc
#define DO 0xfd
#define DONT 0xfe
#define IAC 0xff

#define OPT_BINARY_TRANSMISSION 0
#define OPT_ECHO 1
#define OPT_RECONNECTION 2
#define OPT_SUPPRESS_GO_AHEAD 3
#define OPT_APPROX_MESSAGE_SIZE_NEGOTIATION 4
#define OPT_STATUS 5
#define OPT_TIMING_MARK 6
#define OPT_REMOTE_CONTROLLED_TRANS_AND_ECHO 7
#define OPT_OUTPUT_LINE_WIDTH 8
#define OPT_OUTPUT_PAGE_SIZE 9
#define OPT_OUTPUT_CARRIAGE_RETURN_DISPOSITION 10
#define OPT_OUTPUT_HORIZONTAL_TAB_STOPS 11
#define OPT_OUTPUT_HORIZONTAL_TAB_DISPOSITION 12
#define OPT_OUTPUT_FORMFEED_DISPOSITION 13
#define OPT_OUTPUT_VERTICAL_TABSTOPS 14
#define OPT_OUTPUT_VERTICAL_TAB_DISPOSITION 15
#define OPT_OUTPUT_LINEFEED_DISPOSITION 16
#define OPT_EXTENDED_ASCII 17
#define OPT_LOGOUT 18
#define OPT_BYTE_MACRO 19
#define OPT_DATA_ENTRY_TERMINAL 20
#define OPT_SUPDUP 21
#define OPT_SUPDUP_OUTPUT 22
#define OPT_SEND_LOCATION 23
#define OPT_TERMINAL_TYPE 24
#define OPT_END_OF_RECORD 25
#define OPT_TACACS_USER_IDENTIFICATION 26
#define OPT_OUTPUT_MARKING 27
#define OPT_TERMINAL_LOCATION_NUMBER 28
#define OPT_TELNET_3270_REGIME 29
#define OPT_X_3_PAD 30
#define OPT_NEGOTIATE_ABOUT_WINDOW_SIZE 31
#define OPT_TERMINAL_SPEED 32
#define OPT_REMOTE_FLOW_CONTROL 33
#define OPT_LINEMODE 34
#define OPT_X_DISPLAY_LOCATION 35
#define OPT_ENVIRONMENT_OPTION 36
#define OPT_AUTHENTICATION_OPTION 37
#define OPT_ENCRYPTION_OPTION 38
#define OPT_NEW_ENVIRONMENT_OPTION 39
#define OPT_TN3270E 40
#define OPT_XAUTH 41
#define OPT_CHARSET 42
#define OPT_TELNET_REMOTE_SERIAL_PORT_(RSP) 43
#define OPT_COM_PORT_CONTROL_OPTION 44
#define OPT_TELNET_SUPPRESS_LOCAL_ECHO 45
#define OPT_TELNET_START_TLS 46
#define OPT_KERMIT 47
#define OPT_SEND_URL 48
#define OPT_FORWARD_X 49
#define OPT_UNASSIGNED 50_137
#define OPT_TELOPT_PRAGMA_LOGON 138
#define OPT_TELOPT_SSPI_LOGON 139
#define OPT_TELOPT_PRAGMA_HEARTBEAT 140

struct Telnet : public Connection
{
	SOCKET sock;

#ifdef WIN32
	static string win32_strerror(int code)
	{
		char* buf;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER, 0, code, 0, (LPTSTR)&buf, 0, 0);
		string s(buf);
		LocalFree(buf);
		return s;
	}
#endif

	static void throw_socket_error(const string& s)
	{
		ostringstream ss;
		ss << s << ": ";
#ifdef WIN32
		ss << win32_strerror(WSAGetLastError());
#else
		ss << strerror(errno);
#endif
		throw std::runtime_error(ss.str());
	}

	Telnet(const string& host, unsigned port, unsigned rows, unsigned cols)
	{
#ifdef WIN32
		WSADATA wsaData;
		int startup_rc = WSAStartup(0x202, &wsaData);

		if (startup_rc != 0)
		{
			std::ostringstream ss;
			ss << "Windows Sockets startup failed: " << win32_strerror(startup_rc);
			throw new std::runtime_error(ss.str());
		}
#endif

		struct hostent *he = gethostbyname(host.c_str());
		if (he == NULL)
			throw_socket_error("Failed to lookup host " + host);
		sock = socket(PF_INET, SOCK_STREAM, 0);
		if (sock == -1)
			throw_socket_error("Failed to create TCP socket");
		struct sockaddr_in addr;
		addr.sin_family = AF_INET;
		addr.sin_port = htons(port);
		addr.sin_addr = *((struct in_addr *) he->h_addr);
		memset(addr.sin_zero, '\0', sizeof (addr.sin_zero));

		if (connect(sock, (struct sockaddr *) &addr, sizeof (addr)) == -1)
			throw_socket_error("Failed to connect to " + host);

	#if 0
		/* telnet "ping" hack, currently not used */
		ping.push_back((char)IAC); // IAC
		ping.push_back((char)DO); // DO
		ping.push_back(0x63); // 99
	#endif

		unsigned char data[] = {
				IAC, WILL, OPT_TERMINAL_TYPE,
				IAC, SB, OPT_TERMINAL_TYPE, 0x00, 'x', 't', 'e', 'r', 'm', IAC, SE,
				IAC, WONT, OPT_TERMINAL_SPEED,
				IAC, WONT, OPT_X_DISPLAY_LOCATION,
				IAC, WONT, OPT_NEW_ENVIRONMENT_OPTION,
				IAC, DO, OPT_SUPPRESS_GO_AHEAD,
				IAC, WILL, OPT_ECHO,
				IAC, DO, OPT_STATUS,
				IAC, WILL, OPT_REMOTE_FLOW_CONTROL,
				IAC, WILL, OPT_NEGOTIATE_ABOUT_WINDOW_SIZE,
				IAC, SB, OPT_NEGOTIATE_ABOUT_WINDOW_SIZE, cols >> 8, cols, rows >> 8, rows, IAC, SE,
		};

		transmit(data, sizeof(data));
	}

	virtual ptrdiff_t retrieve(void* buf, size_t size)
	{
		return recv(sock, (char*)buf, size, 0);
	}

	virtual ptrdiff_t transmit(const void* buf, size_t size)
	{
		return send(sock, (const char*)buf, size, 0);
	}

	virtual  ~Telnet()
	{
		closesocket(sock);

#ifdef WIN32
		WSACleanup();
#endif
	}
};

Connection* create_telnet_connection(const string& host, unsigned port, unsigned rows, unsigned cols)
{
	return new Telnet(host, port, rows, cols);
}
