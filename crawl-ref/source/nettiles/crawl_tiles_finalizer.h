/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#ifndef CRAWL_TILES_FINALIZER_H_
#define CRAWL_TILES_FINALIZER_H_

#include "crawl_defs.h"

#include <utility>

struct CrawlTilesFinalizer
{
	virtual ~CrawlTilesFinalizer() {}

	virtual void reset() = 0;
	virtual void finalize(CrawlFragment& frag, unsigned* floor_tiles, unsigned* wall_tiles, coord_def player_pos, bool seen_works, bool is_silenced) = 0;

	static CrawlTilesFinalizer* create();
};

#endif /* CRAWL_TILES_FINALIZER_H_ */
