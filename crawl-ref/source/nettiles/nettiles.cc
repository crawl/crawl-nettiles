/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#include "AppHdr.h"
#include "env.h"
#include "cio.h"
#include "libutil.h"
#include "tilereg-text.h"
#include "coordit.h"

#include "nettiles.h"
#include "screen.h"
#include "crawl_parser.h"
#include "launcher_parser.h"
#include "crawl_charset_detector.h"
#include "line_discipline.h"
#include "connection.h"

#include <SDL.h>

#include <iostream>
#include <sstream>
using namespace std;
using namespace std::tr1;
using namespace std::tr1::placeholders;

static const char* charset_names[] = {"ASCII", "IBM437", "ISO-8859-1", "UTF-8"};

struct NetTilesImpl : public NetTiles
{
	unsigned charset;
	Screen* screen;
	CrawlParser* crawl_parser;
	LauncherParser* launcher_parser;
	CrawlCharsetDetector* crawl_charset_detector;
	Connection* conn;
	ostringstream input_data_stream;
	SDL_mutex* input_data_stream_mutex;
	bool quit;
	LineDiscipline* ldisc;
	signal_connection launcher_login_connection;
	signal_connection ldisc_line_connection;
	signal_connections crawl_parser_connections;
	int activity_mode;
	bool sent_using_nettiles;
	bool tiles_enabled;

	NetTilesImpl()
	{
		tiles_enabled = true;
		crawl_charset_detector = CrawlCharsetDetector::create();
		screen = Screen::create(255, 255);
		crawl_parser = CrawlParser::create(screen);
		crawl_parser->enable_tiles(tiles_enabled);
		crawl_parser_connections += crawl_parser->message_signal.connect(std::tr1::bind(&NetTilesImpl::handle_message, this, _1));
		crawl_parser_connections += crawl_parser->screen_type_changed_signal.connect(std::tr1::bind(&NetTilesImpl::handle_screen_type_changed, this, _1));
		crawl_parser_connections += crawl_parser->update_dungeon_signal.connect(std::tr1::bind(&NetTilesImpl::handle_update_dungeon, this, _1, _2, _3, _4, _5));
		crawl_parser_connections += crawl_parser->update_text_region_signal.connect(std::tr1::bind(&NetTilesImpl::handle_update_text_region, this, _1, _2));
		launcher_parser = LauncherParser::create(screen);
		launcher_login_connection = launcher_parser->login_signal.connect(std::tr1::bind(&NetTilesImpl::handle_login, this, _1));
		input_data_stream_mutex = SDL_CreateMutex();
		ldisc = LineDiscipline::create();
		ldisc_line_connection = ldisc->line_signal.connect(std::tr1::bind(&NetTilesImpl::handle_line, this, _1));
		conn = 0;
	}

	~NetTilesImpl()
	{
		ldisc_line_connection.remove();
		delete ldisc;
		SDL_DestroyMutex(input_data_stream_mutex);
		launcher_login_connection.remove();
		delete launcher_parser;
		crawl_parser_connections.remove();
		delete crawl_parser;
		delete screen;
		delete crawl_charset_detector;
	}

	static const unsigned max_input_lines = 16;
	deque<string> input;

	void handle_line(const std::string& s)
	{
		if(input.size() >= max_input_lines)
			input.resize(max_input_lines - 1);
		input.push_front(s);
	}

	void handle_login(const std::string& login)
	{
		string next;
		for(deque<string>::iterator i = input.begin(); i != input.end(); ++i) {
			if(ends_with(*i, login)) {
				string cur = *i;
				++i;
				string password;
				if(i != input.end() && ends_with(*i, login))
					password = cur;
				else
					password = next;

				login_signal(login, password);
				break;
			}
			next = *i;
		}
	}

	void handle_message(std::string message)
	{
		//cout << "MSG: " << message << endl;
		if(message[0] == ' ' || message[0] == '_')
			message = message.substr(1);
		if(activity_mode >= 0 && !sent_using_nettiles) {
			if(starts_with(message, "Welcome back, ") || starts_with(message, "Welcome, ")) {
				bool dosend = true;
				for(unsigned i = 0; i < crawl_parser->messages.size(); ++i) {
					string s = crawl_parser->messages[i];
					if(s[0] == ' ' || s[0] == '_')
						s = s.substr(1);
					if(starts_with(s, "Enter note: Using NetTiles "))
					{
						dosend = false;
						break;
					}
				}
				if(dosend) {
					ostringstream ss;
					ss << ":Using NetTiles " << Version::Long() << " for " << Version::BuildOS() << " (added for statistical purposes)\r";
					conn->transmit(ss.str().data(), ss.str().size());
				}
				sent_using_nettiles = true;
			}
		}
	}

	void handle_screen_type_changed(screen_type_t type)
	{
		sent_using_nettiles = false;
	}

	void handle_update_dungeon(const CrawlFragment& frag, unsigned default_floor, unsigned default_wall, const std::vector<std::pair<coord_def, std::string> >& tags, coord_def double_center_pos)
	{
		tiles.clear_overlays();
		tiles.clear_text_tags(TAG_NAMED_MONSTER);

		coord_def off = double_center_pos;
		off -= crawl_view.viewsz;
		off += 1;
		off /= 2;

		off.x = (int)(frag.cols - crawl_view.viewsz.x) / 2;
		if(off.x < 0) {
			off.x = (double_center_pos.x - (crawl_view.viewsz.x - 1)) / 2;
			off.x = max(off.x, 0);
			off.x = min(off.x, (int)frag.cols - crawl_view.viewsz.x);
		}

		off.y = 0;
		if((int)frag.rows > crawl_view.viewsz.y) {
			off.y = (double_center_pos.y - (crawl_view.viewsz.y - 1)) / 2;
			off.y = max(off.y, 0);
			off.y = min(off.y, (int)frag.rows - crawl_view.viewsz.y);
		}

		screen_buffer_t *bufp(crawl_view.vbuf);

		for(int y = off.y; y < (off.y + crawl_view.viewsz.y); ++y) {
			for(int x = off.x; x < (off.x + crawl_view.viewsz.x); ++x) {
				if(x < 0 || y < 0 || x >= (int)frag.cols || y >= (int)frag.rows) {
					bufp[0] = 0;
					bufp[1] = 0;
				} else {
					const pair<screen_buffer_t, screen_buffer_t>& p = frag(x, y);
					bufp[0] = p.first;
					bufp[1] = p.second;
				}
				bufp += 2;
			}
		}

		for(unsigned i = 0; i < tags.size(); ++i)
			tiles.add_text_tag(TAG_NAMED_MONSTER, tags[i].second, tags[i].first - off);

		coord_def tent_player_pos = double_center_pos;
		tent_player_pos /= 2;
		tent_player_pos -= off;
		you.position = tent_player_pos;
		crawl_view.set_player_at(tent_player_pos, true);
		crawl_view.vgrdc = crawl_view.view_centre();
		crawl_view.calc_vlos();

	        for (rectangle_iterator ri(0); ri; ++ri) {
			env.tile_flv(*ri).special = 0;
			env.tile_flv(*ri).feat = 0;
			env.tile_flv(*ri).floor = default_floor;
			env.tile_flv(*ri).wall = default_wall;
	        }

		tiles.set_need_redraw(0);
		tiles.load_dungeon(crawl_view.vbuf, coord_def(crawl_view.viewsz.x/2, crawl_view.viewsz.y/2));
	}

	void handle_update_text_region(GotoRegion region, const std::vector<std::vector<simple_cell> >& region_cells)
	{
		{
			int w = 0;
			int h = region_cells.size();
			for(int y = 0; y < h; ++y)
			{
				/* TODO: maybe ignore colored trailing spaces */
				w = max(w, (int)region_cells[y].size());
			}
			cgotoxy(1, 1, region);

			// TODO: should do this better, I think this may risk going out of screen bounds...
			if(w > TextRegion::text_mode->mx || h != TextRegion::text_mode->my) {
				TextRegion::text_mode->resize(max(w, TextRegion::text_mode->mx), max(h, TextRegion::text_mode->my));
				cgotoxy(1, 1, region);
			}
		}

		int y;
		for(y = 0; y < TextRegion::text_mode->my; ++y) {
			cgotoxy(1, 1 + y, region);
			if(y < (int)region_cells.size()) {
				vector<span> spans;
				generate_spans(spans, region_cells[y], GENERATE_SPANS_INCLUDE_SPACES);

				for(unsigned i = 0; i < spans.size(); ++i) {
					textcolor(spans[i].a.fg);
					textbackground(spans[i].a.bg);
					TextRegion::text_mode->addstr((char*)spans[i].s.c_str());
				}
			}
			textcolor(LIGHTGRAY);
			textbackground(BLACK);
			clear_to_end_of_line();
		}
	}

	virtual void run(Connection* pconn, int pactivity_mode)
	{
		activity_mode = pactivity_mode;
		sent_using_nettiles = false;
		conn = pconn;
		run();
		delete conn;
		conn = 0;
	}

	void run_connection_thread() {
		char buf[65536];
		for(;;) {
			ptrdiff_t ret = conn->retrieve(buf, sizeof(buf));
			if(ret <= 0)
				break; /* TODO: quit or similar */

			SDL_LockMutex(input_data_stream_mutex);
			input_data_stream.write(buf, ret);
			SDL_UnlockMutex(input_data_stream_mutex);

			/* this is a portable way to guarantee a memory barrier
			 * the unlock above + the lock below is actually the barrier
			 * note that just one lock+unlock may not be enough instead,
			 * since the simulate_key_event can be moved before the unlock
			 * by the CPU out-of-order logic
			 *
			 * note that in practice this will be almost surely unnecessary, but let's be safe
			 */
			SDL_LockMutex(input_data_stream_mutex);
			SDL_UnlockMutex(input_data_stream_mutex);

			tiles.simulate_key_event(CK_NETTILES_DATA);
		}

		SDL_LockMutex(input_data_stream_mutex);
		quit = true;
		SDL_UnlockMutex(input_data_stream_mutex);
		SDL_LockMutex(input_data_stream_mutex);
		SDL_UnlockMutex(input_data_stream_mutex);
		tiles.simulate_key_event(CK_NETTILES_DATA);
	}

	static int SDLCALL connection_thread_func(void* cookie)
	{
		((NetTilesImpl*)cookie)->run_connection_thread();
		return 0;
	}

#define CSI "\x1b["
#define SS3 "\x1bO"
#define SHIFT 1
#define ALT 2
#define CTRL 4
#define META 8
	string alpha_key(const char* code, char c, unsigned mods = 0) {
		ostringstream ss;
		if(mods)
			ss << CSI << "1;" << (mods + 1);
		else
			ss << code;
		ss << c;
		return ss.str();
	}

	string ss3_key(char c, unsigned mods = 0) {
		return alpha_key(SS3, c, mods);
	}

	string csi_key(char c, unsigned mods = 0) {
		return alpha_key(CSI, c, mods);
	}

	string csi_ss3_key(char c, unsigned mods = 0) {
		return alpha_key(screen->in_application_cursor_mode() ? SS3 : CSI, c, mods);
	}

	string csi_tilde_key(unsigned n, unsigned mods = 0) {
		ostringstream ss;
		ss << CSI << n;
		if(mods)
			ss << ';' << (1 + mods);
		ss << '~';
		return ss.str();
	}

	char keypad_vi_key(unsigned key) {
		switch(key) {
		case CK_UP:
			return 'k';
		case CK_DOWN:
			return 'j';
		case CK_RIGHT:
			return 'l';
		case CK_LEFT:
			return 'h';
		case CK_HOME:
			return 'y';
		case CK_END:
			return 'b';
		case CK_PGUP:
			return 'u';
		case CK_PGDN:
			return 'n';
		case CK_CLEAR:
			return '.';
		default:
			return 0;
		}
	}

	string raw_keypad_key(unsigned key, unsigned mods = 0) {
		switch(key) {
		case CK_ENTER:
			if(mods)
				return ss3_key('M', mods);
			else
				return "\r";
		case CK_BKSP:
			return "\b";
		case CK_ESCAPE:
			return "\x1b";
		case CK_DELETE:
			if(!mods)
				return csi_tilde_key(3, mods);
		case CK_UP:
			return csi_ss3_key('A', mods);
		case CK_DOWN:
			return csi_ss3_key('B', mods);
		case CK_RIGHT:
			return csi_ss3_key('C', mods);
		case CK_LEFT:
			return csi_ss3_key('D', mods);
		case CK_HOME:
			return csi_ss3_key('H', mods);
		case CK_END:
			return csi_ss3_key('F', mods);
		case CK_PGUP:
			return csi_tilde_key(5, mods);
		case CK_PGDN:
			return csi_tilde_key(6, mods);
		case CK_CLEAR:
			return csi_key('E', mods);
		case CK_INSERT:
			return csi_tilde_key(2, mods);
		default:
			return string();
		}
	}

	string keypad_key(unsigned key, unsigned mods = 0) {
		// Crawl doesn't actually support mods, so use the ad-hoc long forms instead
		if(mods & CTRL)
			return string(1, '*') + keypad_key(key);
		else if(mods & SHIFT) {
			// use the uppercase vi key since it can be remapped, unlike the / arrow form
			char c = keypad_vi_key(key);
			if(!c)
				return keypad_key(key, mods);
			else if(c == '.')
				return "5";
			else
				return string(1, toupper(c));
		} else
			return raw_keypad_key(key);
	}

	string special_key(unsigned key, unsigned mods = 0) {
		// Crawl doesn't actually support mods right now...
		mods = 0;

		switch(key) {
		case SDLK_F1:
			return ss3_key('P', mods);
		case SDLK_F2:
			return ss3_key('Q', mods);
		case SDLK_F3:
			return ss3_key('R', mods);
		case SDLK_F4:
			return ss3_key('S', mods);
		case SDLK_F5:
			return csi_tilde_key(15, mods);
		case SDLK_F6:
			return csi_tilde_key(17, mods);
		case SDLK_F7:
			return csi_tilde_key(18, mods);
		case SDLK_F8:
			return csi_tilde_key(19, mods);
		case SDLK_F9:
			return csi_tilde_key(20, mods);
		case SDLK_F10:
			return csi_tilde_key(21, mods);
		case SDLK_F11:
			return csi_tilde_key(23, mods);
		case SDLK_F12:
			return csi_tilde_key(24, mods);
		case SDLK_F13:
			return csi_tilde_key(25, mods);
		case SDLK_F14:
			return csi_tilde_key(26, mods);
		case SDLK_F15:
			return csi_tilde_key(28, mods);
		default:
			return string();
		}
	}

	string term_key(unsigned key) {
		if(key < 128)
			return string(1, (char)key);
		else if(key == CK_MOUSE_CLICK || key == CK_MOUSE_CMD)
		{
			//cout << ((key == CK_MOUSE_CMD) ? "right" : "left") << " click at " <<  you.last_clicked_grid.x << ' ' << you.last_clicked_grid.y << endl;
			char mods = 0; /* 4 = shift, 8 = alt, 16 = control , 64 = wheel button*/
			char buf[4];
			buf[0] = 33 + you.last_clicked_grid.x;
			buf[1] = 33 + you.last_clicked_grid.y;
			buf[2] = 0;
			ostringstream ss;
			ss << CSI << 'M';
			ss << (char)(32 | mods | (key == CK_MOUSE_CLICK ? 0 : 2));
			ss << buf;
			ss << CSI << 'M';
			ss << (char)(32 | mods | 3);
			ss << buf;
			return ss.str();
			//cout << "Mouse: " << tosend << endl;
		}
		else if(key >= CK_DELETE && key <= CK_PGDN)
			return keypad_key(key, 0);
		else if(key >= CK_SHIFT_UP && key <= CK_SHIFT_PGDN)
			return keypad_key(key + CK_UP - CK_SHIFT_UP, SHIFT);
		else if(key >= CK_CTRL_UP && key <= CK_CTRL_PGDN)
			return keypad_key(key + CK_UP - CK_CTRL_UP, CTRL);
		else if(key >= SDLK_F1 && key <= (SDLK_UNDO + (SDLK_UNDO + 1 - SDLK_F1) * 8)) {
			unsigned off = key - SDLK_F1;
			unsigned range = SDLK_UNDO + 1 - SDLK_F1;
			unsigned ck_mods = off / range;
			unsigned mods = ((ck_mods & 1) ? SHIFT : 0) | ((ck_mods & 2) ? CTRL : 0) | ((ck_mods & 4) ? ALT : 0);
			unsigned base_key = SDLK_F1 + (off % range);
			return special_key(base_key, mods);
		} else if(key >= (1000 + 3 * 256) && key < (1000 + 8 * 256 + 256)) {
			unsigned off = key - (1000 + 3 * 256);
			unsigned range = 256;
			unsigned ck_mods = off / range;
			unsigned mods = ((ck_mods & 1) ? SHIFT : 0) | ((ck_mods & 2) ? CTRL : 0) | ((ck_mods & 4) ? ALT : 0);
			unsigned base_key = (off % range);
			return keypad_key(base_key, mods);
		}
		return string();
	}

	void run() {
		charset = CSET_ASCII;
		quit = false;
		clrscr();
		SDL_Thread* connection_thread = SDL_CreateThread(NetTilesImpl::connection_thread_func, this);
		for(;;) {
			mouse_control mc(MOUSE_MODE_NETTILES);

			string tosend;
			int key = getch_ck();
			if(key == 22) { /* ctrl-v */
				tiles_enabled = !tiles_enabled;
				crawl_parser->enable_tiles(tiles_enabled);
			} else
				tosend = term_key(key);

			if(!tosend.empty()) {
				ldisc->process(tosend);
				conn->transmit(tosend.data(), tosend.size());
			}

			string s;
			SDL_LockMutex(input_data_stream_mutex);
			s = input_data_stream.str();
			input_data_stream.str(string());
			SDL_UnlockMutex(input_data_stream_mutex);

			if(quit)
				break;

			if(!s.empty()) {
				crawl_charset_detector->process(s);
				if(crawl_charset_detector->charset != charset) {
					charset = crawl_charset_detector->charset;
					screen->set_charset(charset_names[charset]);

					crawl_parser->set_charset(charset);
				}

				screen->add_data(s.data(), s.length());
				screen->update();
			}
		}
		int status;
		SDL_WaitThread(connection_thread, &status);
	}
};

NetTiles* NetTiles::create()
{
	return new NetTilesImpl();
}
