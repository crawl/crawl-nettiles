/*
 * Find the platform-specific header for this platform.
 */

#ifndef PUTTY_PUTTYPS_H
#define PUTTY_PUTTYPS_H

// CRAWL: no thanks
#define NO_GSSAPI

// CRAWL: use _WIN32 instead of Windows
#ifdef WIN32

#include "winstuff.h"

#elif defined(macintosh)

#include "macstuff.h"

#elif defined(MACOSX)

#include "osx.h"

#else

#include "unix.h"

#endif

#endif
