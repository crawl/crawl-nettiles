/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#ifndef CRAWL_DATA_H_
#define CRAWL_DATA_H_

#include <vector>
#include "ext_hash.h"
#ifdef _MSC_VER
#include <unordered_map>
#else
#include <tr1/unordered_map>
#endif

#include "cell.h"
#include "crawl_defs.h"

struct parsed_monster
{
	monster_type type;
	monster_type base_type;
	unsigned number;
	unsigned tile;
	bool is_shapeshifter;
	std::string name;
};

struct CrawlData
{
	typedef std::vector<std::vector<unsigned char> > element_to_color_t;
	element_to_color_t element_to_color;
	typedef std::vector<std::vector<unsigned char> > color_to_element_t;
	color_to_element_t color_to_element;
	float log_element_to_color_prob[ETC_RANDOM + 1][16];
	typedef std::tr1::unordered_multimap<std::pair<char, unsigned char>, std::pair<monster_type, unsigned> > char_fg_to_monsters_t;
	char_fg_to_monsters_t char_fg_to_monsters;
	std::pair<monster_type, unsigned> char_to_base_monster[128];
	typedef std::tr1::unordered_map<wchar_t, unsigned char> wchar_to_dchar_t;
	wchar_to_dchar_t wchar_to_dchar[NUM_CSET];
	typedef std::tr1::unordered_map<unsigned char, std::pair<dungeon_feature_type, unsigned> > color_to_feature_t;
	typedef std::tr1::unordered_map<unsigned char, color_to_feature_t> dchar_to_color_to_feature_t;
	dchar_to_color_to_feature_t dchar_to_color_to_feature;
	typedef std::tr1::unordered_map<std::pair<unsigned char, unsigned char>, unsigned> dchar_fg_to_tile_t;
	dchar_fg_to_tile_t dchar_fg_to_tile;
	typedef std::tr1::unordered_map<std::string, std::pair<monster_type, unsigned> > name_to_monster_t;
	name_to_monster_t name_to_monster;
	typedef std::tr1::unordered_map<std::string, std::pair<monster_type, unsigned> > name_to_monster_tile_t;
	name_to_monster_tile_t name_to_monster_tile;
	typedef std::tr1::unordered_map<std::string, species_type> name_to_species_t;
	name_to_species_t name_to_species;
	typedef std::tr1::unordered_map<std::string, god_type> name_to_god_t;
	name_to_god_t name_to_god;
	typedef std::tr1::unordered_map<wchar_t, char> unicode_replacements_t;
	unicode_replacements_t unicode_replacements;
	bool inited;

	void init();

	void adjust_monster_tile(unsigned& fg, monster_type mon);
	void adjust_monster_tile_bg(unsigned& fg, unsigned abg);
	parsed_monster parse_monster_name(std::string s);
	parsed_monster parse_monster(char c, cattr_t a, const std::string& s, cattr_t sa, int health);

	int guess_element_color(std::vector<unsigned> elems, unsigned char* freqs);

	char replace_unicode(wchar_t wc)
	{
		unicode_replacements_t::iterator ri = unicode_replacements.find(wc);
		if(wc < 0x80)
			return (char)wc;
		else if(ri == unicode_replacements.end())
			return '?';
		else
			return ri->second;
	}
};

extern CrawlData crawl_data;

#endif /* CRAWL_DATA_H_ */
