/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#ifndef LINE_DISCIPLINE_H_
#define LINE_DISCIPLINE_H_

#include "signal.h"

struct LineDiscipline
{
	virtual void process(const std::string& s) = 0;

	signal<void(const std::string&)> line_signal;

	static LineDiscipline* create();
};

#endif /* LINE_DISCIPLINE_H_ */
