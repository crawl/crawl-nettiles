/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#include "AppHdr.h"
#include "putty_terminal_frontend.h"
extern "C" {
#include "putty/putty.h"
extern void move_cursor(void* frontend, int x, int y, int marg_clip);
}

/* Crawl swaps red/blue and brown/cyan (why?!) */
static unsigned color_ansi_to_crawl[] =
{
	BLACK,
	RED,
	GREEN,
	BROWN,
	BLUE,
	MAGENTA,
	CYAN,
	LIGHTGREY,
	DARKGREY,
	LIGHTRED,
	LIGHTGREEN,
	YELLOW,
	LIGHTBLUE,
	LIGHTMAGENTA,
	LIGHTCYAN,
	WHITE
};

void sys_cursor(void *frontend, int x, int y)
{
	((PuttyTerminalFrontend*)frontend)->set_cursor(x, y);
}

static unsigned convert_putty_color(unsigned color)
{
	if(color == 256 || color == 261)
		return LIGHTGREY;
	if(color == 257)
		return WHITE;
	if(color == 258 || color == 260)
		return BLACK;
	if(color == 257)
		return DARKGREY;
	if(color == 260)
		return BLACK;

	/* xterm colors, not used by crawl */
	if(color >= 16)
		return LIGHTGREY;
	return color;
}

void do_text(Context ctx, int x, int y, wchar_t *text, int len, unsigned long attr, int lattr)
{
	xattr_u a;
	unsigned fg = convert_putty_color((attr & ATTR_FGMASK) >> ATTR_FGSHIFT);
	unsigned bg = convert_putty_color((attr & ATTR_BGMASK) >> ATTR_BGSHIFT);

	fg |= (attr & ATTR_BOLD) ? 8 : 0;
	bg |= (attr & ATTR_BLINK) ? 8 : 0;

	a.v = 0;
	a.s.fg = color_ansi_to_crawl[fg];
	a.s.bg = color_ansi_to_crawl[bg];
	a.s.reverse = !!(attr & ATTR_REVERSE);
	a.s.vis_cursor = !!(attr & (TATTR_ACTCURS | TATTR_PASCURS));

        if(a.s.vis_cursor)
		((PuttyTerminalFrontend*)ctx)->draw_cursor(x, y);

        ((PuttyTerminalFrontend*)ctx)->draw_text(x, y, text, len, a);
}



extern "C"
{

void set_iconic(void *frontend, int iconic) {}
void move_window(void *frontend, int x, int y) {}
void set_zorder(void *frontend, int top) {}
void refresh_window(void *frontend) {}
void set_zoomed(void *frontend, int zoomed) {}
int is_iconic(void *frontend) { return 0; }
void get_window_pos(void *frontend, int *x, int *y) { *x = *y = 0; }
void get_window_pixels(void *frontend, int *x, int *y) { *x = *y = 0; }
char *get_window_title(void *frontend, int icon) { return (char*)""; }
void set_title(void *frontend, char *title) {}
void set_icon(void *frontend, char *title) {}
void set_sbar(void *frontend, int total, int start, int page) {}
void get_clip(void *frontend, wchar_t ** p, int *len) { *p = NULL; *len = 0; }
void write_clip(void *frontend, wchar_t * data, int *attr, int len,
                int must_deselect) {}
void request_paste(void *frontend) {}
void request_resize(void *frontend, int w, int h) {}
void palette_reset(void *frontend) {}
void palette_set(void *frontend, int n, int r, int g, int b) {}
void set_raw_mouse_mode(void *frontend, int activate) {}
void logflush(void *handle) {}
void logtraffic(void *handle, unsigned char c, int logmode) {}
void do_beep(void *frontend, int mode) {}

void ldisc_send(void *handle, char *buf, int len, int interactive)
{}

Context get_ctx(void *frontend)
{
	return (Context)frontend;
}

void free_ctx(Context ctx)
{
}

void do_cursor(Context ctx, int x, int y, wchar_t *text, int len,
	       unsigned long attr, int lattr)
{
}

void fatalbox(char *p, ...)
{
	va_list ap;

	fprintf(stderr, "FATAL ERROR: ");
	va_start(ap, p);
	vfprintf(stderr, p, ap);
	va_end(ap);
	fputc('\n', stderr);
	exit(1);
}

void modalfatalbox(char *p, ...)
{
	va_list ap;

	fprintf(stderr, "FATAL ERROR: ");
	va_start(ap, p);
	vfprintf(stderr, p, ap);
	va_end(ap);
	fputc('\n', stderr);
	exit(1);
}

int char_width(Context ctx, int uc)
{
    /*
     * I don't expect this to come up very often.
     */
    return 1;
}

void *open_settings_w(const char *sessionname, char **errmsg)
{ return NULL; }
void write_setting_s(void *handle, const char *key, const char *value) {}
void write_setting_i(void *handle, const char *key, int value) {}
void close_settings_w(void *handle) {}
void *open_settings_r(const char *sessionname)
{ return NULL; }
char *read_setting_s(void *handle, const char *key, char *buffer, int buflen)
{ return NULL; }
int read_setting_i(void *handle, const char *key, int defvalue)
{ return defvalue; }
int read_setting_fontspec(void *handle, const char *name, FontSpec *result)
{ return 0; }
int read_setting_filename(void *handle, const char *name, Filename *result)
{ return 0; }
void write_setting_fontspec(void *handle, const char *name, FontSpec result) {}
void write_setting_filename(void *handle, const char *name, Filename result) {}
void close_settings_r(void *handle) {}
void *enum_settings_start(void) { return NULL; }
char *enum_settings_next(void *handle, char *buffer, int buflen) {return NULL;}
FontSpec platform_default_fontspec(const char *name)
{
    FontSpec ret;
    *ret.name = '\0';
    return ret;
}
Filename platform_default_filename(const char *name)
{
    Filename ret;
    *ret.path = '\0';
    return ret;
}
char *platform_default_s(const char *name) { return NULL; }
int platform_default_i(const char *name, int def) { return def; }
void enum_settings_finish(void *handle) {}
int default_port = -1, default_protocol = -1;

}
