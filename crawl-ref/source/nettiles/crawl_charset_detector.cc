/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#include "AppHdr.h"
#include "crawl_charset_detector.h"
#include <iostream>
using namespace std;

extern "C" const unsigned dchar_table[ NUM_CSET ][ NUM_DCHAR_TYPES ];

static const char* charset_friendly_names[] = {"ASCII", "IBM", "DEC", "UTF-8"};
static const char clear_screen[] = "\x1b[2J";

/* We only consider the current screen and the previous one
 * Without this logic, if you watch an ASCII game after a non-ASCII game,
 * the charset won't switch to ASCII, causing problems (e.g. walls displayed as clouds)
 */
struct CrawlCharsetDetectorImpl : public CrawlCharsetDetector
{
	bool warned_about_charsets;
	unsigned charset_masks[256];
	unsigned clear_screen_seq_place;
	unsigned prev_screen_possible_charsets;
	unsigned screen_possible_charsets;

	virtual void reset()
	{
		warned_about_charsets = false;
		prev_screen_possible_charsets = 0;
		screen_possible_charsets = possible_charsets = (1 << NUM_CSET) - 1;
		charset = choose_charset(possible_charsets);
		clear_screen_seq_place = 0;
	}

	CrawlCharsetDetectorImpl()
	{
		for(unsigned i = 0; i < 0x80; ++i)
			charset_masks[i] = (1 << NUM_CSET) - 1;

		for(unsigned i = 0x80; i < 0xf0; ++i)
			charset_masks[i] = 0;

		/* telnet characters */
		for(unsigned i = 0xf0; i < 0x100; ++i)
			charset_masks[i] = (1 << NUM_CSET) - 1;

		for(unsigned dc = 0; dc < NUM_DCHAR_TYPES; ++dc) {
			wchar_t wc = dchar_table[CSET_IBM][dc];
			if(wc >= 0x80) {
				assert(wc < 0x100);
				charset_masks[wc] |= (1 << CSET_IBM);
			}
		}

		for(unsigned dc = 0; dc < NUM_DCHAR_TYPES; ++dc) {
			wchar_t wc = dchar_table[CSET_DEC][dc];
			if(wc >= 0x80 && wc <= 0xe0) {
				charset_masks[wc] |= (1 << CSET_DEC);
			}
		}

		for(unsigned dc = 0; dc < NUM_DCHAR_TYPES; ++dc) {
			wchar_t wc = dchar_table[CSET_UNICODE][dc];
			if(wc < 0x80)
			{}
			else if(wc < 0x800) {
				charset_masks[0xc0 | (wc >> 6)] |= (1 << CSET_UNICODE);
				charset_masks[0x80 | (wc & 0x3f)] |= (1 << CSET_UNICODE);
			} else if(wc <= 0x10000) {
				charset_masks[0xe0 | (wc >> 12)] |= (1 << CSET_UNICODE);
				charset_masks[0x80 | ((wc >> 6) & 0x3f)] |= (1 << CSET_UNICODE);
				charset_masks[0x80 | (wc & 0x3f)] |= (1 << CSET_UNICODE);
			} else
				assert(0); /* non-BMP chars are not currently used by Crawl */
		}

		reset();
	}

	unsigned choose_charset(unsigned pc) {
		for(unsigned i = 0; i < NUM_CSET; ++i) {
			if(pc & (1 << i))
				return i;
		}
		return NUM_CSET;
	}

	int utf8_len(unsigned char c)
	{
		if (c < 0x80)
			return 1;
		else if((c & 0xc0) == 0x80)
			return -1;
	        else if ((c & 0xe0) == 0xc0)
			return 2;
	        else if ((c & 0xf0) == 0xe0)
			return 3;
	        else /* telnet characters */
			return 1;
	}

	virtual void process(const string& s)
	{
		int utf8_left = -1;
		for(unsigned i = 0; i < s.size(); ++i) {
			unsigned char c = (unsigned char)s[i];

			if(c == clear_screen[clear_screen_seq_place]) {
				++clear_screen_seq_place;
				if(!clear_screen[clear_screen_seq_place]) {
					clear_screen_seq_place = 0;
					prev_screen_possible_charsets = screen_possible_charsets;
					screen_possible_charsets = (1 << NUM_CSET) - 1;
				}
			} else
				clear_screen_seq_place = 0;

			unsigned pc = screen_possible_charsets & charset_masks[c];
			int ul = utf8_len(c);
			if(ul >= 0) {
				if(utf8_left > 0) {
//					cerr << "Bad utf8: utf8_len is " << utf8_left << " and I got a " << hex << (unsigned)c << dec << endl;
					pc &= ~(1 << CSET_UNICODE);
				}
				else
					utf8_left = ul;
			}

			if(!utf8_left) {
//				cerr << "Bad utf8: overlong sequence" << endl;
				pc &= ~(1 << CSET_UNICODE);
			}
			else if(utf8_left > 0)
				--utf8_left;

			if(!pc) {
				if(!warned_about_charsets) {
					cerr << "Multiple incompatible charsets used: resetting charset detection (caused by " << hex << (unsigned)c << dec << ')' << endl;
					//warned_about_charsets = true;
				}
				pc = (1 << NUM_CSET) - 1;
			}

			screen_possible_charsets = pc;
		}

		unsigned fpc = prev_screen_possible_charsets & screen_possible_charsets;
		if(!fpc)
			fpc = screen_possible_charsets;
		if(fpc != possible_charsets) {
			possible_charsets = fpc;
			charset = choose_charset(possible_charsets);
			/*
			if(possible_charsets & ~(1 << charset))
				cerr << "Guessing charset " << charset_friendly_names[charset] << endl;
			else
				cerr << "Detected charset " << charset_friendly_names[charset] << endl;
			*/
		}
	}
};

CrawlCharsetDetector* CrawlCharsetDetector::create()
{
	return new CrawlCharsetDetectorImpl();
}
