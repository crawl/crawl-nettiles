/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#ifndef CRAWL_CHARSET_DETECTOR_H_
#define CRAWL_CHARSET_DETECTOR_H_

struct CrawlCharsetDetector
{
	unsigned charset;
	unsigned possible_charsets;

	virtual void reset() = 0;
	virtual void process(const std::string& s) = 0;

	static CrawlCharsetDetector* create();
};

#endif /* CRAWL_CHARSET_DETECTOR_H_ */
