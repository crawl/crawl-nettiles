/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#ifndef CELL_H_
#define CELL_H_

struct cattr_t
{
	uint8_t fg : 4;
	uint8_t bg : 4;

	operator uint8_t()
	{
		return (bg << 4) | fg;
	}
};

union xattr_u
{
	struct xattr_t : public cattr_t
	{
		uint8_t reverse : 1;
		uint8_t int_cursor : 1;
		uint8_t vis_cursor : 1;
		uint8_t unused : 5;

		cattr_t simplify() const
		{
			cattr_t ca;
			if(reverse ^ vis_cursor) {
				ca.fg = bg;
				ca.bg = fg;
			} else {
				ca.fg = fg;
				ca.bg = bg;
			}
			return ca;
		}

		operator uint8_t() const
		{
			return ((const xattr_u*)this)->b;
		}

		operator uint16_t() const
		{
			return ((const xattr_u*)this)->v;
		}
	};

	xattr_t s;
	uint8_t b;
	uint16_t v;
};

typedef xattr_u::xattr_t xattr_t;

struct wcell
{
	wchar_t c;
	xattr_u a;
};

struct simple_cell
{
	char c;
	cattr_t a;
};

struct span
{
	int x;
	std::string s;
	cattr_t a;
};

/* currently we don't need wchar_t versions of these helpers, so don't template them for now */

#define GENERATE_SPANS_INCLUDE_SPACES 1
#define GENERATE_SPANS_WORDS 2

void generate_spans(std::vector<span>& res, const simple_cell* line, size_t size, unsigned flags = 0);

static inline void generate_spans(std::vector<span>& res, const std::vector<simple_cell>& line, unsigned flags = 0)
{
	generate_spans(res, &line[0], line.size(), flags);
}

static inline void generate_spans(std::vector<span>& res, const std::vector<simple_cell>& line, unsigned x, unsigned size, unsigned flags = 0)
{
	if(x >= line.size())
		return;

	int max_size = line.size() - x;
	if(max_size <= 0)
		return;
	if(size > (unsigned)max_size)
		size = max_size;
	generate_spans(res, &line[x], size, flags);
}

#endif /* CELL_H_ */
