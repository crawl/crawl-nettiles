/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#include "AppHdr.h"

#include "tiledef-gui.h"
#include "tiledef-dngn.h"
#include "menu.h"
#include "arena.h"
#include "ng-input.h"
#include "cio.h"
#include "state.h"
#include "initfile.h"
#include "message.h"
#include "maps.h"
#include "viewgeom.h"
#include "tilereg-crt.h"
#include "stuff.h"
#include "tilefont.h"

#include "nettiles/nettiles.h"
#include "connection.h"
#include "server_data.h"

#include <sstream>
using namespace std;
using namespace std::tr1;
using namespace std::tr1::placeholders;

struct server_info
{
	char letter;
	const char* host;
	server_type type;
	const char* abbrev;
	const char* short_desc;
	const char* long_desc;
	unsigned tile;
	int telnet_port;
	const char* ssh_host_hash;
	const char* ssh_username;
	const char* ssh_password;
	const char* ssh_key;
};

struct account_info
{
	unsigned server;
	string username;
	string password;
};

static const char cao_ssh_key[] =
		"-----BEGIN RSA PRIVATE KEY-----\n"
		"MIIEoQIBAAKCAQEA2ztCchPiQL6kRFtk1Rqk/sk1ygnc0PbD6GP1CLOqu/+pD1Dg\n"
		"slIo+TXXxJmX76ChKtk2RanvoSjQQM+igD5cctTXZhWvQrvpOxEucVN9WaE/tl4y\n"
		"8SYYQjov3X3Kr06AASkscSP9mcJp10lxGN+EyFASg7RyCaXOioDAFHZnLy6nkHKl\n"
		"NnWfY1ZEB0V2ujG19v9VVoGNJCmrut8lvThjewo7YG2utX6sqz5GaiUBrjvhWGV0\n"
		"er882ykekCrclMAKxjQcP1+1GZOoSgTuiK8kTtPj5mVQoshqas+R6xbp78p4Os9R\n"
		"B6cWvtp8AdkIJ1b8tGkPtasfRDUdcJ/v1CmZiQIBIwKCAQBwv2QAJ3usRMmCPaGS\n"
		"KvXE3IIQIlRONcPWmc55gPi4dNqaKZghMZFqOPKfn3K1wFLi1hvpUBTWigSznfvM\n"
		"7N8WfBcBTPsMYKPVPAlBmKbdo2KYTbPMef3Y6rmH2kr7Em23dEK91/7EGtdYxq8x\n"
		"XQJ1pYXdVXx5/YAp+RJTqpt3WN6rq377Zjv1wWKb9vzaSlRD6USf7tSx0cvY4RW5\n"
		"NXVHKBZmtqsWb54QO0cHUxjJZ4CGdYc7D99CX/vYRjeJirrwzB4SDbzZGAb2W8hp\n"
		"xOVxIlrKAScPFhLjKvFfbnuk9uB5h/tdC22hnCkvM5KiPwo7EGRKmseBWPcRuH9u\n"
		"pZQjAoGBAPYUr9rm9atYChfCOodtIrOkWVFhbq0Hd44G4SuVdFaTrSR2JaaMnUhJ\n"
		"Iemmt2dnesshFkzq03OA5M4OBtuOrY/1PenXbnx13D8llSPsBRTUeFENSzvqSfL9\n"
		"O6O+Hrl8R7c3OE+KeNiR/BjnXyt9I2Xal8B9s8X2ywMRV47vabHbAoGBAOQRg4xK\n"
		"OSofN7OkAAOkmuyyJ4y+F9DC74McgTN4hsVOyAfNOi1EXbEJqRnDaHhNW2Nqv/Xy\n"
		"aV5ZNFiWfwbP9oEZMGm2nZ7Es1uavVMkVd+QJv7CF9b+txnQTqcP2kAIFH5QKwqB\n"
		"Jj5WLr2weSMZY62f3BpuA0cnPR2D79ri77lrAoGBAKG10qXK+TYVSHYDSwiJi9Ui\n"
		"25vg8PVcraZ5jKecuipDyYz9Lq9GdfxNUMy+A353M2+KxYMAttbfrEzkpWu1h/+D\n"
		"5tt+8NVyBcMYsnaxCqdJy2h2c0ShRom8WmRJvGtDCo5XfMaOMifq534UYxyUD/J5\n"
		"tC4JdiMBQ5RNOYnQh0j9AoGAe87+RNfV46qL884OouRUGheR0A9sA50FrZMhkPhJ\n"
		"KUgGMB7zsixtYBsv6WoUJAy8lQ4Qb5I5Mze9VKl4KEUCKNMo6O4bDQux0qR1Z6YC\n"
		"uzD/OdcUQYL1ru1shpOTvFwoYdO/l/z3gOz0zV/Mu0hMDcvPQYwtqkiAQzj+jMRW\n"
		"QBUCgYBofwHY0pcG8XLp4hoKXf8yA6LQxJ9z+mofRIHOVMpOwPBmTOqwDrr15ovd\n"
		"eOo8P8O22rZYrNpOSV800L9fH6LmDHYZp8ChdK3fHuqcyVGplctXEZ3XMIQhcv4p\n"
		"cPeYhJwUMebhn5QFk8hlhdNQDYZSomXFjonWBpJsrYxcrmfO9Q==\n"
		"-----END RSA PRIVATE KEY-----\n";


static server_info default_servers[] = {
	{'a',
			"crawl.akrasiac.org",
			SERVER_DGAMELAUNCH,
			"CAO",
			"main server, USA",
			"Play or watch others on the main online Crawl server (USA)",
			TILEG_SERVER_CAO,
			23,
			"\x6f\xa3\x52\x4f\xf3\x8e\xb9\x93\xa3\x5d\x1f\xf6\x0b\x8e\x72\xf4", "joshua", "joshua", cao_ssh_key},
	{'d',
			"crawl.develz.org",
			SERVER_DGAMELAUNCH,
			"CDO",
			"preview server, Germany",
			"Play or watch others on the server with Crawl development previews (Germany)",
			TILEG_SERVER_CDO,
			345,
			"\x85\x2e\xa3\xb3\x61\x1e\x80\xa7\x0f\x27\x0b\x68\xc1\x46\xeb\x74", "crawl", 0, cao_ssh_key},
	{'f',
			"rl.heh.fi",
			SERVER_RLSERVER,
			"RHF",
			"Finland",
			"Play or watch others on rl.heh.fi, a Finnish roguelike server",
			TILEG_SERVER_RHF,
			23,
			"\x18\x9b\x3a\xca\x1c\xe2\xb3\xb1\x72\xb1\xe2\xed\x3b\xe5\xa8\xfa", "rl", 0, 0},
	{'w',
			"termcast.org",
			SERVER_TV,
			"TV",
			"Crawl TV channels",
			"Watch the C_SPLAT automated TV channel or requested games on FooTV\n"
			"Interact with the Henzell bot in #crawl on freenode IRC to request games",
			TILEG_TV,
			23,
			0, 0, 0, 0}
};

#define ONLINE_MENU_TYPE_MASK 0xf0000000
#define ONLINE_MENU_TYPE_SERVER 0x10000000
#define ONLINE_MENU_TYPE_ACCOUNT 0x20000000

#define ONLINE_MENU_QUIT 1
#define ONLINE_MENU_ARENA 3
#define ONLINE_MENU_TTYREC 4
#define ONLINE_MENU_PROGRAM 5
#define ONLINE_MENU_FORGET 6

/*
void read_account_file()
{
	ifstream account;
	account.open(file.c_str());
	if (!account) {
		cerr << "Unable to read " << file << " (which should contain url, username and password on separate lines)" << endl;;
		exit(1);
	}
	string url;
	string username;
	string password;
	account >> url;
	account >> username;
	account >> password;
	account.close();
	username.append("\n");
	password.append("\n");
}
*/

struct OnlineUI
{
	vector<server_info> servers;
	vector<account_info> accounts;
	bool loaded;

	PrecisionMenu* online_menu;
	bool show_abbrevs;

	// TODO: set rows/cols properly
	unsigned rows;
	unsigned cols;

	OnlineUI()
	: loaded(false), show_abbrevs(false), online_menu(0), rows(25), cols(80)
	{}

	string get_config_file()
	{
		// TODO: should have a config option for this maybe
		string filename = !SysEnv.crawl_dir.empty()  ? SysEnv.crawl_dir : "";
		if (!filename.empty() && filename[filename.size() - 1] != FILE_SEPARATOR)
			filename += FILE_SEPARATOR;

		filename += "login.txt";
		return filename;
	}

	void load()
	{
		if(loaded)
			return;
		loaded = true;

		// TODO: maybe make servers configurable
		servers.insert(servers.begin(), default_servers, default_servers + sizeof(default_servers) / sizeof(default_servers[0]));

		string s;
		string filename = get_config_file();
		ifstream in(filename.c_str());

		while(getline(in, s)) {
			istringstream ss(s);
			string type;
			ss >> type;
			if(type[0] == '#')
				continue;
			if(type == "account") {
				account_info account;
				string server;
				ss >> server >> account.username >> account.password;
				for(account.server = 0; account.server < servers.size(); ++account.server) {
					if(servers[account.server].host == server)
						break;
				}
				if(account.server >= servers.size() || account.username.empty())
					cerr << "Bad login file line: account " << server << ' ' << account.username << endl;
				else
					accounts.push_back(account);
			}
		}

		/* TODO: is this really totally right?
		 * TODO: move this in the proper place
		 */
		cols = crawl_view.viewsz.x + 2 + 42;
		if(cols & 1)
			++cols;
		rows = crawl_view.viewsz.y + 7;

		/* If we don't do this, text will be screwed up
		 * Viewport cutting tries to solves the too-large viewport problem.
		 */
		if(cols < 80)
			cols = 80;
		if(rows < 24)
			rows = 24;
	}

	void save()
	{
		string filename = get_config_file();
		ofstream out(filename.c_str());
		out << "# This file contains logins for online Crawl servers\n";
		out << "# Crawl automatically writes your logins here and adds main menu entries for them\n";
		out << "# Format:\n";
		out << "# account <hostname> <username> <password>\n";
		out << "\n";
		for(unsigned i = 0; i < accounts.size(); ++i)
			out << "account " << servers[i].host << ' ' << accounts[i].username << ' ' << accounts[i].password << '\n';
	}

	void save_and_new_online_menu()
	{
		save();
		delete online_menu;
		online_menu = 0;
	}

	void handle_login(unsigned server, const std::string& username, const std::string& password)
	{
		account_info account;
		account.server = server;
		account.username = username;
		account.password = password;
		accounts.push_back(account);
		save_and_new_online_menu();
	}

	static const int SCROLLER_MARGIN_X  = 20;
	static const int MENU_WIDTH   = 58;

	static TextItem* create_text_item(const std::string& text)
	{
		TextItem* item = new TextItem();
		item->set_text(text);
		return item;
	}

	static TextItem* create_text_item(const std::string& text, tile_def tile, bool use_tile = true)
	{
		if(!use_tile)
			return create_text_item(text);

		TextTileItem* item;
		item = new TextTileItem();
		item->add_tile(tile);
		item->set_text(string(" ") + text);
		return item;
	}

	static TextItem* create_text_item(const std::string& text, unsigned tile, bool use_tile = true)
	{
		return create_text_item(text, tile_def(tile, TEX_GUI), use_tile);
	}

	static unsigned tiles_height(unsigned items, bool use_tiles = true)
	{
		if(!use_tiles)
			return items;
		unsigned ch = tiles.get_crt_font()->char_height();
		return ((items * 32 + ch - 1) / ch);
	}

	void setup_online_menu()
	{
		if(online_menu) {
			// TODO: it should be possible to reuse the menu...
			delete online_menu;
		}

		bool accounts_tiles = true;
		bool servers_tiles = true;
		bool tools_tiles = true;

retry:
		unsigned menu_y = wherey() + 1;
		// XXX: limit the number of accounts displayed
		unsigned accounts_h = accounts.empty() ? 1 : tiles_height(accounts.size() + 1, accounts_tiles);
		unsigned servers_h = tiles_height((sizeof(default_servers) / sizeof(default_servers[0])), servers_tiles);
#ifdef create_local_connection
		unsigned tools_h = tiles_height(2, tools_tiles);
#else
		unsigned tools_h = tiles_height(1, tools_tiles);
#endif
		unsigned footer_h = 4;

		unsigned accounts_y = menu_y;
		unsigned servers_y = accounts_y + accounts_h + 1;
		unsigned tools_y = servers_y + servers_h + 1;
		unsigned footer_y = tools_y + tools_h + 1;
		unsigned end_h = footer_y + footer_h;

		if(end_h > get_number_of_lines()) {
			if(tools_tiles) {
				tools_tiles = false;
				goto retry;
			} else if(servers_tiles) {
				servers_tiles = false;
				goto retry;
			} else if(accounts_tiles) {
				accounts_tiles = false;
				goto retry;
			}
		}

		online_menu = new PrecisionMenu();
		online_menu->set_select_type(PrecisionMenu::PRECISION_SINGLESELECT);

		MenuFreeform* freeform = new MenuFreeform();
		freeform->init(coord_def(1, 1), coord_def(get_number_of_cols() - 1, get_number_of_lines() - 1), "freeform");
		freeform->allow_focus(false);

		TextItem* item;

		item = new NoSelectTextItem();
		item->set_text("Manual login:");
		item->set_bounds(coord_def(1, servers_y), coord_def(SCROLLER_MARGIN_X, servers_y + 1));
		freeform->attach_item(item);
		item->set_visible(true);

		item = new NoSelectTextItem();
		item->set_text("Automatic login:");
		item->set_bounds(coord_def(1, accounts_y), coord_def(SCROLLER_MARGIN_X, accounts_y + 1));
		freeform->attach_item(item);
		item->set_visible(true);

		item = new NoSelectTextItem();
		item->set_text("Other tools:");
		item->set_bounds(coord_def(1, tools_y), coord_def(SCROLLER_MARGIN_X, tools_y + 1));
		freeform->attach_item(item);
		item->set_visible(true);

		item = new NoSelectTextItem();
		item->set_text("Click on an item, navigate with the keyboard, or press '?' for shortcuts.\n");
		item->set_bounds(coord_def(1, footer_y), coord_def(get_number_of_cols() - 2, footer_y + 1));
		freeform->attach_item(item);
		item->set_visible(true);

		MenuScroller* accounts_scroller = new MenuScroller();
		accounts_scroller->init(coord_def(SCROLLER_MARGIN_X, accounts_y), coord_def(get_number_of_cols() - 1, accounts_y + accounts_h), "save games");

	        if(!accounts.empty()) {
			for(unsigned i = 0; i < accounts.size(); ++i) {
				ostringstream ss;
				if(show_abbrevs) {
					if(i < 9)
						ss << (char)('1' + i) << " - ";
					else
						ss << "    ";
				}
				ss << accounts[i].username << " on " << servers[accounts[i].server].host;
				item = create_text_item(ss.str(), servers[accounts[i].server].tile + 1, accounts_tiles);

				item->set_description_text(servers[accounts[i].server].long_desc);

				item->set_bounds(coord_def(1, 1), coord_def(1, 2));
				item->set_fg_colour(YELLOW);
				item->set_highlight_colour(WHITE);
				item->set_id(ONLINE_MENU_TYPE_ACCOUNT | i);

				accounts_scroller->attach_item(item);
				item->set_visible(true);
			}

			ostringstream ss;
			if(show_abbrevs)
				ss << "r - ";
			ss << "Remove an account from this list";
			item = create_text_item(ss.str(), TILEG_DELETE_ACCOUNT, accounts_tiles);
			item->set_description_text("Remove an account from this list (but not from the server)");

			item->set_bounds(coord_def(1, 1), coord_def(1, 2));
			item->set_fg_colour(RED);
			item->set_highlight_colour(WHITE);
			item->set_id(ONLINE_MENU_FORGET);

			accounts_scroller->attach_item(item);
			item->set_visible(true);
	        } else {
			item = new NoSelectTextItem();
			item->set_text("no account details stored, login manually first");
			item->set_fg_colour(LIGHTGREY);
			item->set_bounds(coord_def(1, 1), coord_def(1, 2));
			accounts_scroller->attach_item(item);
			item->set_visible(true);
	        }

		MenuScroller* servers_scroller = new MenuScroller();
		servers_scroller->init(coord_def(SCROLLER_MARGIN_X, servers_y), coord_def(get_number_of_cols() - 1, servers_y + servers_h), "servers");

	        for(unsigned i = 0; i < servers.size(); ++i)
	        {
			ostringstream ss;
			if(show_abbrevs)
				ss << servers[i].letter << " - ";
			ss << servers[i].host << " (" << servers[i].short_desc << ")";
			item = create_text_item(ss.str(), servers[i].tile, servers_tiles);

			item->set_description_text(servers[i].long_desc);

			item->set_bounds(coord_def(1, 1), coord_def(1, 2));
			item->set_fg_colour(WHITE);
			item->set_highlight_colour(WHITE);
			item->set_id(ONLINE_MENU_TYPE_SERVER | i);

			servers_scroller->attach_item(item);
			item->set_visible(true);
	        }

		MenuScroller* tools_scroller = new MenuScroller();
		tools_scroller->init(coord_def(SCROLLER_MARGIN_X, tools_y), coord_def(get_number_of_cols() - 1, tools_y + tools_h), "tools");

		{
			ostringstream ss;
			if(show_abbrevs)
				ss << "t - ";
			ss << "Replay a .ttyrec play recording";
			item = create_text_item(ss.str(), TILEG_PLAY, tools_tiles);
		}

		item->set_description_text(
				"Watch a Crawl play recording in .ttyrec format\n"
				"ttyrecs for games on CAO are on http://crawl.akrasiac.org/rawdata/<player>");

		item->set_bounds(coord_def(1, 1), coord_def(1, 2));
		item->set_fg_colour(MAGENTA);
		item->set_highlight_colour(WHITE);
		item->set_id(ONLINE_MENU_TTYREC);

		tools_scroller->attach_item(item);
		item->set_visible(true);

#ifdef create_local_connection
		{
			ostringstream ss;
			if(show_abbrevs)
				ss << "p - ";
			ss << "Run any program (advanced)";
			item = create_text_item(ss.str(), TILEG_TERMINAL, tools_tiles);
		}
		item->set_description_text("Run any program showing Crawl-like output in this window using tiles\n"
								"This can be used to run console Crawl or connect to an unlisted server\n");

		item->set_bounds(coord_def(1, 1), coord_def(1, 2));
		item->set_fg_colour(MAGENTA);
		item->set_highlight_colour(WHITE);
		item->set_id(ONLINE_MENU_PROGRAM);

		tools_scroller->attach_item(item);
		item->set_visible(true);
#endif

		online_menu->attach_object(freeform);
		online_menu->attach_object(accounts_scroller);
		online_menu->attach_object(servers_scroller);
		online_menu->attach_object(tools_scroller);

		MenuDescriptor* descriptor = new MenuDescriptor(online_menu);
		descriptor->init(coord_def(1, footer_y + 2), coord_def(get_number_of_cols() - 1, footer_y + 3), "descriptor");
		online_menu->attach_object(descriptor);

#ifdef USE_TILE
		// Black and White highlighter looks kinda bad on tiles
		BoxMenuHighlighter* highlighter = new BoxMenuHighlighter(online_menu);
#else
		BlackWhiteHighlighter* highlighter = new BlackWhiteHighlighter(online_menu);
#endif
		highlighter->init(coord_def(-1, -1), coord_def(-1, -1), "highlighter");
		online_menu->attach_object(highlighter);

		freeform->set_visible(true);
		accounts_scroller->set_visible(true);
		servers_scroller->set_visible(true);
		tools_scroller->set_visible(true);
		descriptor->set_visible(true);
		highlighter->set_visible(true);
	}

	void run_nettiles(Connection* conn, int activity_mode, int server = -1)
	{
		if(!conn)
			return;

		tiles.initialise_items();

		NetTiles* nettiles = NetTiles::create();
		signal_connection login_connection;
		if(server >= 0)
			login_connection = nettiles->login_signal.connect(std::tr1::bind(&OnlineUI::handle_login, this, server, _1, _2));
		nettiles->run(conn, activity_mode);
		if(server >= 0)
			login_connection.remove();
		delete nettiles;
	}

	void handle_program()
	{
#ifdef create_local_connection
		cgotoxy(1, 1);
		clrscr();
		textcolor(YELLOW);
		cprintf("Welcome to the Crawl NetTiles universal terminal emulator!\n");
		textcolor(LIGHTGRAY);
		cprintf(
				"\n"
				"Crawl NetTiles supports running arbitrary programs inside the Crawl window\n"
				"If the program ends up displaying a Crawl sessions, it will be shown with Tiles\n"
				"\n"
				"Example of useful commands follow.\n"
				"- View ttyrecs:\n"
				"  ipbt <ttyrec>\n"
				"- Display a local console crawl binary with Tiles\n"
				"  my_crawl_console/bin/crawl\n"
				"- Connect to an unlisted server\n"
				"  ssh <options> <server> # to connect to an unlisted\n"
				"- Shell, for running other programs through it\n"
				"  bash\n"
				);

		char buf[4096];
		int response;

#ifdef WIN32
#define DEFAULT_COMMAND "cmd"
#else
#define DEFAULT_COMMAND "bash"
#endif

		cprintf("\nCommand to run (enter for " DEFAULT_COMMAND "): ");
		response = cancelable_get_line(buf, sizeof(buf));
		if(!response) {
			if(!buf[0])
				strcpy(buf, DEFAULT_COMMAND);
			run_nettiles(create_local_connection(buf, rows, cols), 1);
		}
#endif
	}

	void handle_ttyrec()
	{
		cgotoxy(1, 1);
		clrscr();
		textcolor(YELLOW);
		cprintf("Welcome to the Crawl ttyrec player!\n");
		textcolor(LIGHTGRAY);
		cprintf(
				"\n"
				"Crawl play sessions can be recorded to .ttyrec format for later viewing using the \"ttyrec\" program.\n"
				"\n"
				"Online servers automatically do so for all players. You can find such ttyrecs at:\n"
				"http://crawl.akrasiac.org/rawdata/PlayerName/\n"
				"http://crawl.develz.org/ttyrecs/PlayerName/\n"
				"http://rl.heh.fi/crawl/stuff/PlayerName/\n"
				"http://rl.heh.fi/crawl-0.6/stuff/PlayerName/\n"
				"http://rl.heh.fi/trunk/stuff/PlayerName/\n"
				"\n"
				"These files must be decompressed with bunzip2 or 7-Zip before viewing\n"
				"\n"
				"On Unix systems, it is often better to choose the \"Run program\" option, and launch ipbt on the ttyrec filename\n"
				"ipbt will allow you to navigate inside the .ttyrec and even search inside it\n"
				"\n"
				"Press '+' or 'f' to speed up, '-' or 's' to slow down, and '1' to restore normal speed\n"
				"Press enter to go forward 1 second, space to pause and unpause, 'q' or esc to quit\n"
				);

		char buf[4096];
		int response;

		cprintf("\nUncompressed .ttyrec filename: ");
		response = cancelable_get_line(buf, sizeof(buf));
		if(!response)
			run_nettiles(create_ttyrec_connection(buf), -1);
	}

	void handle_arena()
	{
		cgotoxy(1, 1);
		clrscr();
		textcolor(YELLOW);
		cprintf("Welcome to Crawl's Arena mode!\n");
		textcolor(LIGHTGRAY);
		cprintf(
				"\n"
				"In the arena mode, you can pit two factions of monsters of your choice against each other.\n"
				"Crawl's artificial intelligence will automatically act for both factions until either is wiped out.\n"
				"\n"
				"Specify teams as a comma separated list of singular monster names with optional number\n"
				"Example: Sigmund, 2 orc warrior, 4 orc\n"
				);
		ostringstream ss;
		char buf[4096];
		int response;

retry1:
		cprintf("\nFirst faction (enter for random): ");
		response = cancelable_get_line(buf, sizeof(buf));
		if(response)
			return;
		if(!buf[0])
			ss << "random";
		else {
			if(!arena_is_faction_valid(buf)) {
				cprintf("\nInvalid faction specification!");
				goto retry1;
			}
			ss << buf;
		}

		ss << " v ";

retry2:
		cprintf("\nSecond faction (enter for random): ");
		response = cancelable_get_line(buf, sizeof(buf));
		if(response)
			return;

		if(!buf[0])
			ss << "random";
		else {
			if(!arena_is_faction_valid(buf)) {
				cprintf("\nInvalid faction specification!");
				goto retry2;
			}
			ss << buf;
		}

retry_map:
		cprintf("\nMap name (enter for simple square): ");
		response = cancelable_get_line(buf, sizeof(buf));
		if(response)
			return;

		if(buf[0]) {
			std::string map_name = "arena_";
			map_name += buf;
			if(map_count_for_tag(map_name) <= 0) {
				cprintf("\nInvalid arena map!");
				goto retry_map;
			}

			ss << " arena:" << buf;
		}

retry_delay:
		cprintf("\nTime to wait between turns, in seconds (less than 2, fractions allowed, enter for default): ");
		response = cancelable_get_line(buf, sizeof(buf));
		if(response)
			return;

		if(buf[0]) {
			char* end = 0;
			double v = strtod(buf, &end);
			if(end == buf || !(v >= 0.0) || !(v < 2.0)) {
				cprintf("\nInvalid delay specification (must be positive and less than 2.0)!");
				goto retry_delay;
			}

			ss << " delay:" << (unsigned)(v * 1000.0);
		}

		you.init();

		crawl_state.type = GAME_TYPE_ARENA;
		run_arena(ss.str());
		flush_prev_message();
		crawl_state.type = GAME_TYPE_NORMAL;
	}

	void handle_network(const account_info& account)
	{
		Connection* conn;
		server_info& server = servers[account.server];
		bool use_ssh = false;

#ifdef HAVE_LIBSSH
		if(server.ssh_host_hash)
			use_ssh = true;
#endif

		cgotoxy(1, 1);
		clrscr();
		ostringstream ss;
		ss << "Connecting to " << server.host << " using " << (use_ssh ? "SSH" : "Telnet") << "...\n";
		cprintf(ss.str().c_str());
		tiles.redraw();

#ifdef HAVE_LIBSSH
		if(use_ssh)
			conn = create_ssh_connection(server.host, server.ssh_host_hash, server.ssh_username, server.ssh_password, server.ssh_key, rows, cols);
		else
#endif
			conn = create_telnet_connection(server.host, server.telnet_port, rows, cols);

		if(!conn)
			return;

		if(!account.username.empty()) {
			conn->transmit("l", 1);
			conn->transmit(account.username.data(), account.username.size());
			conn->transmit("\n", 1);

			if(!account.password.empty()) {
				conn->transmit(account.password.data(), account.password.size());
				conn->transmit("\n", 1);
			}
		}

		run_nettiles(conn, 1, account.server);
	}

	void handle_forget()
	{
		MenuEntry* me;
		Menu menu(MF_SINGLESELECT | MF_ALLOW_FORMATTING, "main", false);

		for(;;) {
			menu.clear();
			me = new MenuEntry("Forget about account details: (ESC to cancel)", MEL_TITLE);
			menu.set_title(me, true);

			for(unsigned i = 0; i < accounts.size(); ++i) {
				ostringstream ss;
				ss << "Forget about account " << accounts[i].username << " on " << servers[accounts[i].server].host;

				me = new MenuEntry(ss.str(), MEL_ITEM, 1, (i < 9) ? ('1' + i) : 0);
				me->add_tile(tile_def(servers[accounts[i].server].tile + 1, TEX_GUI));

				me->data = (void*)(ONLINE_MENU_TYPE_ACCOUNT | i);
				menu.add_entry(me);
			}

			std::vector<MenuEntry*> sels;

			sels = menu.show();
			if(sels.empty())
				break;

			int idx = ((unsigned)(uintptr_t)sels[0]->data & ~ONLINE_MENU_TYPE_MASK);

			accounts.erase(accounts.begin() + idx);
			save_and_new_online_menu();
		}
	}

	void run()
	{
		load();

	        // TODO: set properly/make configurable
		for(;;) {
			std::vector<MenuItem*> sels;

			cgotoxy(1, 1, GOTO_CRT);
			clrscr();
			opening_screen();
			textcolor(LIGHTCYAN);
			cprintf("\n"
				"Graphical online play is experimental and imperfections are present.\n"
				"Use Ctrl-V to switch to the text view (and back) in case of problems.\n");

			setup_online_menu();

#ifdef USE_TILE
			tiles.get_crt()->attach_menu(online_menu);
#endif
			online_menu->draw_menu();

			unsigned sel = 0;
			for(;;) {
				int keyn = getch_ck();

				switch(keyn) {
				case CK_ESCAPE:
				case 'q':
					sel = ONLINE_MENU_QUIT;
					break;
				case 'r':
					sel = ONLINE_MENU_FORGET;
					break;
				case 't':
					sel = ONLINE_MENU_TTYREC;
					break;
				case 'p':
					sel = ONLINE_MENU_PROGRAM;
					break;
				default:
					if('1' <= keyn && keyn <= '9')
						sel = ONLINE_MENU_TYPE_ACCOUNT | (keyn - '1');
					else {
						for(unsigned i = 0; i < servers.size(); ++i) {
							if(servers[i].letter == keyn) {
								sel = ONLINE_MENU_TYPE_SERVER | i;
								break;
							}
						}
					}
				}

				if(sel)
					break;

				const char* allowed_keys = "<>+-\"=;";
				if(keyn <= ' ' || keyn >= 128 || strchr(allowed_keys, (char)keyn)) {
					online_menu->process_key(keyn);

					sels = online_menu->get_selected_items();
					if(!sels.empty()) {
						sel = sels[0]->get_id();
						break;
					}
				} else if(!show_abbrevs) {
					show_abbrevs = true;
					delete online_menu;
					online_menu = 0;
					break;
				}
			}

			try
			{
				if((sel & ONLINE_MENU_TYPE_MASK) == 0) {
					if(sel == ONLINE_MENU_QUIT)
						return;
					else if(sel == ONLINE_MENU_PROGRAM)
						handle_program();
					else if(sel == ONLINE_MENU_TTYREC)
						handle_ttyrec();
					else if(sel == ONLINE_MENU_ARENA)
						handle_arena();
					else if(sel == ONLINE_MENU_FORGET)
						handle_forget();
				} else if((sel & ONLINE_MENU_TYPE_MASK) == ONLINE_MENU_TYPE_SERVER) {
					account_info account;
					account.server =  sel & ~ONLINE_MENU_TYPE_MASK;
					handle_network(account);
				} else if((sel & ONLINE_MENU_TYPE_MASK) == ONLINE_MENU_TYPE_ACCOUNT) {
					handle_network(accounts[sel & ~ONLINE_MENU_TYPE_MASK]);
				}
			}
			catch(std::exception& e)
			{
				if(tiles.get_cursor_region() != GOTO_CRT)
					cgotoxy(1, 1, GOTO_CRT);
				cprintf("Error: %s\nPress any key or click to return to the menu.\n", e.what());
				tiles.set_need_redraw(0);
				getch_ck();
			}
		}
	}
};

OnlineUI online_ui;

void run_online()
{
	online_ui.run();
}
