/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#include "AppHdr.h"
#include <stdlib.h>
#include <SDL.h>
#include <math.h>
#include <iostream>
#include "connection.h"

#ifdef WIN32
#define le32toh(x) (x)
#else
#include <endian.h>
#endif

using namespace std;

#define READ_LIMIT 4095
#define BUFFER_SIZE 65536

double get_time()
{
	return SDL_GetTicks() * 0.001;
}

void sleep_time(double v)
{
	SDL_Delay((unsigned)(v * 1000.0));
}

struct Ttyrec : public Connection {
	FILE* fp;
	size_t record_left;
	bool started;
	double last_real_time;
	double virtual_time;
	int speed_exp;
	double speed;
	double start_rec_time;
	bool start_rec_time_set;
	bool paused;
	bool quit;
	double wait_limit;
	SDL_mutex* mutex;
	SDL_cond* cond;

	Ttyrec(const char* filename)
	: record_left(0), started(false), last_real_time(-1), virtual_time(0), speed_exp(0), speed(1), start_rec_time(0), start_rec_time_set(false), paused(false), quit(false), wait_limit(1.0)
	{
		mutex = SDL_CreateMutex();
		cond = SDL_CreateCond();
		fp = fopen(filename, "r");
	}

	~Ttyrec()
	{
		fclose(fp);
		SDL_DestroyCond(cond);
		SDL_DestroyMutex(mutex);
	}

	void update_virtual_time()
	{
		if(!started) {
			last_real_time = get_time();
			started = true;
		} else {
			double cur_time = get_time();
			if(!paused)
				virtual_time += (cur_time - last_real_time) * speed;
			last_real_time = cur_time;
		}
	}


	void wait_until_virtual_time(double wait_until)
	{
		SDL_LockMutex(mutex);
		while(!quit)
		{
			update_virtual_time();
			double towait = wait_until - virtual_time;
			if(towait <= 0.0)
				break;

			if(towait > wait_limit) {
				virtual_time += (towait - wait_limit);
				towait = wait_limit;
			}

			unsigned real_ms = (unsigned)(towait * 1000.0 / speed);
			if(!real_ms)
				break;

			cerr << "WAIT: " << real_ms << endl;
			SDL_CondWaitTimeout(cond, mutex, real_ms);
		}
		SDL_UnlockMutex(mutex);
	}

	ptrdiff_t retrieve(void* buf, size_t size)
	{
		while(!record_left) {
			struct {
				unsigned sec;
				unsigned usec;
				unsigned len;
			} hdr;

			if(quit)
				return 0;

			int ret = fread(&hdr, sizeof(hdr), 1, fp);
			if(ret <= 0)
				return ret;

			double rec_time = hdr.usec * 0.000001 + hdr.sec;
			if(!start_rec_time_set) {
				start_rec_time = rec_time;
				start_rec_time_set = true;
			}

			wait_until_virtual_time(rec_time - start_rec_time);

			record_left = le32toh(hdr.len);
			cout << "LEFT: " << record_left << endl;
		}

		if(quit)
			return 0;

		unsigned toread = record_left;
		if(size < toread)
			toread = size;

		int ret = fread(buf, 1, toread, fp);
		if(ret > 0)
			record_left -= ret;
		return ret;
	}

	ptrdiff_t transmit(const void* buf, size_t size)
	{
		const char* p = (const char*)buf;
		/* this is the only place that can change these, and cannot be called concurrently */
		int new_speed_exp = speed_exp;
		bool new_paused = paused;
		bool new_quit = quit;
		double virtual_time_offset = 0.0;
		for(unsigned i = 0; i < size; ++i)
		{
			char c = p[i];
			if(c == '+' || c == 'f')
				++new_speed_exp;
			else if(c == '-' || c == 's')
				--new_speed_exp;
			else if(c == '1')
				new_speed_exp = 0;
			else if(c == '\r')
				virtual_time_offset += wait_limit;
			else if(c == ' ')
				new_paused = !paused;
			else if(c == '\x1b' || c == 'q')
				new_quit = true;
		}
		if(new_speed_exp != speed_exp || new_paused != paused || new_quit != quit || virtual_time_offset) {
			SDL_LockMutex(mutex);
			update_virtual_time();
			virtual_time += virtual_time_offset;
			speed_exp = new_speed_exp;
			speed = pow(2.0, speed_exp / 4.0);
			paused = new_paused;
			quit = new_quit;
			SDL_CondBroadcast(cond);
			SDL_UnlockMutex(mutex);
		}
		return size;
	}
};

Connection* create_ttyrec_connection(const std::string& filename)
{
	return new Ttyrec(filename.c_str());
}
