/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#ifndef ONLINE_STARTUP_H
#define ONLINE_STARTUP_H

void run_online();

#endif
