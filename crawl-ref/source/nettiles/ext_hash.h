/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#ifndef EXT_HASH_H_
#define EXT_HASH_H_

#ifdef _MSC_VER
#include <functional>
#else
#include <tr1/functional>
#endif

namespace std
{
	namespace tr1 {
		template <class T>
		inline void hash_combine(std::size_t& seed, T const& v)
		{
			std::tr1::hash<T> hashobj;
			seed ^= hashobj(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
		}

		template <class A, class B>
		struct hash<std::pair<A, B> > : public std::unary_function<std::pair<A, B>, size_t>
		{
			size_t
			operator()(std::pair<A, B> const& v) const
			{
				std::size_t seed = 0;
				hash_combine(seed, v.first);
				hash_combine(seed, v.second);
				return seed;
			}
		};
	}
}

#endif /* EXT_HASH_H_ */
