/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#include "AppHdr.h"
#include "libutil.h"

#include "launcher_parser.h"
#include "server_data.h"
#include "line_discipline.h"

#include <vector>
#include <iostream>
using namespace std;
using namespace std::tr1;
using namespace std::tr1::placeholders;

struct LauncherParserImpl : public LauncherParser
{
	Screen* screen;

	signal_connection screen_update_connection;

	LauncherParserImpl(Screen* pscreen)
	: screen(pscreen)
	{
		screen_update_connection = screen->update_signal.connect(std::tr1::bind(&LauncherParserImpl::handle_screen_update, this, _1));
	}

	~LauncherParserImpl()
	{
		screen_update_connection.remove();
	}

	string current_login;

	void handle_screen_update(Screen& the_screen)
	{
		vector<vector<simple_cell> > simple_cells;
		vector<vector<span> > screen_spans;

		screen->compute_simple_cells(simple_cells, 0);
		screen_spans.resize(simple_cells.size());
		for(unsigned i = 0; i < simple_cells.size(); ++i)
			generate_spans(screen_spans[i], simple_cells[i]);

		server_type type = SERVER_UNKNOWN;
		string login;

		/* check for rlserver */
		for(int y = screen_spans.size() - 1; y >= 0; --y) {
			if(!screen_spans[y].empty()) {
				span& h = screen_spans[y][0];
				if(h.x == 0 && h.a.bg == BLUE && h.a.fg == LIGHTGREY)
				{
					string ts = h.s;
					ts = trim_string(ts);
					if(ts == "Not logged in") {
						type = SERVER_RLSERVER;
					} else if(ts == "Logged in as" && screen_spans[y].size() >= 2) {
						type = SERVER_RLSERVER;
						login = screen_spans[y][1].s;
						login = trim_string(login);
					}
				}
				break;
			}
		}

		/* check for dgamelaunch */
		if(type == SERVER_UNKNOWN)
		{
			for(unsigned y = 0; y < screen_spans.size(); ++y)
			{
				for(unsigned i = 0; i < screen_spans[y].size(); ++i)
				{
					string& s = screen_spans[y][i].s;

					size_t lia = s.find("Logged in as: ");
					if(lia != string::npos) {
						type = SERVER_DGAMELAUNCH;
						login = s.substr(lia + sizeof("Logged in as: ") - 1);
						login = trim_string(login);
					}
				}
			}

			/* currently we don't need to detect non-logged-in dgamelaunch, so we don't do so */
		}

		if(!login.empty() && login != current_login) {
			current_login = login;

			login_signal(login);
		}
	}
};

LauncherParser* LauncherParser::create(Screen* screen)
{
	return new LauncherParserImpl(screen);
}
