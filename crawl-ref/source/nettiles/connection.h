/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#ifndef CONNECTION_H
#define CONNECTION_H

#include <fstream>
#include <string>

struct Connection {
	virtual ~Connection() {}

	virtual ptrdiff_t retrieve(void* buf, size_t size) = 0;
	virtual ptrdiff_t transmit(const void* buf, size_t size) = 0;
};

#ifdef HAVE_LIBSSH
Connection* create_ssh_connection(const std::string& host, const char* host_hash, const char* username, const char* password, const char* privkey, unsigned rows, unsigned cols);
#endif
Connection* create_telnet_connection(const std::string& host, unsigned port, unsigned rows, unsigned cols);
#ifdef __unix__
Connection* create_local_connection(const std::string& command, unsigned rows, unsigned cols);
#define create_local_connection create_local_connection
#endif
Connection* create_ttyrec_connection(const std::string& filename);

#endif
