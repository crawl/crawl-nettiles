/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#ifndef CRAWL_PARSER_H_
#define CRAWL_PARSER_H_

#include "screen.h"
#include "viewgeom.h"
#include "signal.h"
#include "crawl_defs.h"

enum screen_type_t
{
	SCREEN_UNKNOWN = 0,
	SCREEN_DUNGEON = 1,
	SCREEN_CRT = 2
};

struct CrawlParser
{
	static CrawlParser* create(Screen* screen);
	virtual ~CrawlParser() {}

	virtual void set_charset(unsigned charset) = 0;
	virtual void enable_tiles(bool value) = 0;

	signal<void(const std::string&)> message_signal;
	signal<void(screen_type_t)> screen_type_changed_signal;
	signal<void(const CrawlFragment&, unsigned, unsigned, const std::vector<std::pair<coord_def, std::string> >&, coord_def)> update_dungeon_signal;
	signal<void(GotoRegion, const std::vector<std::vector<simple_cell> >&)> update_text_region_signal;

	enum screen_type_t screen_type;
	std::vector<std::string> messages;

protected:
	CrawlParser()
	: screen_type(SCREEN_UNKNOWN)
	{}
};

#endif /* CRAWL_PARSER_H_ */
