/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#ifndef NETTILES_H_
#define NETTILES_H_

#include "connection.h"
#include "signal.h"

struct NetTiles
{
	static NetTiles* create();

	signal<void(const std::string&, const std::string&)> login_signal;

	virtual void run(Connection* conn, int activity_mode) = 0; /* conn will be destroyed by this function */
};

#endif /* NETTILES_H_ */
