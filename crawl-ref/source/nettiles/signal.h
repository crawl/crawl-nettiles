/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#ifndef SIGNAL_H_
#define SIGNAL_H_

#ifdef _MSC_VER
#include <functional>
#else
#include <tr1/functional>
#endif

#include <list>

/* Could use Boost.Signals2 instead... */

struct slot_node
{
	virtual ~slot_node() {}

	slot_node* next;
	slot_node* prev;
	unsigned rc;

	void add_ref()
	{
		++rc;
	}

	void release()
	{
		if(!--rc) {
			next->prev = prev;
			prev->next = next;
			delete this;
		}
	}

	slot_node(slot_node* p, slot_node* n)
	: next(n), prev(p), rc(0)
	{
		p->next = this;
		n->prev = this;
	}

	slot_node()
	: rc(0)
	{
		next = this;
		prev = this;
	}
};

template<typename T>
struct slot_list : public slot_node
{
	struct node : public slot_node
	{
		T v;

		node(slot_node* p, slot_node* n, const T& pv)
		: slot_node(p, n), v(pv)
		{}
	};
};

struct signal_connection
{
	signal_connection(slot_node* pp = 0)
	: p(pp)
	{
		if(p)
			p->add_ref();
	}

	signal_connection(const signal_connection& c)
	: p(c.p)
	{
		if(p)
			p->add_ref();
	}

	signal_connection& operator=(signal_connection c)
	{
		if(p)
			p->release();
		p = c.p;
		c.p = 0;
		return *this;
	}

	~signal_connection()
	{
		remove();
	}

	void remove()
	{
		if(p) {
			p->release();
			p = 0;
		}
	}

private:
	slot_node* p;
};

struct signal_connections
{
	std::vector<signal_connection> connections;

	signal_connections& operator +=(const signal_connection& conn)
	{
		connections.push_back(conn);
		return *this;
	}

	void remove()
	{
		connections.clear();
	}
};

#ifndef __has_feature
#define __has_feature(x) 0
#endif

template<typename T>
struct signal
{
	typedef slot_list<std::tr1::function<T> > list_type;
	typedef typename list_type::node list_node_type;
	list_type list;

#define FOR_EACH_SLOT_NODE(node) for(list_node_type *node = (list_node_type*)list.next, *next = (list_node_type*)node->next; (slot_node*)node != (slot_node*)&list; node = next, next = (list_node_type*)node->next)

	signal_connection connect(std::tr1::function<T> fn)
	{
		return new list_node_type(list.prev, &list, fn);
	}

#if __cplusplus > 200900L || defined(__GXX_EXPERIMENTAL_CXX0X__) || defined(__GXX_CXX0X__) || (__has_feature(cxx_variadic_templates) && __has_feature(cxx_rvalue_references))
	template<typename... A>
	void
	operator()(A&&... a) const
	{
		FOR_EACH_SLOT_NODE(node)
			(node->v)(a...);
	}
#else
	void
	operator()() const
	{
		FOR_EACH_SLOT_NODE(node)
			(node->v)();
	}

	template<typename A1>
	void
	operator()(const A1& a1) const
	{
		FOR_EACH_SLOT_NODE(node)
			(node->v)(const_cast<A1&>(a1));
	}

	template<typename A1, typename A2>
	void
	operator()(const A1& a1, const A2& a2) const
	{
		FOR_EACH_SLOT_NODE(node)
			(node->v)(const_cast<A1&>(a1), const_cast<A2&>(a2));
	}

	template<typename A1, typename A2, typename A3>
	void
	operator()(const A1& a1, const A2& a2, const A3& a3) const
	{
		FOR_EACH_SLOT_NODE(node)
			(node->v)(const_cast<A1&>(a1), const_cast<A2&>(a2), const_cast<A3&>(a3));
	}

	template<typename A1, typename A2, typename A3, typename A4>
	void
	operator()(const A1& a1, const A2& a2, const A3& a3, const A4& a4) const
	{
		FOR_EACH_SLOT_NODE(node)
			(node->v)(const_cast<A1&>(a1), const_cast<A2&>(a2), const_cast<A3&>(a3), const_cast<A4&>(a4));
	}

	template<typename A1, typename A2, typename A3, typename A4, typename A5>
	void
	operator()(const A1& a1, const A2& a2, const A3& a3, const A4& a4, const A5& a5) const
	{
		FOR_EACH_SLOT_NODE(node)
			(node->v)(const_cast<A1&>(a1), const_cast<A2&>(a2), const_cast<A3&>(a3), const_cast<A4&>(a4), const_cast<A5&>(a5));
	}
#endif

#undef FOR_EACH_SLOT_NODE
};

#endif /* SIGNAL_H_ */
