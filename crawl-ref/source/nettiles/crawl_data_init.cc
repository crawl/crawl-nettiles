/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#include "AppHdr.h"
#include "branch.h"
#include "output.h"
#include "skills2.h"
#include "colour.h"
#include "feature.h"
#include "env.h"
#include "view.h"
#include "mon-util.h"
#include "items.h"
#include "mgen_data.h"
#include "mon-place.h"
#include "coord.h"
#include "coordit.h"
#include "tiledef-main.h"
#include "tiledef-dngn.h"
#include "tiledef-player.h"
#include "libutil.h"
#include "transform.h"
#include "options.h"
#include "mon-stuff.h"

#include "crawl_data.h"
#include "crawl_parser.h"
#include "crawl_defs.h"

#include <sstream>
#include <iostream>
#ifdef _MSC_VER
#include <unordered_map>
#include <unordered_set>
#include <functional>
#else
#include <tr1/unordered_map>
#include <tr1/unordered_set>
#include <tr1/functional>
#endif
#include "ext_hash.h"
#include <map>
#include <iomanip>
#include <math.h>
#ifndef HUGE_VALF
#define HUGE_VALF ((float)HUGE_VAL)
#endif

using namespace std;
using namespace std::tr1;

struct element_color {
	unsigned char etc;
	unsigned char prob;
	unsigned char color;
};

extern "C" const unsigned dchar_table[ NUM_CSET ][ NUM_DCHAR_TYPES ];
extern mon_display monster_symbols[NUM_MONSTERS];

typedef std::map<show_type, feature_def> feat_map;
extern feat_map Features;

static const char *dchar_names[] =
{
    "wall", "wall_magic", "floor", "floor_magic", "door_open",
    "door_closed", "trap", "stairs_down", "stairs_up", "altar", "arch",
    "fountain", "wavy", "statue", "invis_exposed", "item_detected",
    "item_orb", "item_weapon", "item_armour", "item_wand", "item_food",
    "item_scroll", "item_ring", "item_potion", "item_missile", "item_book",
    "item_stave", "item_miscellany", "item_corpse", "item_gold",
    "item_amulet", "cloud", "trees",
    "space", "fi_flask", "fi_bolt", "fi_chunk", "fi_book", "fi_weapon", "fi_zap", "fi_burst", "fi_stick", "fi_trinket", "fi_scroll", "fi_debug", "fi_armour", "fi_missile", "explosion"
};

static const element_color element_colors[] = {
	{ETC_FIRE, 40, RED},
	{ETC_FIRE, 40, YELLOW},
	{ETC_FIRE, 40, LIGHTRED},
	{ETC_ICE, 40, LIGHTBLUE},
	{ETC_ICE, 40, BLUE},
	{ETC_ICE, 40, WHITE},
	{ETC_EARTH, 70, BROWN},
	{ETC_EARTH, 50, GREEN},
	{ETC_AIR, 60, LIGHTGREY},
	{ETC_AIR, 60, WHITE},
	{ETC_ELECTRICITY, 40, LIGHTCYAN},
	{ETC_ELECTRICITY, 40, LIGHTBLUE},
	{ETC_ELECTRICITY, 40, CYAN},
	{ETC_POISON, 60, LIGHTGREEN},
	{ETC_POISON, 60, GREEN},
	{ETC_WATER, 60, BLUE},
	{ETC_WATER, 60, CYAN},
	{ETC_MAGIC, 30, LIGHTMAGENTA},
	{ETC_MAGIC, 30, LIGHTBLUE},
	{ETC_MAGIC, 30, MAGENTA},
	{ETC_MAGIC, 30, BLUE},
	{ETC_MUTAGENIC, 60, LIGHTMAGENTA},
	{ETC_MUTAGENIC, 60, MAGENTA},
	{ETC_WARP, 60, LIGHTMAGENTA},
	{ETC_WARP, 60, MAGENTA},
	{ETC_ENCHANT, 60, LIGHTBLUE},
	{ETC_ENCHANT, 60, BLUE},
	{ETC_HEAL, 60, LIGHTBLUE},
	{ETC_HEAL, 60, YELLOW},
	{ETC_BLOOD, 60, RED},
	{ETC_BLOOD, 60, DARKGREY},
	{ETC_DEATH, 80, DARKGREY},
	{ETC_DEATH, 40, MAGENTA},
	{ETC_NECRO, 80, DARKGREY},
	{ETC_NECRO, 40, MAGENTA},
	{ETC_UNHOLY, 80, DARKGREY},
	{ETC_UNHOLY, 40, RED},
	{ETC_DARK, 80, DARKGREY},
	{ETC_DARK, 40, LIGHTGREY},
	{ETC_HOLY, 60, YELLOW},
	{ETC_HOLY, 60, WHITE},
	{ETC_VEHUMET, 40, LIGHTRED},
	{ETC_VEHUMET, 40, LIGHTMAGENTA},
	{ETC_VEHUMET, 40, LIGHTBLUE},
	{ETC_BEOGH, 60, LIGHTRED},
	{ETC_BEOGH, 60, BROWN},
	{ETC_CRYSTAL, 40, LIGHTGREY},
	{ETC_CRYSTAL, 40, GREEN},
	{ETC_CRYSTAL, 40, LIGHTRED},
	{ETC_SLIME, 40, GREEN},
	{ETC_SLIME, 40, BROWN},
	{ETC_SLIME, 40, LIGHTGREEN},
	{ETC_SMOKE, 30, LIGHTGREY},
	{ETC_SMOKE, 30, DARKGREY},
	{ETC_SMOKE, 30, LIGHTBLUE},
	{ETC_SMOKE, 30, MAGENTA},
	{ETC_JEWEL, 12, WHITE},
	{ETC_JEWEL, 12, YELLOW},
	{ETC_JEWEL, 12, LIGHTMAGENTA},
	{ETC_JEWEL, 12, LIGHTRED},
	{ETC_JEWEL, 12, LIGHTBLUE},
	{ETC_JEWEL, 12, MAGENTA},
	{ETC_JEWEL, 12, RED},
	{ETC_JEWEL, 12, GREEN},
	{ETC_JEWEL, 12, BLUE},
	{ETC_ELVEN, 40, LIGHTGREEN},
	{ETC_ELVEN, 40, GREEN},
	{ETC_ELVEN, 20, LIGHTBLUE},
	{ETC_ELVEN, 20, BLUE},
	{ETC_DWARVEN, 40, BROWN},
	{ETC_DWARVEN, 40, LIGHTRED},
	{ETC_DWARVEN, 20, LIGHTGREY},
	{ETC_DWARVEN, 20, CYAN},
	{ETC_ORCISH, 40, DARKGREY},
	{ETC_ORCISH, 40, RED},
	{ETC_ORCISH, 20, BROWN},
	{ETC_ORCISH, 20, MAGENTA},
	{ETC_GILA, 30, LIGHTMAGENTA},
	{ETC_GILA, 30, MAGENTA},
	{ETC_GILA, 30, YELLOW},
	{ETC_GILA, 15, LIGHTRED},
	{ETC_GILA, 15, RED},
	{ETC_KRAKEN, 15, GREEN},
	{ETC_KRAKEN, 15, LIGHTGREEN},
	{ETC_KRAKEN, 15, LIGHTCYAN},
	{ETC_KRAKEN, 15, LIGHTBLUE},
	{ETC_KRAKEN, 15, RED},
	{ETC_KRAKEN, 15, LIGHTRED},
	{ETC_KRAKEN, 15, MAGENTA},
	{ETC_KRAKEN, 15, LIGHTMAGENTA},
	{ETC_MIST, 100, CYAN},
	{ETC_MIST, 20, BLUE},
	{ETC_SHIMMER_BLUE, 90, BLUE},
	{ETC_SHIMMER_BLUE, 20, LIGHTBLUE},
	{ETC_SHIMMER_BLUE, 10, CYAN},
	{ETC_DECAY, 60, BROWN},
	{ETC_DECAY, 60, GREEN},
	{ETC_SILVER, 90, LIGHTGREY},
	{ETC_SILVER, 30, WHITE},
	{ETC_GOLD, 60, YELLOW},
	{ETC_GOLD, 60, BROWN},
	{ETC_IRON, 40, CYAN},
	{ETC_IRON, 40, LIGHTGREY},
	{ETC_IRON, 40, DARKGREY},
	{ETC_BONE, 90, WHITE},
	{ETC_BONE, 30, LIGHTGREY},
	{ETC_RANDOM, 8, 1},
	{ETC_RANDOM, 8, 2},
	{ETC_RANDOM, 8, 3},
	{ETC_RANDOM, 8, 4},
	{ETC_RANDOM, 8, 5},
	{ETC_RANDOM, 8, 6},
	{ETC_RANDOM, 8, 7},
	{ETC_RANDOM, 8, 8},
	{ETC_RANDOM, 8, 9},
	{ETC_RANDOM, 8, 10},
	{ETC_RANDOM, 8, 11},
	{ETC_RANDOM, 8, 12},
	{ETC_RANDOM, 8, 13},
	{ETC_RANDOM, 8, 14},
	{ETC_RANDOM, 8, 15},
	/* not strictly correct, but makes things work */
	{ETC_FLOOR, 8, 1},
	{ETC_FLOOR, 8, 2},
	{ETC_FLOOR, 8, 3},
	{ETC_FLOOR, 8, 4},
	{ETC_FLOOR, 8, 5},
	{ETC_FLOOR, 8, 6},
	{ETC_FLOOR, 8, 7},
	{ETC_FLOOR, 8, 8},
	{ETC_FLOOR, 8, 9},
	{ETC_FLOOR, 8, 10},
	{ETC_FLOOR, 8, 11},
	{ETC_FLOOR, 8, 12},
	{ETC_FLOOR, 8, 13},
	{ETC_FLOOR, 8, 14},
	{ETC_FLOOR, 8, 15},
	{ETC_ROCK, 8, 1},
	{ETC_ROCK, 8, 2},
	{ETC_ROCK, 8, 3},
	{ETC_ROCK, 8, 4},
	{ETC_ROCK, 8, 5},
	{ETC_ROCK, 8, 6},
	{ETC_ROCK, 8, 7},
	{ETC_ROCK, 8, 8},
	{ETC_ROCK, 8, 9},
	{ETC_ROCK, 8, 10},
	{ETC_ROCK, 8, 11},
	{ETC_ROCK, 8, 12},
	{ETC_ROCK, 8, 13},
	{ETC_ROCK, 8, 14},
	{ETC_ROCK, 8, 15},
	{ETC_STONE, 8, 1},
	{ETC_STONE, 8, 2},
	{ETC_STONE, 8, 3},
	{ETC_STONE, 8, 4},
	{ETC_STONE, 8, 5},
	{ETC_STONE, 8, 6},
	{ETC_STONE, 8, 7},
	{ETC_STONE, 8, 8},
	{ETC_STONE, 8, 9},
	{ETC_STONE, 8, 10},
	{ETC_STONE, 8, 11},
	{ETC_STONE, 8, 12},
	{ETC_STONE, 8, 13},
	{ETC_STONE, 8, 14},
	{ETC_STONE, 8, 15},
};

struct dchar_fg_tile
{
	unsigned char dc;
	unsigned char fg;
	uint16_t tile;
};


static const struct dchar_fg_tile dchar_fg_tile_table[] =
{
	{DCHAR_ITEM_WEAPON, BLUE, TILE_WPN_BOW},
	{DCHAR_ITEM_WEAPON, LIGHTBLUE, TILE_WPN_CROSSBOW},
	{DCHAR_ITEM_WEAPON, WHITE, TILE_WPN_BLOWGUN},
	{DCHAR_ITEM_WEAPON, BROWN, TILE_WPN_SLING},
	{DCHAR_ITEM_WEAPON, CYAN, TILE_WPN_DAGGER},
	{DCHAR_ITEM_WEAPON, LIGHTCYAN, TILE_WPN_LONG_SWORD},
	{DCHAR_ITEM_WEAPON, DARKGREY, TILE_WPN_HAND_AXE},
	{DCHAR_ITEM_WEAPON, LIGHTGREY, TILE_WPN_CLUB},
	{DCHAR_ITEM_WEAPON, RED, TILE_WPN_SPEAR},
	{DCHAR_ITEM_WEAPON, GREEN, TILE_WPN_QUARTERSTAFF},

	{DCHAR_ITEM_MISSILE, BROWN, TILE_MI_STONE},
	{DCHAR_ITEM_MISSILE, BLUE, TILE_MI_ARROW},
	{DCHAR_ITEM_MISSILE, WHITE, TILE_MI_NEEDLE},
	{DCHAR_ITEM_MISSILE, LIGHTBLUE, TILE_MI_BOLT},
	{DCHAR_ITEM_MISSILE, CYAN, TILE_MI_DART},
	{DCHAR_ITEM_MISSILE, RED, TILE_MI_JAVELIN},
	{DCHAR_ITEM_MISSILE, DARKGREY, TILE_MI_THROWING_NET},

	{DCHAR_ITEM_ARMOUR, GREEN, TILE_ARM_CENTAUR_BARDING},
	//{DCHAR_ITEM_ARMOUR, GREEN, TILE_ARM_DRAGON_ARMOUR},
	//{DCHAR_ITEM_ARMOUR, GREEN, TILE_ARM_NAGA_BARDING},

	{DCHAR_ITEM_ARMOUR, WHITE, TILE_ARM_CLOAK},
	//{DCHAR_ITEM_ARMOUR, WHITE, TILE_ARM_CRYSTAL_PLATE_MAIL},
	//{DCHAR_ITEM_ARMOUR, WHITE, TILE_ARM_ICE_DRAGON_ARMOUR},

        {DCHAR_ITEM_ARMOUR, BLUE, TILE_ARM_BOOTS},
	//{DCHAR_ITEM_ARMOUR, BLUE, TILE_ARM_STEAM_DRAGON_ARMOUR},

        {DCHAR_ITEM_ARMOUR, LIGHTBLUE, TILE_ARM_GLOVES},
	//{DCHAR_ITEM_ARMOUR, LIGHTBLUE, TILE_ARM_STORM_DRAGON_ARMOUR},

        {DCHAR_ITEM_ARMOUR, BROWN, TILE_ARM_LEATHER_ARMOUR},
        //{DCHAR_ITEM_ARMOUR, BROWN, TILE_ARM_SWAMP_DRAGON_ARMOUR},
	//{DCHAR_ITEM_ARMOUR, BROWN, TILE_ARM_TROLL_LEATHER_ARMOUR},

	{DCHAR_ITEM_ARMOUR, MAGENTA, TILE_THELM_CAP},

	{DCHAR_ITEM_ARMOUR, DARKGREY, TILE_THELM_HELM},
	// several robes as DARKGREY too...

	{DCHAR_ITEM_ARMOUR, RED, TILE_ARM_ROBE},

	{DCHAR_ITEM_ARMOUR, LIGHTGREY, TILE_ARM_ANIMAL_SKIN},

	{DCHAR_ITEM_ARMOUR, CYAN, TILE_ARM_SHIELD},

	/* randarts with "exciting" colors */
        {DCHAR_ITEM_ARMOUR, YELLOW, TILE_ARM_GOLD_DRAGON_ARMOUR},
        //{DCHAR_ITEM_ARMOUR, YELLOW, TILE_ARM_GOLD_DRAGON_ARMOUR},

        {DCHAR_ITEM_ARMOUR, LIGHTGREEN, TILE_ARM_GOLD_DRAGON_ARMOUR},

        {DCHAR_ITEM_ARMOUR, LIGHTRED, TILE_ARM_GOLD_DRAGON_ARMOUR},

        {DCHAR_ITEM_ARMOUR, LIGHTMAGENTA, TILE_ARM_GOLD_DRAGON_ARMOUR},
        //{DCHAR_ITEM_ARMOUR, LIGHTMAGENTA, TILE_ARM_MOTTLED_DRAGON_ARMOUR},

	{DCHAR_ITEM_WAND, CYAN, TILE_WAND_OFFSET + 0},
	{DCHAR_ITEM_WAND, YELLOW, TILE_WAND_OFFSET + 5},
	{DCHAR_ITEM_WAND, LIGHTGREY, TILE_WAND_OFFSET + 8},
	{DCHAR_ITEM_WAND, BROWN, TILE_WAND_OFFSET + 3},
	{DCHAR_ITEM_WAND, WHITE, TILE_WAND_OFFSET + 6},

	{DCHAR_ITEM_RING, BROWN, TILE_RING_NORMAL_OFFSET},
	{DCHAR_ITEM_RING, LIGHTGREY, TILE_RING_NORMAL_OFFSET + 1},
	{DCHAR_ITEM_RING, YELLOW, TILE_RING_NORMAL_OFFSET + 2},
	{DCHAR_ITEM_RING, CYAN, TILE_RING_NORMAL_OFFSET + 3},
	{DCHAR_ITEM_RING, WHITE, TILE_RING_NORMAL_OFFSET + 9},
	{DCHAR_ITEM_RING, GREEN, TILE_RING_NORMAL_OFFSET + 12},
	{DCHAR_ITEM_RING, LIGHTCYAN, TILE_RING_NORMAL_OFFSET + 13},

	{DCHAR_ITEM_AMULET, WHITE, TILE_AMU_NORMAL_OFFSET + 0},
	{DCHAR_ITEM_AMULET, LIGHTBLUE, TILE_AMU_NORMAL_OFFSET + 1},
	{DCHAR_ITEM_AMULET, YELLOW, TILE_AMU_NORMAL_OFFSET + 2},
	{DCHAR_ITEM_AMULET, GREEN, TILE_AMU_NORMAL_OFFSET + 3},
	{DCHAR_ITEM_AMULET, RED, TILE_AMU_NORMAL_OFFSET + 4},
	{DCHAR_ITEM_AMULET, BROWN, TILE_AMU_NORMAL_OFFSET + 5},
	{DCHAR_ITEM_AMULET, LIGHTGREY, TILE_AMU_NORMAL_OFFSET + 10},
	{DCHAR_ITEM_AMULET, GREEN, TILE_AMU_NORMAL_OFFSET + 12},

	{DCHAR_ITEM_POTION, LIGHTGREY, TILE_POTION_OFFSET + 0},
	{DCHAR_ITEM_POTION, BLUE, TILE_POTION_OFFSET + 1},
	{DCHAR_ITEM_POTION, DARKGREY, TILE_POTION_OFFSET + 2},
	{DCHAR_ITEM_POTION, WHITE, TILE_POTION_OFFSET + 3},
	{DCHAR_ITEM_POTION, CYAN, TILE_POTION_OFFSET + 4},
	{DCHAR_ITEM_POTION, MAGENTA, TILE_POTION_OFFSET + 5},
	{DCHAR_ITEM_POTION, LIGHTRED, TILE_POTION_OFFSET + 6},
	{DCHAR_ITEM_POTION, RED, TILE_POTION_OFFSET + 8},
	{DCHAR_ITEM_POTION, YELLOW, TILE_POTION_OFFSET + 9},
	{DCHAR_ITEM_POTION, GREEN, TILE_POTION_OFFSET + 10},
	{DCHAR_ITEM_POTION, BROWN, TILE_POTION_OFFSET + 11},
	{DCHAR_ITEM_POTION, LIGHTMAGENTA, TILE_POTION_OFFSET + 12},

	{DCHAR_ITEM_BOOK, BROWN, TILE_BOOK_LEATHER_OFFSET},
	{DCHAR_ITEM_BOOK, DARKGREY, TILE_BOOK_LEATHER_OFFSET_1},
	{DCHAR_ITEM_BOOK, CYAN, TILE_BOOK_METAL_OFFSET},
	{DCHAR_ITEM_BOOK, LIGHTGREY, TILE_BOOK_PAPYRUS},

//	{DCHAR_ITEM_MISCELLANY, BROWN, TILE_MISC_STONE_OF_EARTH_ELEMENTALS},
//	{DCHAR_ITEM_MISCELLANY, YELLOW, TILE_MISC_LAMP_OF_FIRE},
//	{DCHAR_ITEM_MISCELLANY, DARKGREY, TILE_MISC_BOX_OF_BEASTS},

//	{DCHAR_ITEM_GOLD, YELLOW, TILE_GOLD},

/* these conflict with chunks, and it's bad to kill a monster and see it turn into a choko or a pear...
 * so, they aren't actually used ('%' is mapped to DCHAR_ITEM_CORPSE)
 */
	{DCHAR_ITEM_FOOD, BROWN, TILE_FOOD_BREAD_RATION},
	{DCHAR_ITEM_FOOD, YELLOW, TILE_FOOD_ROYAL_JELLY},
	{DCHAR_ITEM_FOOD, LIGHTGREEN, TILE_FOOD_PEAR},
	{DCHAR_ITEM_FOOD, GREEN, TILE_FOOD_CHOKO},
	{DCHAR_ITEM_FOOD, RED, TILE_FOOD_STRAWBERRY},
	{DCHAR_ITEM_FOOD, MAGENTA, TILE_FOOD_GRAPE},

	{DCHAR_ITEM_CORPSE, LIGHTGREY, TILE_FOOD_BONE},

//	{DCHAR_ITEM_ORB, MAGENTA, TILE_ORB},
};

static const struct dchar_fg_tile dchar_num_tile_defaults_table[] =
{
	{DCHAR_ITEM_FOOD, 1, TILE_FOOD_HONEYCOMB},
	{DCHAR_ITEM_CORPSE, 1, TILE_FOOD_CHUNK},
	{DCHAR_ITEM_WEAPON, 1, TILE_UNSEEN_WEAPON},
	{DCHAR_ITEM_MISSILE, 1, TILE_UNSEEN_WEAPON},
	{DCHAR_ITEM_ARMOUR, 1, TILE_UNSEEN_ARMOUR},
	{DCHAR_ITEM_WAND, 12, TILE_WAND_OFFSET},
	{DCHAR_ITEM_SCROLL, 1, TILE_SCROLL},
	{DCHAR_ITEM_POTION, 14, TILE_POTION_OFFSET},
	{DCHAR_ITEM_RING, 14, TILE_RING_NORMAL_OFFSET},
	{DCHAR_ITEM_AMULET, 14, TILE_AMU_NORMAL_OFFSET},
	{DCHAR_ITEM_BOOK, 16, TILE_BOOK_PAPER_OFFSET},
	{DCHAR_ITEM_MISCELLANY, 1, TILE_UNSEEN_ITEM},
	{DCHAR_ITEM_STAVE, 10, TILE_STAFF_OFFSET},
	{DCHAR_ITEM_GOLD, 1, TILE_GOLD},
	{DCHAR_ITEM_ORB, 1, TILE_ORB},
};

struct species_info
{
	species_type type;
	const char* name;
};

static species_info species[] =
{
	{SP_BASE_DRACONIAN, "Draconian"},
	{SP_RED_DRACONIAN, "Red Draconian"},
	{SP_WHITE_DRACONIAN, "White Draconian"},
	{SP_GREEN_DRACONIAN, "Green Draconian"},
	{SP_YELLOW_DRACONIAN, "Yellow Draconian"},
	{SP_GREY_DRACONIAN, "Grey Draconian"},
	{SP_BLACK_DRACONIAN, "Black Draconian"},
	{SP_PURPLE_DRACONIAN, "Purple Draconian"},
	{SP_MOTTLED_DRACONIAN, "Mottled Draconian"},
	{SP_PALE_DRACONIAN, "Pale Draconian"},
	{SP_ELF, "Elf"},
	{SP_HIGH_ELF, "High Elf"},
	{SP_DEEP_ELF, "Deep Elf"},
	{SP_SLUDGE_ELF, "Sludge Elf"},
	{SP_GREY_ELF, "Grey Elf"},
	{SP_MOUNTAIN_DWARF, "Dwarf"},
	{SP_MOUNTAIN_DWARF, "Mountain Dwarf"},
	{SP_DEEP_DWARF, "Deep Dwarf"},
	{SP_HILL_DWARF, "Hill Dwarf"},
	{SP_HUMAN, "Human"},
	{SP_HALFLING, "Halfling"},
	{SP_KOBOLD, "Kobold"},
	{SP_MUMMY, "Mummy"},
	{SP_NAGA, "Naga"},
	{SP_CENTAUR, "Centaur"},
	{SP_SPRIGGAN, "Spriggan"},
	{SP_MINOTAUR, "Minotaur"},
	{SP_KENKU, "Kenku"},
	{SP_HILL_ORC, "Hill Orc"},
	{SP_OGRE, "Ogre"},
	{SP_OGRE_MAGE, "Ogre Mage"},
	{SP_TROLL, "Troll"},
	{SP_DEMIGOD, "Demigod"},
	{SP_DEMONSPAWN, "Demonspawn"},
	{SP_GHOUL, "Ghoul"},
	{SP_MERFOLK, "Merfolk"},
	{SP_VAMPIRE, "Vampire"},
};

struct god_info
{
	god_type type;
	const char* name;
};

static god_info gods[] =
{
	{GOD_NO_GOD, ""},
	{GOD_ZIN, "Zin"},
	{GOD_SHINING_ONE, "The Shining One"},
	{GOD_KIKUBAAQUDGHA, "Kikubaaqudgha"},
	{GOD_YREDELEMNUL, "Yredelemnul"},
	{GOD_VEHUMET, "Vehumet"},
	{GOD_OKAWARU, "Okawaru"},
	{GOD_MAKHLEB, "Makhleb"},
	{GOD_SIF_MUNA, "Sif Muna"},
	{GOD_TROG, "Trog"},
	{GOD_NEMELEX_XOBEH, "Nemelex Xobeh"},
	{GOD_ELYVILON, "Elyvilon"},
	{GOD_LUGONU, "Lugonu"},
	{GOD_BEOGH, "Beogh"},
	{GOD_FEDHAS, "Fedhas"},
	{GOD_CHEIBRIADOS, "Cheibriados"},
	{GOD_JIYVA, "Jiyva"},
	{GOD_XOM, "Xom"},
};

/* we get these for free from the putty copy we carry */
struct charset_spec {
    int charset;
    void (*read)(...);
    void (*write)(...);
    unsigned long* data;
};

extern "C" const charset_spec charset_CS_ISO8859_1;
extern "C" const charset_spec charset_CS_CP437;

struct CrawlDataInit : public CrawlData
{
	void add_dchar_fg_to_feature(unsigned dchar, unsigned color, dungeon_feature_type feat, unsigned flags, bool ignore_ambiguous = false)
	{
		if(dchar == NUM_DCHAR_TYPES)
			return;

		/* there is a #if TILES that changes this, change it back... */
		if(feat == DNGN_OPEN_SEA)
			dchar = DCHAR_WALL;

		if(dchar_to_color_to_feature[dchar].count(color)) {
//			if(!ignore_ambiguous)
//				cerr << "Ambiguous dchar/fg: " << dchar_names[dchar] << ' ' << colour_to_str(color) << endl;
		} else
			dchar_to_color_to_feature[dchar][color] = make_pair(feat, flags);
		//cout << hex << dchar << dec << ' ' << color << ": " << (unsigned)feat<< endl;
	}

	void add_char_fg_to_monster_tile(wchar_t c, unsigned color, unsigned monster, unsigned tile)
	{
		if(invalid_monster_type((monster_type)monster)
				|| monster == MONS_RAKSHASA_FAKE || monster == MONS_MARA_FAKE)
			return;

		if(is_element_colour(color)) {
			for(unsigned j = 0; j < element_to_color[color].size(); ++j)
				add_char_fg_to_monster(c, element_to_color[color][j], monster, tile);
			return;
		}

		adjust_monster_tile(tile, (monster_type)monster);

		char_fg_to_monsters.insert(make_pair(make_pair((char)c, color), make_pair((monster_type)monster, tile)));
//		cout << (char)c << ' ' << color << ": " << get_monster_data(monster)->name << endl;
	}

	void add_char_fg_to_monster(wchar_t c, unsigned color, unsigned monster, unsigned flags = 0)
	{
		add_char_fg_to_monster_tile(c, color, monster, tileidx_monster_type((monster_type)monster, (monster_type)0, color, 0) | flags);
	}

	void add_name_to_monster(string name, monster_type type, unsigned data = 0)
	{
		assert(!name_to_monster.count(name));
		name_to_monster[name] = make_pair(type, data);
		name_to_monster[pluralise(name)] = make_pair(type, data);
	}

	void add_name_to_monster_tile(string name, monster_type type, unsigned tile)
	{
		assert(!name_to_monster_tile.count(name));
		name_to_monster_tile[name] = make_pair(type, tile);
		name_to_monster_tile[pluralise(name)] = make_pair(type, tile);
	}

	void init()
	{
		if(inited)
			return;
		inited = true;

		for(unsigned cs = CSET_ASCII + 1; cs < NUM_CSET; ++cs) {
			for(unsigned dc = 0; dc < NUM_DCHAR_TYPES; ++dc) {
				wchar_t wc = dchar_table[cs][dc];
				if(wc >= 0x80)
					unicode_replacements[wc] = dchar_table[CSET_ASCII][dc];
			}
		}

		// the terminal emulator converts these for us, so we must look at the converted data too

		static const unsigned long unitab_xterm_std[32] = {
		    0x2666, 0x2592, 0x2409, 0x240c, 0x240d, 0x240a, 0x00b0, 0x00b1,
		    0x2424, 0x240b, 0x2518, 0x2510, 0x250c, 0x2514, 0x253c, 0x23ba,
		    0x23bb, 0x2500, 0x23bc, 0x23bd, 0x251c, 0x2524, 0x2534, 0x252c,
		    0x2502, 0x2264, 0x2265, 0x03c0, 0x2260, 0x00a3, 0x00b7, 0x0020
		};

		unsigned long dec_table[256];
		memcpy(dec_table, charset_CS_ISO8859_1.data, sizeof(dec_table));

		for(unsigned i = 0; i < 32; ++i)
			dec_table[0x80 + 0x60 + i] = unitab_xterm_std[i];

		unsigned long* wc_table[4];
		wc_table[CSET_ASCII] = 0;
		wc_table[CSET_DEC] = dec_table;
		wc_table[CSET_IBM] = charset_CS_CP437.data;
		wc_table[CSET_UNICODE] = 0;

		for(unsigned cs = 0; cs < NUM_CSET; ++cs) {
			unsigned csets[4];
			csets[0] = cs;
			csets[1] = (cs != CSET_DEC) ? CSET_DEC : CSET_ASCII;
			if(cs == CSET_ASCII || cs == CSET_DEC)
				csets[2] = CSET_IBM;
			else
				csets[2] = CSET_ASCII;
			csets[3] = (cs != CSET_UNICODE) ? CSET_UNICODE : CSET_IBM;

			for(unsigned ucsi = 0; ucsi < 4; ++ucsi) {
				unsigned ucs = csets[ucsi];
				for(unsigned dc = 0; dc < NUM_DCHAR_TYPES; ++dc) {
					unsigned tc = dchar_table[ucs][dc];
					wchar_t wc = wc_table[ucs] ? wc_table[ucs][tc] : tc;
					if(!wchar_to_dchar[cs].count(wc)) {
						// these are ambiguous in all charsets, and it looks better to
						// misidentify food as chunks than the opposite
						unsigned udc = dc;
						if(udc == DCHAR_ITEM_FOOD)
							udc = DCHAR_ITEM_CORPSE;
						wchar_to_dchar[cs][wc] = udc;
					}
				}
			}
		}

		/* 0.6-master difference: determine more precisely elsewhere */
		wchar_to_dchar[CSET_ASCII]['{'] = DCHAR_AMBIGUOUS_CURLY;
		wchar_to_dchar[CSET_ASCII]['~'] = DCHAR_AMBIGUOUS_TILDE;

		/* 0.6 compatibility */
		wchar_to_dchar[CSET_IBM]['~'] = DCHAR_INVIS_EXPOSED;
		wchar_to_dchar[CSET_UNICODE]['~'] = DCHAR_INVIS_EXPOSED;

		for(unsigned i = 0; i < sizeof(log_element_to_color_prob) / sizeof(float); ++i)
			((float*)log_element_to_color_prob)[i] = -HUGE_VALF;

		for(unsigned i = 0; i < 16; ++i)
			log_element_to_color_prob[i][i] = 0.0f;

		for(unsigned i = 0; i < sizeof(element_colors) / sizeof(element_colors[0]); ++i) {
			if(element_colors[i].etc >= element_to_color.size())
				element_to_color.resize(element_colors[i].etc + 1);
			element_to_color[element_colors[i].etc].push_back(element_colors[i].color);

			if(element_colors[i].color >= color_to_element.size())
				color_to_element.resize(element_colors[i].color + 1);
			color_to_element[element_colors[i].color].push_back(element_colors[i].etc);

			log_element_to_color_prob[element_colors[i].etc][element_colors[i].color] = logf(element_colors[i].prob / 120.0f);
		}

		for(feat_map::iterator i = Features.begin(); i != Features.end(); ++i) {
			if(i->first.cls == SH_FEATURE)
				add_dchar_fg_to_feature(i->second.dchar, i->second.colour, i->first.feat, (i->second.seen_colour != i->second.colour) ? TILE_FLAG_SEEN : 0);
		}

		for(feat_map::iterator i = Features.begin(); i != Features.end(); ++i) {
			if(i->first.cls == SH_FEATURE && i->second.seen_colour != i->second.colour)
				add_dchar_fg_to_feature(i->second.dchar, i->second.seen_colour, i->first.feat, TILE_FLAG_UNSEEN, true);
		}

		for(feat_map::iterator i = Features.begin(); i != Features.end(); ++i) {
			if(i->first.cls == SH_FEATURE) {
				for(int j = 0; j < 16; ++j)
					add_dchar_fg_to_feature(i->second.dchar, j, i->first.feat, j == RED ? TILE_FLAG_BLOOD : 0, true);
			}
		}

		add_dchar_fg_to_feature(DCHAR_WAVY, LIGHTGREEN, DNGN_SHALLOW_WATER, true);
		add_dchar_fg_to_feature(DCHAR_WAVY, GREEN, DNGN_DEEP_WATER, true);

		add_dchar_fg_to_feature(DCHAR_STAIRS_DOWN, WHITE, DNGN_STONE_STAIRS_DOWN_I, true);
		add_dchar_fg_to_feature(DCHAR_STAIRS_UP, WHITE, DNGN_STONE_STAIRS_UP_I, true);

		for(unsigned i = 0; i < NUM_MONSTERS; ++i) {
			if(monster_symbols[i].colour != BLACK && monster_symbols[i].colour != ETC_RANDOM)
				add_char_fg_to_monster(monster_symbols[i].glyph, monster_symbols[i].colour, i, TILE_FLAG_SEEN);
		}

		for(unsigned i = 0; i < NUM_MONSTERS; ++i) {
			if(monster_symbols[i].colour == BLACK || monster_symbols[i].colour == ETC_RANDOM) {
				for(unsigned j = 0; j < 16; ++j) {
					if(char_fg_to_monsters.count(make_pair(monster_symbols[i].glyph, j)))
						continue;
					add_char_fg_to_monster(monster_symbols[i].glyph, j, i, j != DARKGRAY ? TILE_FLAG_SEEN : 0);
				}
			}
		}

		for(unsigned j = 0; j < 16; ++j) {
			if(!char_fg_to_monsters.count(make_pair('f', j)))
				add_char_fg_to_monster('f', j, MONS_TOADSTOOL, j != DARKGRAY ? TILE_FLAG_SEEN : 0);
		}

		add_char_fg_to_monster_tile('P', LIGHTGRAY, MONS_PLANT, TILEP_MONS_WITHERED_PLANT);

		for(unsigned c = 0; c < 128; ++c) {
			pair<CrawlData::char_fg_to_monsters_t::iterator, CrawlData::char_fg_to_monsters_t::iterator> range;
			vector<pair<monster_type, unsigned>  > monsters;
			for(unsigned i = 0; i < 16; ++i) {
				range = crawl_data.char_fg_to_monsters.equal_range(make_pair((char)c, (unsigned)i));
				for(CrawlData::char_fg_to_monsters_t::iterator mi = range.first; mi != range.second; ++mi)
					monsters.push_back(mi->second);
			}

			if(monsters.empty()) {
				char_to_base_monster[c] = make_pair(NUM_MONSTERS, 0);
				continue;
			}

			set<monster_type> detected_monsters;
			unsigned flags = TILE_FLAG_SEEN | TILE_FLAG_UNSEEN;
			for(unsigned i = 0; i < monsters.size(); ++i) {
				detected_monsters.insert(mons_detected_base(monsters[i].first));
				if(!(monsters[i].first & TILE_FLAG_SEEN))
					flags &= ~TILE_FLAG_SEEN;
				if(!(monsters[i].first & TILE_FLAG_UNSEEN))
					flags &= ~TILE_FLAG_UNSEEN;
			}

			if(detected_monsters.size() > 1)
				cerr << "More than one detected monster for " << (char)c << endl;

			unsigned tile = tileidx_monster_type(*detected_monsters.begin(), (monster_type)0, 0, 0, true) | flags;

			char_to_base_monster[c] = make_pair(*detected_monsters.begin(), tile);
		}

		for(unsigned i = 0; i < sizeof(dchar_fg_tile_table) / sizeof(dchar_fg_tile_table[0]); ++i)
			dchar_fg_to_tile[make_pair(dchar_fg_tile_table[i].dc, dchar_fg_tile_table[i].fg)] = dchar_fg_tile_table[i].tile;

		for(unsigned i = 0; i < sizeof(dchar_num_tile_defaults_table) / sizeof(dchar_num_tile_defaults_table[0]); ++i) {
			unsigned char dc = dchar_num_tile_defaults_table[i].dc;
			unsigned num = dchar_num_tile_defaults_table[i].fg;
			unsigned tile = dchar_num_tile_defaults_table[i].tile;
			unsigned used_mask = 0;
			for(unsigned j = 0; j < 16; ++j) {
				dchar_fg_to_tile_t::iterator it = dchar_fg_to_tile.find(make_pair(dc, j));
				if(it != dchar_fg_to_tile.end()) {
					unsigned idx = it->second - tile;
					if(idx < num)
						used_mask |= 1 << idx;
				}
			}

			unsigned idx = 0;
			for(unsigned j = 0; j < 16; ++j) {
				dchar_fg_to_tile_t::iterator it = dchar_fg_to_tile.find(make_pair(dc, j));
				if(it == dchar_fg_to_tile.end()) {
					while(used_mask & (1 << idx)) {
						idx = (idx + 1);
						if(idx == num)
							idx = 0;
					}
					dchar_fg_to_tile[make_pair(dc, j)] = tile + idx;
				}
			}
		}

		vector<pair<string, pair<monster_type, unsigned> > > name_to_monster_vec;

		for(monster_type i = (monster_type)0; i < NUM_MONSTERS; i = (monster_type)((int)i + 1))
		{
			if(invalid_monster_type(i)
					|| i == MONS_RAKSHASA_FAKE || i == MONS_MARA_FAKE
					|| i == MONS_ARMOUR_MIMIC || i == MONS_SCROLL_MIMIC || i == MONS_POTION_MIMIC
			)
				continue;

			monsterentry *me = get_monster_data(i);

			if(me)
				add_name_to_monster(me->name, i);
		}

		add_name_to_monster("freed slave", MONS_SLAVE, 0);

		add_name_to_monster("Blork", MONS_BLORK_THE_ORC, 0);

		add_name_to_monster("red ugly thing", MONS_UGLY_THING, 0);
		add_name_to_monster("brown ugly thing", MONS_UGLY_THING, 1);
		add_name_to_monster("green ugly thing", MONS_UGLY_THING, 2);
		add_name_to_monster("cyan ugly thing", MONS_UGLY_THING, 3);
		add_name_to_monster("purple ugly thing", MONS_UGLY_THING, 4);
		add_name_to_monster("white ugly thing", MONS_UGLY_THING, 5);

		add_name_to_monster("red very ugly thing", MONS_VERY_UGLY_THING, 0);
		add_name_to_monster("brown very ugly thing", MONS_VERY_UGLY_THING, 1);
		add_name_to_monster("green very ugly thing", MONS_VERY_UGLY_THING, 2);
		add_name_to_monster("cyan very ugly thing", MONS_VERY_UGLY_THING, 3);
		add_name_to_monster("purple very ugly thing", MONS_VERY_UGLY_THING, 4);
		add_name_to_monster("white very ugly thing", MONS_VERY_UGLY_THING, 5);

		add_name_to_monster("large slime creature", MONS_SLIME_CREATURE, 1);
		add_name_to_monster("very large slime creature", MONS_SLIME_CREATURE, 2);
		add_name_to_monster("enormous slime creature", MONS_SLIME_CREATURE, 3);
		add_name_to_monster("titanic slime creature", MONS_SLIME_CREATURE, 4);

		add_name_to_monster("active ballistomycete", MONS_BALLISTOMYCETE, 1);

		/* Tukima's Studio */
		add_name_to_monster_tile("strange machine", MONS_ICE_STATUE, TILEP_MONS_STATUE_GUARDIAN);

		/* Eringya's Formal Garden */
		add_name_to_monster_tile("vine covered stone golem", MONS_STONE_GOLEM, TILEP_MONS_VINE_GOLEM);

		/* Cigotuvi's Fleshworks */
		string cigotuvi_adjectives[7] ={"sickly", "monstrous", "deformed", "twisted", "grotesque", "hideous", "febrile"};
		for(unsigned i = 0; i < 7; ++i)
			add_name_to_monster_tile(cigotuvi_adjectives[i] + " human", MONS_HUMAN, TILEP_MONS_DEFORMED_HUMAN);
		for(unsigned i = 0; i < 7; ++i)
			add_name_to_monster_tile(cigotuvi_adjectives[i] + " elf", MONS_ELF, TILEP_MONS_DEFORMED_ELF);
		for(unsigned i = 0; i < 7; ++i)
			add_name_to_monster_tile(cigotuvi_adjectives[i] + " orc", MONS_ORC, TILEP_MONS_DEFORMED_ORC);
		add_name_to_monster_tile("flesh golem", MONS_CLAY_GOLEM, TILEP_MONS_FLESH_GOLEM);
		add_name_to_monster_tile("Cigotuvi's Monster", MONS_TENTACLED_MONSTROSITY, TILEP_MONS_CIGOTUVIS_MONSTER);

		/* Iskenderun's Mystic Tower */
		add_name_to_monster_tile("statue", MONS_STATUE, TILEP_MONS_STATUE_MAGE);

		/* Zonguldrok's Mausoleum */
		add_name_to_monster_tile("antique lich", MONS_ANCIENT_LICH, TILEP_MONS_ZONGULDROK_LICH);

		/* Wucad Mu's Monastery */
		add_name_to_monster("Statue of Wucad Mu", MONS_ORANGE_STATUE);
		add_name_to_monster_tile("human monk", MONS_HUMAN, TILEP_MONS_HUMAN_MONK);
		add_name_to_monster_tile("rock troll monk", MONS_ROCK_TROLL, TILEP_MONS_ROCK_TROLL_MONK);
		add_name_to_monster_tile("iron troll monk", MONS_IRON_TROLL, TILEP_MONS_IRON_TROLL_MONK);

		/* Cloud Mage's Chambers */
		add_name_to_monster(/* Foo */ "the Cloud Mage", MONS_WIZARD);

		/* Hellbinder's Lair */
		add_name_to_monster(/* Foo */ "the Hellbinder", MONS_WIZARD);

		/* sewer */
		add_name_to_monster("malarious mermaid", MONS_MERMAID);
		add_name_to_monster("sickly siren", MONS_SIREN);

		/* misc. vaults */
		add_name_to_monster_tile("archer statue", MONS_STATUE, TILEP_MONS_STATUE_CROSSBOW);
		add_name_to_monster_tile("warrior statue", MONS_STATUE, TILEP_MONS_STATUE_MACE);
		add_name_to_monster_tile("wizard statue", MONS_STATUE, TILEP_MONS_STATUE_MAGE);
		add_name_to_monster("deep elf elementalist", MONS_DEEP_ELF_SORCERER);
		add_name_to_monster("master elementalist", MONS_WIZARD);
		add_name_to_monster("ancient champion", MONS_SKELETAL_WARRIOR);
		add_name_to_monster("altered rat", MONS_RAT);
		add_name_to_monster("charred statue", MONS_STATUE);
		add_name_to_monster("frost-covered statue", MONS_STATUE);
		add_name_to_monster_tile("withered plant", MONS_PLANT, TILEP_MONS_WITHERED_PLANT);
		add_name_to_monster("demonic plant", MONS_PLANT);
		add_name_to_monster("strawberry plant", MONS_PLANT);
		add_name_to_monster_tile("black sheep", MONS_SHEEP, TILEP_MONS_BLACK_SHEEP);

		/* 0.6 compatibility */
		add_char_fg_to_monster('3', GREEN, MONS_DEMONIC_CRAWLER);
		add_char_fg_to_monster('5', LIGHTRED, MONS_IRON_IMP);
		add_name_to_monster("manes", MONS_IRON_IMP); /* monster is quite different though */
		add_name_to_monster("skeletal dragon", MONS_BONE_DRAGON);

		/* 0.5 compatibility */
		add_name_to_monster("hell-hog", MONS_HELL_HOG);
		add_name_to_monster("giant iguana", MONS_IGUANA);
		add_name_to_monster("firedrake", MONS_FIRE_DRAKE);
		add_name_to_monster("giant brown frog", MONS_GIANT_TOAD);
		add_name_to_monster("guardian naga", MONS_GUARDIAN_SERPENT);
		add_name_to_monster("ogre-mage", MONS_OGRE_MAGE);
		add_name_to_monster("brown snake", MONS_WATER_MOCCASIN);
		add_name_to_monster("black snake", MONS_BLACK_MAMBA);
		add_name_to_monster("yellow snake", MONS_VIPER);
		add_name_to_monster("grey snake", MONS_ANACONDA);
		add_name_to_monster("electrical eel", MONS_ELECTRIC_EEL);

		for(unsigned i = 0; i < sizeof(species) / sizeof(species[0]); ++i)
			name_to_species[species[i].name] = species[i].type;

		for(unsigned i = 0; i < sizeof(gods) / sizeof(gods[0]); ++i)
			name_to_god[gods[i].name] = gods[i].type;
	}
};

void CrawlData::init()
{
	((CrawlDataInit*)this)->init();
}

CrawlData crawl_data;
