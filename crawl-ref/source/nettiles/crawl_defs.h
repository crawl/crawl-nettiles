/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#ifndef CRAWL_DEFS_H_
#define CRAWL_DEFS_H_

#define TILE_FLAG_SEEN     0x80000000
#define TILE_FLAG_NO_HALO 0x40000000
#define TILE_FLAG_NOT_SILENCED 0x20000000

#define DCHAR_AMBIGUOUS_TILDE (NUM_DCHAR_TYPES + 1)
#define DCHAR_AMBIGUOUS_CURLY (NUM_DCHAR_TYPES + 2)

struct CrawlFragment
{
	std::pair<screen_buffer_t, screen_buffer_t>* buf;
	unsigned rows;
	unsigned cols;

	CrawlFragment(unsigned pcols, unsigned prows)
	: rows(prows), cols(pcols)
	{
		buf = new std::pair<screen_buffer_t, screen_buffer_t>[rows * cols];
	}

	~CrawlFragment()
	{
		delete [] buf;
	}

	std::pair<screen_buffer_t, screen_buffer_t>& operator ()(unsigned x, unsigned y)
	{
		return buf[y * cols + x];
	}

	const std::pair<screen_buffer_t, screen_buffer_t>& operator ()(unsigned x, unsigned y) const
	{
		return buf[y * cols + x];
	}
};

#endif /* CRAWL_DEFS_H_ */
