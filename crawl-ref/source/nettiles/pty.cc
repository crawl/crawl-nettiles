/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#include "AppHdr.h"

#ifdef __unix__
#include <stdlib.h>
#include <fcntl.h>
#include <iostream>

#ifdef __APPLE__
#include <util.h>
#else
#include <pty.h>
#endif

#include <sys/wait.h>
#include "connection.h"

using namespace std;

struct Pty : public Connection
{
	pid_t pid;
	int link[2];

	Pty(const char* command, unsigned rows, unsigned cols) {
		int fd = -1;
		char slave[256] = {0};
		winsize wsize;
		wsize.ws_row = rows;
		wsize.ws_col = cols;
		wsize.ws_xpixel = 32767;
		wsize.ws_ypixel = 32767;
		pid = forkpty(&fd, slave, NULL, &wsize);
		if (pid == -1) {
			cerr << "failed to forkpty" << endl;
			exit(1);
		} else if (!pid) {
			setenv("TERM", "xterm", 1);
			unsetenv("LINES");

			unsetenv("COLUMNS");
			execlp("bash", "bash", "-c", command, NULL);
			exit(1);
		}

		fcntl(fd, F_SETFL, 0);

		link[0] = link[1] = fd;
	}

	ptrdiff_t retrieve(void* buf, size_t size)
	{
		return read(link[0], buf, size);
	}

	ptrdiff_t transmit(const void* buf, size_t size)
	{
		return write(link[1], buf, size);
	}

	~Pty()
	{
		close(link[0]);
		close(link[1]);
		int status;
		waitpid(pid, &status, 0);
	}
};

Connection* create_local_connection(const std::string& command, unsigned rows, unsigned cols)
{
	return new Pty(command.c_str(), rows, cols);
}
#endif
