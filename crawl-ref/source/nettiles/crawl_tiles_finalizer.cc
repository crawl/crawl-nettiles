/*
 * Copyright (C) 2010 Luca Barbieri
 * Released under the terms of the Crawl General Public License.
 */

#include "AppHdr.h"
#include "tiledef-dngn.h"
#include "tiledef-player.h"
#include "coord.h"
#include "env.h"

#include "crawl_defs.h"
#include "crawl_tiles_finalizer.h"

#include <algorithm>
#include <iostream>
#ifdef _MSC_VER
#include <unordered_map>
#include <unordered_set>
#else
#include <tr1/unordered_map>
#include <tr1/unordered_set>
#endif

using namespace std;
using namespace std::tr1;

static int _pick_random_dngn_tile(unsigned int idx, int value = -1)
{
    ASSERT(idx >= 0 && idx < TILE_DNGN_MAX);
    const int count = tile_dngn_count(idx);
    if (count == 1)
        return (idx);

    const int total = tile_dngn_probs(idx + count - 1);
    const int rand  = (value == -1 ? random2(total) : value % total);

    for (int i = 0; i < count; ++i)
    {
        int curr = idx + i;
        if (rand < tile_dngn_probs(curr))
            return (curr);
    }

    return (idx);
}

struct CrawlTilesFinalizerImpl : public CrawlTilesFinalizer
{
	struct Instance
	{
		CrawlFragment& frag;
		unsigned stamp;

		vector<unsigned> bg_stamps;
		typedef unordered_map<unsigned, unsigned> bg_counts_t;
		unsigned bg_water_total;
		unsigned bg_floor_total;
		bg_counts_t bg_water_counts;
		bg_counts_t bg_floor_counts;
		typedef unordered_map<unsigned, unsigned> bg_map_t;
		bg_map_t bg_map;
		static const unsigned water = 0x80000000;

		unsigned seen_total;
		unsigned unseen_total;
		vector<unsigned> seen_stamps;

		vector<coord_def> tofill;

		coord_def player_pos;

		void bg_visit(unsigned x, unsigned y)
		{
			if(x >= (unsigned)frag.cols || y >= (unsigned)frag.rows || bg_stamps[y * frag.cols + x] == stamp)
				return;

			bg_stamps[y * frag.cols + x] = stamp;

			pair<screen_buffer_t, screen_buffer_t> p = frag(x, y);
			unsigned bgb = p.second & TILE_FLAG_MASK;
			unsigned fgb = p.first & TILE_FLAG_MASK;

			if(bgb) {
				bg_map_t::iterator i = bg_map.find(bgb);
				if(i != bg_map.end()) {
					if(i->second & water) {
						++bg_water_total;
						++bg_water_counts[bgb];
					} else {
						++bg_floor_total;
						++bg_floor_counts[bgb];
					}
				}
				return;
			} else if(!fgb)
				return;

			tofill.push_back(coord_def(x, y));
			for(int dx = -1; dx <= 1; ++dx)
			{
				for(int dy = -1; dy <= 1; ++dy)
				{
					bg_visit(x + dx, y + dy);
				}
			}
		}

		void bg_assign()
		{
			for(unsigned y = 0; y < frag.rows; ++y)
			{
				for(unsigned x = 0; x < frag.cols; ++x)
				{
					if(bg_stamps[y * frag.cols + x])
						continue;

					pair<screen_buffer_t, screen_buffer_t> p = frag(x,  y);
					if(!(p.first & TILE_FLAG_MASK) || p.second & TILE_FLAG_MASK)
						continue;

					++stamp;
					bg_water_total = 0;
					bg_floor_total = 0;
					bg_visit(x, y);

					unsigned best_count = 0;
					unsigned best_bg = env.tile_default.floor;

					if(bg_floor_total >= bg_water_total) {
						for(bg_counts_t::iterator i = bg_floor_counts.begin(); i != bg_floor_counts.end(); ++i) {
							if(i->second > best_count) {
								best_count = i->second;
								best_bg = i->first;
							}
						}
					}
					if(bg_water_total >= bg_floor_total) {
						for(bg_counts_t::iterator i = bg_water_counts.begin(); i != bg_water_counts.end(); ++i) {
							if(i->second > best_count) {
								best_count = i->second;
								best_bg = i->first;
							}
						}
					}

					bg_floor_counts.clear();
					bg_water_counts.clear();

					for(unsigned i = 0; i < tofill.size(); ++i)
						frag(tofill[i].x, tofill[i].y).second |= best_bg;
					tofill.clear();
				}
			}
		}

		void seen_visit(unsigned x, unsigned y)
		{
			if(x >= (unsigned)frag.cols || y >= (unsigned)frag.rows || seen_stamps[y * frag.cols + x] == stamp)
				return;

			seen_stamps[y * frag.cols + x] = stamp;

			pair<screen_buffer_t, screen_buffer_t> p = frag(x, y);
			unsigned bgb = p.second & TILE_FLAG_MASK;
			unsigned fgb = p.first & TILE_FLAG_MASK;

			if(p.second & (TILE_FLAG_SEEN | TILE_FLAG_UNSEEN)) {
				if((p.second & (TILE_FLAG_SEEN | TILE_FLAG_UNSEEN)) == (TILE_FLAG_SEEN | TILE_FLAG_UNSEEN))
					cerr << "Ambiguous visibility for fg " << fgb << " and bg " << bgb << endl;
				if(p.second & TILE_FLAG_UNSEEN)
					++unseen_total;
				else
					++seen_total;
			} else if(bgb || fgb) {
				tofill.push_back(coord_def(x, y));
				for(int dx = -1; dx <= 1; ++dx)
				{
					for(int dy = -1; dy <= 1; ++dy)
					{
						seen_visit(x + dx, y + dy);
					}
				}
			}
		}

		void seen_assign()
		{
			if(player_pos.x >= 0) {
				for(unsigned y = 0; y < frag.rows; ++y)
				{
					for(unsigned x = 0; x < frag.cols; ++x)
					{
						if(grid_distance(player_pos.x, player_pos.y, x, y) <= 1)
							frag(x, y).second = (frag(x, y).second & ~TILE_FLAG_UNSEEN) | TILE_FLAG_SEEN;
						else if(distance(player_pos.x, player_pos.y, x, y) > LOS_MAX_RADIUS_SQ)
							frag(x, y).second |= TILE_FLAG_UNSEEN;
					}
				}
			}

			for(unsigned y = 0; y < frag.rows; ++y)
			{
				for(unsigned x = 0; x < frag.cols; ++x)
				{
					if(seen_stamps[y * frag.cols + x])
						continue;

					pair<screen_buffer_t, screen_buffer_t> p = frag(x,  y);
					if(!(p.first & TILE_FLAG_MASK) && !(p.second & TILE_FLAG_MASK))
						continue;

					if(p.second & (TILE_FLAG_SEEN | TILE_FLAG_UNSEEN))
						continue;

					++stamp;
					unseen_total = 0;
					seen_total = 0;
					seen_visit(x, y);

					if(unseen_total > seen_total) {
						for(unsigned i = 0; i < tofill.size(); ++i)
							frag(tofill[i].x, tofill[i].y).second |= TILE_FLAG_UNSEEN;
					}
					tofill.clear();
				}
			}
		}

		void correct_circular_effect(unsigned yes, unsigned no)
		{
			vector<int> yes_radii;
			vector<int> no_radii;

			for(unsigned y = 0; y < frag.rows; ++y)
			{
				for(unsigned x = 0; x < frag.cols; ++x)
				{
					pair<screen_buffer_t, screen_buffer_t> p = frag(x,  y);
					if(p.second & yes)
						yes_radii.push_back(distance(x, y, player_pos.x, player_pos.y));
					if(p.second & no)
						no_radii.push_back(distance(x, y, player_pos.x, player_pos.y));
				}
			}
			yes_radii.push_back(-1);
			no_radii.push_back(INT_MAX);

			sort(yes_radii.begin(), yes_radii.end(), greater<int>());
			sort(no_radii.begin(), no_radii.end());

			/* while there is no separator, drop a point from each set if possible */
			unsigned yesi = 0, noi = 0;
			while(yes_radii[yesi] >= no_radii[noi]) {
				if((yesi + 1) < yes_radii.size())
					++yesi;
				if((noi + 1) < no_radii.size())
					++noi;
			}

			int radius;
			radius = yes_radii[yesi];
			if(radius > LOS_MAX_RADIUS_SQ)
				radius = LOS_MAX_RADIUS_SQ;

			for(unsigned y = 0; y < frag.rows; ++y)
			{
				for(unsigned x = 0; x < frag.cols; ++x)
				{
					pair<screen_buffer_t, screen_buffer_t>& p = frag(x,  y);
					p.second &=~ (yes | no);
					if(distance(x, y, player_pos.x, player_pos.y) <= radius)
						p.second |= yes;
					else
						p.second |= no;
				}
			}
		}

		Instance(CrawlTilesFinalizerImpl& fin, CrawlFragment& pfrag, unsigned* floor_tiles, unsigned* wall_tiles, coord_def pplayer_pos, bool seen_works, bool is_silenced)
		: frag(pfrag), stamp(0), player_pos(pplayer_pos)
		{
			bg_stamps.resize(frag.rows * frag.cols);
			seen_stamps.resize(frag.rows * frag.cols);

			bg_map[TILE_DNGN_SHALLOW_WATER] = water | TILE_DNGN_SHALLOW_WATER;
			bg_map[TILE_DNGN_SHALLOW_WATER_DISTURBANCE] = water | TILE_DNGN_SHALLOW_WATER;
			bg_map[TILE_DNGN_SHALLOW_WATER_MURKY] = water | TILE_DNGN_SHALLOW_WATER_MURKY;
			bg_map[TILE_DNGN_SHALLOW_WATER_MURKY_DISTURBANCE] = water | TILE_DNGN_SHALLOW_WATER_MURKY_DISTURBANCE;
			bg_map[TILE_DNGN_DEEP_WATER] = water | TILE_DNGN_DEEP_WATER;
			bg_map[TILE_DNGN_DEEP_WATER_MURKY] = water | TILE_DNGN_DEEP_WATER_MURKY;
			bg_map[TILE_DNGN_OPEN_SEA] = water | TILE_DNGN_OPEN_SEA;
			bg_map[TILE_SHOALS_DEEP_WATER] = water | TILE_SHOALS_DEEP_WATER;
			bg_map[TILE_SHOALS_SHALLOW_WATER] = water | TILE_SHOALS_SHALLOW_WATER;
			bg_map[TILE_SHOALS_SHALLOW_WATER_DISTURBANCE] = water | TILE_SHOALS_SHALLOW_WATER_DISTURBANCE;

			bg_map[env.tile_default.floor] = env.tile_default.floor;
			for(unsigned i = 0; i < 16; ++i)
				bg_map[floor_tiles[i]] = floor_tiles[i];

			/* TODO: mark trees and doors as seen */

			seen_assign();
			bg_assign();

			if(player_pos.x >= 0) {
				correct_circular_effect(TILE_FLAG_HALO, TILE_FLAG_NO_HALO);
				correct_circular_effect(TILE_FLAG_SILENCED, TILE_FLAG_NOT_SILENCED);

				pair<screen_buffer_t, screen_buffer_t>& p = frag(player_pos.x,  player_pos.y);
				p.second &=~ (TILE_FLAG_SILENCED | TILE_FLAG_NOT_SILENCED);
				if(is_silenced)
					p.second |= TILE_FLAG_SILENCED;
				else
					p.second |= TILE_FLAG_NOT_SILENCED;
			}

			if(fin.random_data.size() < frag.rows)
				fin.random_data.resize(frag.rows);

			for(unsigned y = 0; y < frag.rows; ++y)
			{
				int last_bgb = -1;
				for(unsigned i = fin.random_data[y].size(); i < frag.cols; ++i)
					fin.random_data[y].push_back(random_int());

				for(unsigned x = 0; x < frag.cols; ++x)
				{
					pair<screen_buffer_t, screen_buffer_t>& p = frag(x,  y);
					unsigned fgb = p.first & TILE_FLAG_MASK;
					unsigned fgf = p.first & ~TILE_FLAG_MASK;
					unsigned bgb = p.second & TILE_FLAG_MASK;
					unsigned bgf = p.second & ~TILE_FLAG_MASK;
					unsigned orig_bgb = bgb;

					bgf &=~ (TILE_FLAG_SEEN | TILE_FLAG_NO_HALO | TILE_FLAG_NOT_SILENCED);
					/* if we cannot determine LOS well, make everything "seen" */
					if(!seen_works)
						bgf &=~ TILE_FLAG_UNSEEN;

					if(fgb >= TILE_MAIN_MAX && fgb < TILEP_PLAYER_MAX && coord_def(x, y) != player_pos)
					{}
					else
						bgf &=~ TILE_FLAG_HALO;

					if ((you.where_are_you == BRANCH_CRYPT || you.where_are_you == BRANCH_TOMB) && bgb == TILE_DNGN_STONE_WALL)
						bgb = TILE_WALL_NORMAL;

					if (bgb == TILE_FLOOR_NORMAL)
						bgb = env.tile_default.floor;
					else if (bgb == TILE_WALL_NORMAL)
						bgb = env.tile_default.wall;

					if (bgb == TILE_DNGN_CLOSED_DOOR || bgb == TILE_DNGN_OPEN_DOOR)
					{
						int off_base = 0;
						int off_dir = 0;
						if(x > 0 && last_bgb == bgb) {
							off_base = 2;
							++off_dir;
						}

						if((x + 1) < frag.cols && (frag(x + 1, y).second & TILE_FLAG_MASK) == bgb) {
							off_base = 2;
							--off_dir;
						}

						bgb += off_base + off_dir;
					}
					else if (bgb == TILE_DNGN_PORTAL_WIZARD_LAB || bgb == TILE_DNGN_ALTAR_CHEIBRIADOS)
					{
						bgb += (fin.frame_num % tile_dngn_count(bgb));
					}
					else if (bgb < TILE_DNGN_MAX)
					{
						unsigned idx;
						if (bgb >= TILE_DNGN_LAVA && bgb < TILE_BLOOD && !(bgf & TILE_FLAG_UNSEEN))
							idx = random2(256);
						else
							idx = fin.random_data[y][x];

						bgb = _pick_random_dngn_tile(bgb, idx);
					}

					p.first = fgb | fgf;
					p.second = bgb | bgf;

					last_bgb = orig_bgb;
				}
			}
		}
	};

	vector<vector<unsigned> > random_data;
	unsigned frame_num;

	CrawlTilesFinalizerImpl()
	: frame_num(0)
	 {}

	~CrawlTilesFinalizerImpl() {}


	virtual void reset()
	{
		frame_num = 0;
		random_data.clear();
	}

	virtual void finalize(CrawlFragment& frag, unsigned* floor_tiles, unsigned* wall_tiles, coord_def player_pos, bool seen_works, bool is_silenced)
	{
		Instance instance(*this, frag, floor_tiles, wall_tiles, player_pos, seen_works, is_silenced);
		++frame_num;
	}
};

CrawlTilesFinalizer* CrawlTilesFinalizer::create()
{
	return new CrawlTilesFinalizerImpl();
}
