#include <libssh/libssh.h>

int main(int argc, char** argv)
{
	return (LIBSSH_VERSION_INT >= SSH_VERSION_INT(0, 4, 0)) ? 0 : 1;
}
